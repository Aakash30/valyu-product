module.exports = {
  // The Webpack config to use when compiling your react app for development or production.
  webpack: function(config) {
    config.module.rules.push({
      test: /\.html$/i,
      loader: 'html-loader',
    })
    return config;
  },
}
