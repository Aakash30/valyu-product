import baseConfig from './env/base.config';
import devConfig from './env/dev.config';
import qaConfig from './env/qa.config';
import uatConfig from './env/uat.config';
import prodConfig from './env/prod.config';
import Config from './env/config';

let runningEnvConfig;

switch (process.env.REACT_APP_SERVER_ENV) {
  case 'dev':
    runningEnvConfig = devConfig;
    break;
  case 'qa':
    runningEnvConfig = qaConfig;
    break;
  case 'uat':
    runningEnvConfig = uatConfig;
    break;
  case 'prod':
    runningEnvConfig = prodConfig;
    break;
  default:
    runningEnvConfig = Config;
    break;
}

console.log(runningEnvConfig)

const config = {
  ...baseConfig,
  ...runningEnvConfig,
};


export default config;
