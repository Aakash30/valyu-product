/**
 * Base settings applicable across environments
 *
 * This file can include shared settings across environments
 *
 */
export default {
  ENV: process.env.NODE_ENV,
  API_TIMEOUT: 60000,
  API_MAX_RETRIES: 2,
  PRIVATE_ROOT: '/dashboard',
  PUBLIC_ROOT: '/',
  APP_ID: 'VALYU_WEB_PORTAL',
  PUSH_NOTIFICATIONS_ENABLED: true,
  FIREBASE: {
    FIREBASE_ANALYTICS_ENABLED: true,
    WEB: {
      apiKey: "AIzaSyCxh2GJ_bylLwmsO3S5yUcj0DRd-Q3cnOU",
      authDomain: "valyu-35120.firebaseapp.com",
      databaseURL: "https://valyu-35120.firebaseio.com",
      projectId: "valyu-35120",
      storageBucket: "valyu-35120.appspot.com",
      messagingSenderId: "346487045922",
      appId: "1:346487045922:web:81a9cf1314ee5440f2af7b",
      measurementId: "G-6KS944QMJR"

    },
    FCM_VAPID_KEY:
      'BAG9f0HuAfbKTz0xEXnZ2TNYUNepL2XzjoCpu1yLbTg2ei24uJP9gJm0y7pJpqlPWoTP0E9CCNpHrymOPu2khGo',
    DEFAULT_CHANNEL_NAME_ANDROID: 'test-channel',
    DEFAULT_SUBSCRIBER_TOPIC: 'test-channel',
  },
  MIN_BROWSER_VERSIONS: {
    msie: '>=11',
    safari: '>=10.1',
    chrome: '>=60.0',
    firefox: '>=56.0',
    opera: '>=22',
  }, // Refer to https://github.com/lancedikson/bowser for setting up browser versions
};
