/**
 * Dev environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys.
 *
 */
export default {
  SERVER_URL: 'https://dev.api.valyu.ai/',
  FIREBASE: {
    FIREBASE_ANALYTICS_ENABLED: true,
    WEB: {
      apiKey: "AIzaSyDfD36gSa1mOpH2VuiSoY5Wt-NU6bxcTc0",
      authDomain: "dev-valyu-df194.firebaseapp.com",
      databaseURL: "https://dev-valyu-df194.firebaseio.com",
      projectId: "dev-valyu",
      storageBucket: "dev-valyu.appspot.com",
      messagingSenderId: "199300484533",
      appId: "1:199300484533:web:228b83efdc854427f5ddfc",
      measurementId: "G-43Y0QRGQEY"
    },
    FCM_VAPID_KEY:
      'BNltX1WIR6PUSBfw8MVcAM_G1GqmOf6SJH1-gqr9gUto9S4raF5KS71iVCU3RmQoBF4ImnyES2KXg2AkhSXKNsE',
    DEFAULT_CHANNEL_NAME_ANDROID: 'test-channel',
    DEFAULT_SUBSCRIBER_TOPIC: 'test-channel',
  },
  DEBUG_MODE: true,
};
