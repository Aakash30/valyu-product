/**
 * QA environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys.
 *
 */
export default {
  SERVER_URL: 'https://uat.api.valyu.ai/',
  FIREBASE: {
    FIREBASE_ANALYTICS_ENABLED: true,
    WEB: {
      apiKey: "AIzaSyAyHECPzCRAyewzliDLtz7eAPSp5Xx5mrY",
      authDomain: "uat-valyu.firebaseapp.com",
      databaseURL: "https://uat-valyu.firebaseio.com",
      projectId: "uat-valyu",
      storageBucket: "uat-valyu.appspot.com",
      messagingSenderId: "429020021805",
      appId: "1:429020021805:web:1ed36028e106a414db24a4",
      measurementId: "G-10FV93SVNJ"
    },
    FCM_VAPID_KEY:
      'BMC7x1nA-jebru0mA6NPnI7ecCidjLp6TXCsrqwjuRyTN_fYMMAr9wI1AgRq5KuYPzxSNfYicJXdYHD_dFzTGVw',
    DEFAULT_CHANNEL_NAME_ANDROID: 'test-channel',
    DEFAULT_SUBSCRIBER_TOPIC: 'test-channel',
  },
  DEBUG_MODE: false,
};
