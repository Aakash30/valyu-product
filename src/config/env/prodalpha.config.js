/**
 * QA environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys.
 *
 */
export default {
  SERVER_URL: 'https://api.valyu.ai/',
  FIREBASE: {
    FIREBASE_ANALYTICS_ENABLED: true,
    WEB: {
      apiKey: 'AIzaSyAfN6pOl8CgyaL0MLFv8pWcaNxEbr8dj34',
      authDomain: 'valyu-prod.firebaseapp.com',
      databaseURL: 'https://valyu-prod-default-rtdb.firebaseio.com',
      projectId: 'valyu-prod',
      storageBucket: 'valyu-prod.appspot.com',
      messagingSenderId: '204033451387',
      appId: '1:204033451387:web:d989a463868acc6d4249de',
      measurementId: 'G-DEFV9HJ8HL',
    },
    FCM_VAPID_KEY:
      'BMC7x1nA-jebru0mA6NPnI7ecCidjLp6TXCsrqwjuRyTN_fYMMAr9wI1AgRq5KuYPzxSNfYicJXdYHD_dFzTGVw',
    DEFAULT_CHANNEL_NAME_ANDROID: 'test-channel',
    DEFAULT_SUBSCRIBER_TOPIC: 'test-channel',
  },
  DEBUG_MODE: false,
};
