/**
 * QA environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys.
 *
 */
export default {
  SERVER_URL: 'https://api.valyu.ai/',
  FIREBASE: {
    FIREBASE_ANALYTICS_ENABLED: true,
    WEB: {
      databaseURL: 'https://prod-301712-default-rtdb.firebaseio.com',
      apiKey: 'AIzaSyCJUGR8xlfyZwX7I7EwKKmerz-_rKXTNDE',
      authDomain: 'prod-301712.firebaseapp.com',
      projectId: 'prod-301712',
      storageBucket: 'prod-301712.appspot.com',
      messagingSenderId: '1010101454099',
      appId: '1:1010101454099:web:5eae73786cd7ff0b3646e6',
      measurementId: 'G-0CLPBFYE78',
    },
    FCM_VAPID_KEY:
      'BMC7x1nA-jebru0mA6NPnI7ecCidjLp6TXCsrqwjuRyTN_fYMMAr9wI1AgRq5KuYPzxSNfYicJXdYHD_dFzTGVw',
    DEFAULT_CHANNEL_NAME_ANDROID: 'test-channel',
    DEFAULT_SUBSCRIBER_TOPIC: 'test-channel',
  },
  DEBUG_MODE: false,
};
