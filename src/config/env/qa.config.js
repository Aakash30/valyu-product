/**
 * QA environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys.
 *
 */
export default {
  SERVER_URL: 'https://qa.api.valyu.ai/',
  FIREBASE: {
    FIREBASE_ANALYTICS_ENABLED: true,
    WEB: {
      apiKey: "AIzaSyBtUFj6a9Q1KXi6S0GtXScHm-CbSLVPmkQ",
      authDomain: "qa-valyu.firebaseapp.com",
      databaseURL: "https://qa-valyu.firebaseio.com",
      projectId: "qa-valyu",
      storageBucket: "qa-valyu.appspot.com",
      messagingSenderId: "1017885669601",
      appId: "1:1017885669601:web:e899e32bebb4cb32df9fa0",
      measurementId: "G-CB0YBQHRKB"
    },
    FCM_VAPID_KEY:
      'BMC7x1nA-jebru0mA6NPnI7ecCidjLp6TXCsrqwjuRyTN_fYMMAr9wI1AgRq5KuYPzxSNfYicJXdYHD_dFzTGVw',
    DEFAULT_CHANNEL_NAME_ANDROID: 'test-channel',
    DEFAULT_SUBSCRIBER_TOPIC: 'test-channel',
  },
  DEBUG_MODE: true,
};
