import React, { Component } from 'react';
import { routes } from '../../routing/routes';

import Aux from '../auxiliary/auxiliary';

import * as moment from 'moment';
import { Redirect } from 'react-router-dom';
import storage from 'redux-persist/lib/storage';

const withAuthCheck = (WrappedComponent) => {
  return class extends Component {
    state = {
      isLogout: false,
    };

    componentDidMount() {
      const loginIn = this.props?.userDetails?.expirationDate;
      if (this.props?.userDetails?.expirationDate) {
        const expireIn = moment(loginIn).add(7, 'days');
        if (moment().isAfter(expireIn)) {
          storage.removeItem('persist:global');
          this.props.history.push(routes.Login);
          setTimeout(() => {
            this.setState({ isLogout: true });
          }, 50);
          return;
        }
      }
    }

    render() {
      let renderComponent = (
        <Aux>
          <WrappedComponent {...this.props} />
        </Aux>
      );

      if (this.state.isLogout) {
        renderComponent = <Redirect to={routes.Login} />;
      }

      return renderComponent;
    }
  };
};

export default withAuthCheck;
