import AppConstants from '../../src/app.constants.json';
import _forEach from 'lodash/forEach';
import _isNumber from 'lodash/isNumber';

export const convertArrayToString = (data) => {
  return data.toString().replace(/,/g, '');
};

export const getFullName = (firstName, lastName) => {
  if (firstName && lastName) {
    return `${firstName} ${lastName}`;
  }

  if (firstName) {
    return firstName;
  }

  if (lastName) {
    return lastName;
  }

  return AppConstants.NOT_AVAILABLE;
};

export const naCheck = (...values) => {
  const NA = AppConstants.NOT_AVAILABLE;
  let output;

  if (!values && values[0] !== 0) {
    return NA;
  }

  _forEach(values, (val) => {
    if (output !== NA && !val && val !== 0) {
      output = NA;
    }
  });

  const isInteger = (val) => (Number.isInteger(val) ? val : val.toFixed(2));

  if (output !== NA) {
    output = _isNumber(values[0]) ? isInteger(values[0]) : values.join(' ');
  }

  return output;
};

export const maskString = (str) => {
  if (!str) {
    return '-';
  } else {
    let start = str && str.toString().substring(0, 2);
    let end =
      str &&
      str
        .toString()
        .substring(str.toString().length, str.toString().length - 4);
    let maskStr =
      str &&
      str
        .toString()
        .substring(2, str.toString().length - 4)
        .replace(/\d/g, 'x');
    return start + maskStr + end;
  }
};

// Convert a Base64-encoded string to a File object
export function base64StringtoFile (base64String, filename) {
  let mime = 'application/pdf',
    bstr = atob(base64String), n = bstr.length, u8arr = new Uint8Array(n)
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }
  return new File([u8arr], filename, {type: mime})
}

// Extract an Base64 Image's File Extension
export function extractImageFileExtensionFromBase64 (base64Data) {
  return base64Data.substring('data:image/'.length, base64Data.indexOf(';base64'))
}

