/**
 * Default Images repository.
 */
import SPINNER from '../assets/images/spinner.svg';
import IC_PLUS from '../assets/images/plus.png';
import LOGO from '../assets/images/logo.svg';
import IC_BELL from '../assets/images/bell.svg';
import IC_WORLD from '../assets/images/world.svg';
import USER from '../assets/images/ic_user_profile.svg';
import IC_HAMBURGER_ICON from '../assets/images/hamburger.svg';
import IC_DOTS from '../assets/images/dots.svg';
import IC_CALENDER from '../assets/images/calender.svg';
import APP_ICON from '../assets/images/icon-splash.png';
import ERROR_ICON from '../assets/images/error.png';
import PICKUP_LOC from '../assets/images/pickup_loc.png';
import DROP_LOC from '../assets/images/drop_loc.png';
import CAR_ICON from '../assets/images/car.png';
import SELECTED_CAR from '../assets/images/selected_car.jpg';
import PLAN_TRIPS from '../assets/images/plan.svg';
import PLAN_TRIPS_ACTIVE from '../assets/images/plan-active.svg';
import FILTER_RIDE from '../assets/images/select-ride.svg';
import OPEN_TRIPS from '../assets/images/open.svg';
import OPEN_TRIPS_ACTIVE from '../assets/images/open-active.svg';
import PAST_TRIPS from '../assets/images/past.svg';
import PAST_TRIPS_ACTIVE from '../assets/images/past-active.svg';
import FILTER_TRIPS from '../assets/images/select.svg';
import FILTER_TRIPS_ACTIVE from '../assets/images/select-active.svg';
import RIDE from '../assets/images/ride.png';
import TRIP_TIME from '../assets/images/trip_time.svg';
import ADD from '../assets/images/add.svg';
import SUBMIT from '../assets/images/submit.svg';
import ACTIVE_LOGIN_ICON from '../assets/images/active-login.svg';
import INACTIVE_LOGIN_ICON from '../assets/images/inactive-login.svg';
import INACTIVE_LOGOUT_ICON from '../assets/images/inactive_in_out.svg';
import ACTIVE_LOGOUT_ICON from '../assets/images/active-logout.svg';
import ICON_HOME from '../assets/images/icon_home.png';
import VALYU_LOGO from '../assets/images/valyu-logo.png';
import EMP_NAME from '../assets/images/emp_name.svg';
import EMP_ID from '../assets/images/emp_id.svg';
import MAIL_ID from '../assets/images/mail_id.svg';
import SALARY from '../assets/images/salary.svg';
import CONTACT from '../assets/images/contact.svg';
import JOINING from '../assets/images/joining.svg';
import MESSAGE from '../assets/images/question.svg';
import STEP1 from '../assets/images/STEP1.svg';
import STEP2 from '../assets/images/STEP2.svg';
import STEP2_ACTIVE from '../assets/images/STEP2_ACTIVE.svg';
import STEP3 from '../assets/images/STEP3.svg';
import STEP3_ACTIVE from '../assets/images/STEP3_ACTIVE.svg';
import STEP4 from '../assets/images/STEP4.svg';
import STEP4_ACTIVE from '../assets/images/STEP4_ACTIVE.svg';
import GOOGLE_BADGE from '../assets/images/google-play-badge.svg';
import APP_BADGE from '../assets/images/app-store-badge.svg';
import ADOBE from '../assets/images/adobe.svg';
import COMPANY from '../assets/images/company.svg';
import VERIFY_MOBILE from '../assets/images/mobile.svg';
import HANDWAVE from '../assets/images/hand-wave.svg';
import ADVANCE_SALARY from '../assets/images/ic_advance_salary.svg';
import CREATE_REQUEST_1 from '../assets/images/create_request_1.svg';
import CREATE_REQUEST_2 from '../assets/images/create_request_2.svg';
import CREATE_REQUEST_3 from '../assets/images/create_request_3.svg';
import CREATE_REQUEST_4 from '../assets/images/create_request_4.svg';
import CREATE_REQUEST_6 from '../assets/images/create_request_6.svg';
import BACK_ARROW from '../assets/images/left-arrow.svg';
import INF0_ICON from '../assets/images/info-icon.svg';
import NAVIGATION_ICON from '../assets/images/navigation-icon.svg';
import PENDING_CLOCK from '../assets/images/pending-clock.svg';
import FAILED_CLOCK from '../assets/images/failed-clock.svg';
import UPLOAD_CLOCK from '../assets/images/upload-clock.svg';
import FAILED from '../assets/images/failed.svg';
import UPLOAD from '../assets/images/upload.svg';
import PENDING from '../assets/images/pending.svg';
import USER_AVATAR from '../assets/images/dummeyuser.png';
import COMPANY_LOGO from '../assets/images/company_logo.svg';
import TEXT_ICON from '../assets/images/text_icon.svg';
import CELEBRATION from '../assets/images/celebration.svg';
import TRIANGLE_UP from '../assets/images/ic_tringle_up.svg';
import TRIANGLE_DOWN from '../assets/images/ic_tringle_down.svg';
import HEADING_BULLET from '../assets/images/ic_heading_bullet.svg';
import OTP_VERIFICATION from '../assets/images/OTP_VERIFICATION.svg';
import TRANSCATION_REDIRECT from '../assets/images/history-line.svg';
import HOME_REDIRECT from '../assets/images/dashboard-line.svg';
import CHECK_GREEN from '../assets/images/check-green.svg';
import DOWNLOAD_BTN from '../assets/images/file-download-line.svg';
import PRINT_BTN from '../assets/images/printer-line.svg';
import MOB_USER from '../assets/images/user-photo.png';
import MOB_DASHBOARD from '../assets/images/mob-dashboard.svg';
import MOB_ARROW from '../assets/images/mob-arrow.svg';
import MOB_REQUEST from '../assets/images/mob-create_request.svg';
import MOB_HELP from '../assets/images/mob-help.svg';
import MOB_PLAYSTORE from '../assets/images/mob-playstore.svg';
import MOB_PROFILE from '../assets/images/mob-profile.svg';
import MOB_SETTINGS from '../assets/images/mob-settings.svg';
import MOB_TRANSCATION from '../assets/images/mob-transcation.svg';
import APP_STORE from '../assets/images/app-store-ios.svg';
import LOGOUT from '../assets/images/logout.svg';
import PRIVATE_POLICY from '../assets/images/private-policy.svg';
import TERMS from '../assets/images/terms.svg';
import DASHBOARD from '../assets/images/dashboard-gray.svg';
import REQUEST from '../assets/images/request.svg';
import TRANSECATION from '../assets/images/transection.svg';
import PROFILE from '../assets/images/profile.svg';
import HELP from '../assets/images/help.svg';
import SETTING from '../assets/images/settings-gray.svg';
import DAILY from '../assets/images/daily.svg';
import MONTHLY from '../assets/images/monthly.svg';
import CONFIRMED from '../assets/images/confirmed.svg';
import CANCELLED from '../assets/images/cancelled.svg';
import TAB_MENU from '../assets/images/3-dots.svg';
import CROSS from '../assets/images/noti-cross.svg';
import N_GREEN from '../assets/images/noti-green.svg';
import N_RED from '../assets/images/noti-red.svg';
import N_LIGHTBLUE from '../assets/images/noti-lightBlue.svg';
import N_BLUE from '../assets/images/noti-blue.svg';
import SELECT_COMPANY from '../assets/images/selectCompany.png';
import VERIFCATION from '../assets/images/verification.png';
import GOOGLEFILE from '../assets/images/google-docs.svg';
import IC_PDF from '../assets/images/ic_pdf.png';
import GOOGLE_FORMS from '../assets/images/google-forms.svg';
import ERROR_IMG from '../assets/images/image.png';

export default {
  ERROR_IMG,
  GOOGLE_FORMS,
  SELECT_COMPANY,
  VERIFCATION,
  DASHBOARD,
  REQUEST,
  TRANSECATION,
  PROFILE,
  HELP,
  SETTING,
  SPINNER,
  IC_PLUS,
  LOGO,
  IC_BELL,
  IC_WORLD,
  IC_HAMBURGER_ICON,
  USER,
  IC_DOTS,
  IC_CALENDER,
  APP_ICON,
  ERROR_ICON,
  RIDE,
  PICKUP_LOC,
  DROP_LOC,
  CAR_ICON,
  SELECTED_CAR,
  PLAN_TRIPS,
  PLAN_TRIPS_ACTIVE,
  FILTER_RIDE,
  OPEN_TRIPS,
  OPEN_TRIPS_ACTIVE,
  PAST_TRIPS,
  PAST_TRIPS_ACTIVE,
  FILTER_TRIPS,
  FILTER_TRIPS_ACTIVE,
  TRIP_TIME,
  ADD,
  SUBMIT,
  ACTIVE_LOGIN_ICON,
  INACTIVE_LOGIN_ICON,
  ACTIVE_LOGOUT_ICON,
  INACTIVE_LOGOUT_ICON,
  ICON_HOME,
  VALYU_LOGO,
  EMP_NAME,
  EMP_ID,
  MAIL_ID,
  SALARY,
  CONTACT,
  JOINING,
  MESSAGE,
  STEP1,
  STEP2,
  STEP2_ACTIVE,
  STEP3,
  STEP3_ACTIVE,
  STEP4,
  STEP4_ACTIVE,
  GOOGLE_BADGE,
  APP_BADGE,
  ADOBE,
  COMPANY,
  VERIFY_MOBILE,
  HANDWAVE,
  ADVANCE_SALARY,
  CREATE_REQUEST_1,
  CREATE_REQUEST_2,
  CREATE_REQUEST_3,
  CREATE_REQUEST_4,
  CREATE_REQUEST_6,
  BACK_ARROW,
  INF0_ICON,
  NAVIGATION_ICON,
  PENDING_CLOCK,
  FAILED_CLOCK,
  UPLOAD_CLOCK,
  PENDING,
  UPLOAD,
  FAILED,
  USER_AVATAR,
  COMPANY_LOGO,
  TEXT_ICON,
  CELEBRATION,
  TRIANGLE_UP,
  TRIANGLE_DOWN,
  HEADING_BULLET,
  OTP_VERIFICATION,
  TRANSCATION_REDIRECT,
  HOME_REDIRECT,
  CHECK_GREEN,
  DOWNLOAD_BTN,
  PRINT_BTN,
  MOB_USER,
  MOB_DASHBOARD,
  MOB_ARROW,
  MOB_REQUEST,
  MOB_HELP,
  MOB_PLAYSTORE,
  MOB_SETTINGS,
  MOB_TRANSCATION,
  MOB_PROFILE,
  APP_STORE,
  LOGOUT,
  PRIVATE_POLICY,
  TERMS,
  DAILY,
  MONTHLY,
  CONFIRMED,
  CANCELLED,
  TAB_MENU,
  CROSS,
  N_GREEN,
  N_RED,
  N_LIGHTBLUE,
  N_BLUE,
  GOOGLEFILE,
  IC_PDF,
};
