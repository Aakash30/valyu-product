import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import configureStore from './store/store';
import { PersistGate } from 'redux-persist/integration/react';

const initialState = {};

const { store, persistor } = configureStore(initialState);

if("serviceWorker" in navigator){
  navigator.serviceWorker
  .register(
    `./firebase-messaging-sw.js`
  )
  .then((reg) => {
    console.log('Register successfully.', reg.scopr);
  })
  .catch(error => console.log(error));
}

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
