import { applyMiddleware, combineReducers, createStore, compose } from 'redux';
import thunk from 'redux-thunk';

import appReducer from './reducer/appReducer';

import { persistStore } from 'redux-persist';

import { persistReducer } from 'redux-persist';

import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web
import autoMergeLevel2 from 'redux-persist/es/stateReconciler/autoMergeLevel2';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleWare = composeEnhancers(applyMiddleware(thunk));

function configureStore(initialState = {}) {
  const reducer = combineReducers({
    global: persistReducer(
      {
        key: 'global', // key for localStorage key, will be: "persist:form"
        version: 0,
        storage,
        debug: true,
        whitelist: [
          'userInfo',
          'clientList',
          'isNextButtonDisable',
          'companies',
          'loanData',
          'usersList',
          'userData',
          'activatedState',
          'UIConstant',
          'notifications',
          'activePath',
          'verifyOTP',
          'helpDesk',
          'helpDeskSubscription',
          'constant',
        ],
        stateReconciler: autoMergeLevel2,
      },
      appReducer,
    ),
  });

  const store = createStore(reducer, initialState, middleWare);

  const persistor = persistStore(store, null);

  return { store, persistor };
}

export default configureStore;
