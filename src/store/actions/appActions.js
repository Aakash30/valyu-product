import * as actionTypes from './actionTypes';

import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/storage';

import * as moment from 'moment';
import * as _ from 'lodash';

import { BASE_URL } from '../../shared/apiConstant';
import { Database_Ref } from '../../utils/firebase/firebase';
import { routes } from '../../routing/routes';
import { updateObject } from '../../shared/shared';
import { PRODUCT_TYPE } from '../../app.constants.json';
import authConstant from '../../screens/auth/auth.constant.json';
import { update } from 'lodash';

export const manageAuthState = (authState) => {
  return {
    type: actionTypes.MANAGE_AUTH_STATE,
    authState,
  };
};

export const onSetRedirectPath = (path) => {
  return {
    type: actionTypes.SET_AUTH_REDIRECT_PATH,
    path,
  };
};

export const onSetUserNumber = (phoneNumber) => {
  return {
    type: actionTypes.SET_USER_PHONE_NUMBER,
    phoneNumber,
  };
};

export const onSetUserData = (userData) => {
  const userInfo = {};

  userInfo['userId'] = userData?.uid ?? userData?.userId;
  userInfo['refreshToken'] = userData?.refreshToken;
  userInfo['accessToken'] = userData?.accessToken;
  userInfo['selectedCompany'] = userData?.selectedCompany;
  userInfo['expirationDate'] = moment();

  return {
    type: actionTypes.SET_USER_ID,
    userInfo,
  };
};

export const onAuthStart = () => {
  return {
    type: actionTypes.AUTH_LOADER_START,
  };
};

export const onAuthEnd = () => {
  return {
    type: actionTypes.AUTH_LOADER_END,
  };
};

export const onChangeNumber = () => {
  return {
    type: actionTypes.SELECT_CHANGE_NUMBER,
  };
};

export const onHandleActive = (path) => {
  return {
    type: actionTypes.SET_ACTIVE_ROUTE,
    activePath: path,
  };
};

export const onFecthUsersProfileSuccess = (data, loanData) => {
  return {
    type: actionTypes.FETCH_WORK_PROFILE_SUCCESS,
    userData: data,
    loanData,
  };
};

export const fecthTransactionSuccess = (transactions) => {
  return {
    type: actionTypes.FETCH_TRANSACTION_SUCCESS,
    transactions,
  };
};

export const failedFunction = (msg) => {
  let errorMsg = msg ?? null;
  return {
    type: actionTypes.FAILED_API,
    errorMsg,
  };
};

export const fetchUsersSuccess = (users) => {
  return {
    type: actionTypes.FETCH_USERS_SUCCESS,
    usersList: users,
  };
};

export const fetchClientSuccess = (clientData) => {
  return {
    type: actionTypes.FETCH_CLIENT_SUCCESS,
    clientList: clientData,
  };
};

export const getDeviceToken = (payload) => {
  return {
    type: actionTypes.GET_DEVICE_TOKEN_SUCESS,
    deviceToken: payload.deviceToken,
  };
};

export const onSetCompany = (selectedCompany) => {
  return {
    type: actionTypes.SET_SELECTED_COMPANY,
    selectedCompany,
  };
};

export const onUpdateStep = (currentStep) => {
  return {
    type: actionTypes.UPDATE_REQUEST_STEP,
    currentStep,
  };
};

export const onBackPreStep = (currentStep) => {
  return {
    type: actionTypes.PREVIOUS_REQUEST_STEP,
    currentStep,
  };
};

export const onRequestLoanData = (data) => {
  return {
    type: actionTypes.REQUEST_LOAN_DATA,
    payload: data,
  };
};

export const getLoanCalculation = (data) => {
  return {
    type: actionTypes.LOAN_DATA_CALCULATION,
    amount: data.amount ?? 0,
    interestRate: data.interestRate ?? 0,
    processing_fee: data.processing_fee ?? 0,
    interestAmount: data.interestAmount ?? 0,
    totalRepayment: data.totalRepayment ?? 0,
    repaymentDate: data.settlementDate ?? '',
    platformFee: data.plateform_fees ?? 0,
    applyLoanAmount: data.total_loan_applied ?? 0,
    loanTenure: data.date_diff ?? 0,
    isSubscriptionEmploye: data.isSubscriptionEmploye,
  };
};

export const onSelectReason = (reason) => {
  return {
    type: actionTypes.SELECTED_REASON,
    reason,
  };
};

export const isFtechUserProfile = (path, companies) => {
  console.log(path);
  return {
    type: actionTypes.FETCH_USER_PROFILE,
    path,
    companies,
  };
};

export const onSetConfirmCode = (code) => {
  return {
    type: actionTypes.HANDLE_CHECK_OTP,
    code,
  };
};

export const onInavlidCode = (isInvalidCode, isConfirmCode) => {
  return {
    type: actionTypes.HANDLE_INVALID_OTP,
    isInvalidCode,
    isConfirmCode,
  };
};

export const onFetchSuccessUI = (data) => {
  return {
    type: actionTypes.SUCCESSFULLY_FETCH_UI,
    data,
  };
};

export const onFetchSuccessHelpDesk = (data) => {
  return {
    type: actionTypes.SUCCESSFULLY_FETCH_HELPDESK_CONSTANT,
    data,
  };
};

export const onFetchSuccessHelpDeskSubscription = (data) => {
  return {
    type: actionTypes.SUCCESSFULLY_FETCH_HELPDESK_SUBSCRIPTION_CONSTANT,
    data,
  };
};

export const onFetchNotification = (data) => {
  return {
    type: actionTypes.SUCCESSFULLY_FETCH_NOTIFICATION,
    data,
  };
};

export const onDisableButton = () => {
  return {
    type: actionTypes.HANDLE_DISABLE_BUTTON,
  };
};

export const onEnableButton = () => {
  return {
    type: actionTypes.HANDLE_ENABLE_BUTTON,
  };
};

export const onResteLoanFields = () => {
  return {
    type: actionTypes.RESET_ALL_LOAN_DATA,
  };
};

export const onSubmitPersonalData = (payload, files) => {
  return {
    type: actionTypes.SET_PERSONAL_DATA,
    payload,
    files,
  };
};

export const logOutUser = () => {
  return {
    type: actionTypes.LOG_OUT,
  };
};

export const sendOtpToPhone = (phoneNumber) => {
  let appVerifier = window.recaptchaVerifier;

  return (dispatch) => {
    dispatch(onAuthStart());
    firebase
      .auth()
      .signInWithPhoneNumber('+91' + phoneNumber, appVerifier)
      .then((confirmationResult) => {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).gol.elon
        window.confirmationResult = confirmationResult;
        dispatch(onSetUserNumber(phoneNumber));
        dispatch(onAuthEnd());
      })
      .catch((error) => {
        dispatch(onAuthEnd());
        // Error; SMS not sent
        // ...
      });
  };
};

export const confirmCode = (code, number, deviceToken) => {
  return (dispatch) => {
    dispatch(onAuthStart());
    window.confirmationResult
      .confirm(code)
      .then((result) => {
        // User signed in successfully.
        let user = result.user;
        if (deviceToken !== '') {
          dispatch(onAuthEnd());
          dispatch(onSetUserData(user));
          dispatch(fecthUIConstant());
          dispatch(fetchHelpDeskConstant());
          dispatch(fetchHelpDeskSubscriptionConstant());
          dispatch(fetchUserProfile(number, deviceToken));
        } else {
          dispatch(onSetConfirmCode(code));
        }
        // ...
      })
      .catch((error) => {
        dispatch(onAuthEnd());
        dispatch(onInavlidCode(true, false));
        dispatch(failedFunction(error?.message));

        setTimeout(() => {
          dispatch(onInavlidCode(false, true));
          dispatch(failedFunction());
        }, 500 * 10);
        // User couldn't sign in (bad verification code?)
        // ...
      });
  };
};

export const fecthUIConstant = () => {
  return (dispatch) => {
    dispatch(onAuthStart());
    let constantRef = Database_Ref.child(BASE_URL.CONSTANT);
    constantRef.once('value', (snapshot) => {
      const fetchData = snapshot.val();
      dispatch(onFetchSuccessUI(fetchData));
    });
    dispatch(onAuthEnd());
  };
};

export const fetchHelpDeskConstant = () => {
  return (dispatch) => {
    dispatch(onAuthStart());
    let constantRef = Database_Ref.child(
      BASE_URL.CONSTANT + '/' + BASE_URL.HELPDESK,
    );
    constantRef.once('value', (snapshot) => {
      const fetchData = snapshot.val();
      dispatch(onFetchSuccessHelpDesk(fetchData));
    });
    dispatch(onAuthEnd());
  };
};

export const fetchHelpDeskSubscriptionConstant = () => {
  return (dispatch) => {
    dispatch(onAuthStart());
    let constantRef = Database_Ref.child(
      BASE_URL.CONSTANT + '/' + BASE_URL.HELPDESK_SUBSCRIPTION,
    );
    constantRef.once('value', (snapshot) => {
      const fetchData = snapshot.val();
      dispatch(onFetchSuccessHelpDeskSubscription(fetchData));
    });
    dispatch(onAuthEnd());
  };
};

export const fetchUserProfile = (number, deviceToken) => {
  console.log('357');
  return (dispatch) => {
    dispatch(onAuthStart());
    let userProfileRef = Database_Ref.child(BASE_URL.USER_URL + '/' + number);
    userProfileRef.once(
      'value',
      (snapshot) => {
        const fetchData = snapshot.val();
        console.log(fetchData);
        let redirectPath = routes.SelectCompany;
        try {
          if (fetchData || fetchData !== null) {
            dispatch(fetchClient());
            const userProfile = fetchData[BASE_URL.USER_PROFILE];
            let loanData = {};
            if (userProfile) {
              loanData = {
                dob: userProfile?.dob,
                firstName: userProfile?.firstName,
                lastName: userProfile?.lastName,
                city: { label: userProfile?.city, value: userProfile?.city },
                currentState: {
                  label: userProfile?.state,
                  value: userProfile?.state,
                },
                currentAddressLine1: userProfile?.address1,
                currentAddressLine2: userProfile?.address2,
                panNumber: fetchData?.personal?.panNumber,
                aadharNumber: fetchData?.personal?.aadharNumber,
                currentPincode: userProfile?.pinCode,
                gender: userProfile?.gender,
              };
            }
            console.log('387 [profile if condition]');
            dispatch(onFetchWorkProfile(userProfile, number));
            dispatch(fecthUIConstant());
            dispatch(onFetchNotifications(number));
            dispatch(onAuthEnd());
            dispatch(onFecthUsersProfileSuccess(fetchData, loanData));
          } else {
            const fetchCompanies = {
              id: authConstant.ADD_SALARY_COMPANY[0],
              name: authConstant.ADD_SALARY_COMPANY[0],
            };
            dispatch(onSetCompany(fetchCompanies));
            dispatch(failedFunction());
            dispatch(postPhoneNumber(number));
            dispatch(isFtechUserProfile(redirectPath, []));
            dispatch(onAuthEnd());
          }
          if (fetchData) {
            const userProfile = fetchData[BASE_URL.USER_PROFILE];
            if ((deviceToken !== '' || deviceToken !== null) && userProfile) {
              userProfile.device_token = deviceToken;
              userProfile.device_type = 'web';
              dispatch(postDeviceDetails(userProfile, number));
            }
          }
        } catch (err) {
          dispatch(failedFunction());
        }
      },
      (errorObject) => {
        console.log('The read failed: ' + errorObject.code);
      },
    );
  };
};

export const onFetchWorkProfile = (data, number) => {
  console.log('420');
  return (dispatch) => {
    const workProfileRef = Database_Ref.child(
      BASE_URL.USER_URL + '/' + number + '/' + BASE_URL.WORKPROFILE_URL,
    );
    workProfileRef.once('value', (snapshot) => {
      const workProfileData = snapshot.val();
      console.log(workProfileData);
      try {
        let redirectPath = routes.SelectCompany;
        let fetchCompanies = [];
        if (workProfileData) {
          for (let key in workProfileData) {
            fetchCompanies.push({
              ...workProfileData[key],
              id: key,
              name: key,
            });
          }
          if (fetchCompanies.length === 1) {
            dispatch(fetchClient());
            // if (authConstant.ADD_SALARY_COMPANY.includes(fetchCompanies[0].id)) {
            //   if (!data?.userProvidedData?.isDataSubmissionComplete) {
            // redirectPath = routes.Dashboard;
            //   }
            // }
            dispatch(onSetCompany(fetchCompanies[0]));
          }
        }
        console.log(redirectPath);
        dispatch(isFtechUserProfile(redirectPath, fetchCompanies));
      } catch (err) {
        console.log(err);
      }
    });
  };
};

export const postPhoneNumber = (phoneNumber) => {
  return (dispatch) => {
    dispatch(onAuthStart());

    const updates = {};
    updates['/' + BASE_URL.USER_URL + '/' + phoneNumber] = {};

    Database_Ref.update(updates)
      .then(() => {
        dispatch(onAuthEnd());
      })
      .catch((error) => {
        dispatch(onAuthEnd());
        dispatch(failedFunction(error?.message));
        setTimeout(() => {
          dispatch(failedFunction());
        }, 500 * 10);
      });
  };
};

export const postDeviceDetails = (payload, userId) => {
  return (dispatch) => {
    dispatch(onAuthStart());
    const updatesLink =
      '/' + BASE_URL.USER_URL + '/' + userId + '/' + BASE_URL.USER_PROFILE;

    const profileRef = Database_Ref.child(updatesLink);
    profileRef.update(payload, (error) => {
      if (error) {
        dispatch(failedFunction(error.message));
        setTimeout(() => {
          dispatch(failedFunction());
        }, 500 * 10);
      } else {
        // Data saved successfully!
      }
      dispatch(onAuthEnd());
    });
  };
};

export const postPersonalDetails = (personalData, userId) => {
  return (dispatch) => {
    dispatch(onAuthStart());
    const updatedPersonalLink =
      '/' + BASE_URL.USER_URL + '/' + userId + '/' + BASE_URL.PERSONAL;
    const presonalRef = Database_Ref.child(updatedPersonalLink);
    presonalRef.update(personalData, (error) => {
      if (error) {
        dispatch(failedFunction(error.message));
        setTimeout(() => {
          dispatch(failedFunction());
        }, 500 * 10);
      } else {
        // Data saved successfully!
      }
      dispatch(onAuthEnd());
    });
  };
};

export const postWorkProfile = (company, data, userId) => {
  console.log(data);
  return (dispatch) => {
    dispatch(onAuthStart());
    const providerKey = BASE_URL.USERPROVIDEDDATA;
    const updatedPersonalLink =
      '/' +
      BASE_URL.USER_URL +
      '/' +
      userId +
      '/' +
      BASE_URL.WORKPROFILE_URL +
      '/' +
      company;
    const workProfileRef = Database_Ref.child(updatedPersonalLink);
    workProfileRef.update(data, (error) => {
      if (error) {
        dispatch(failedFunction(error.message));
        setTimeout(() => {
          dispatch(failedFunction());
        }, 500 * 10);
      } else {
        // Data saved successfully!
      }
      dispatch(onAuthEnd());
    });
  };
};

export const onSetUserQuery = (payload) => {
  return (dispatch) => {
    dispatch(onAuthStart());
    const updatesLink = '/' + BASE_URL.QUERY + '/' + payload.phoneNumber; // + '/' + userId;

    const profileRef = Database_Ref.child(updatesLink);
    profileRef.update(payload, (error) => {
      if (error) {
        dispatch(failedFunction(error.message));
        setTimeout(() => {
          dispatch(failedFunction());
        }, 500 * 10);
      } else {
        // Data saved successfully!
      }
      dispatch(onAuthEnd());
    });
  };
};

export const onFetchNotifications = (number) => {
  return (dispatch) => {
    dispatch(onAuthStart());
    let notifyRef = Database_Ref.child(BASE_URL.NOTIFICATION + '/' + number);
    notifyRef.once('value', (snapshot) => {
      const fetchData = snapshot.val();
      try {
        if (fetchData) {
          dispatch(onFetchNotification(fetchData));
        } else {
          dispatch(failedFunction());
        }
      } catch (err) {
        console.log(err);
      }
    });
    dispatch(onAuthEnd());
  };
};

export const fetchClient = () => {
  return (dispatch) => {
    dispatch(onAuthStart());
    let companyRef = Database_Ref.child(BASE_URL.CLIENT_URL);
    companyRef.once('value', (snapshot) => {
      const fetchData = snapshot.val();
      const companies = [];
      if (fetchData) {
        for (let key in fetchData) {
          companies.push({
            ...fetchData[key],
            id: key,
          });
        }
        dispatch(fetchClientSuccess(companies));
        dispatch(onAuthEnd());
      } else {
        dispatch(failedFunction());
        dispatch(onAuthEnd());
      }
    });
  };
};

export const postLoanData = (userDetails, payload, currentStep, userData) => {
  return (dispatch) => {
    dispatch(onAuthStart());

    const companyName = userDetails?.selectedCompany?.id;
    payload['employerName'] = companyName;
    payload['created_at'] = new Date().toString();

    for (let key in payload) {
      if (key === 'currentCity') {
        payload[key] = payload[key]?.value;
      }
      if (key === 'reason') {
        if (!payload[key].value) {
          dispatch(onUpdateStep(0));
          return;
        }
        payload[key] = payload[key].value;
      }
      if (key === 'currentAddressDuration') {
        payload[key] = payload[key].value ?? 0;
      }
    }

    if (
      !userData?.personal?.isDataSubmissionComplete &&
      !userData?.personal?.aadharFrontImagePath &&
      !userData?.personal?.panImagePath
    ) {
      const personalData = updateObject(userData?.personal, {
        aadharNumber: payload?.aadharNumber.replace(/\s/g, ''),
        panNumber: payload?.panNumber,
        aadharFrontImagePath: payload?.aadharFrontImagePath,
        aadharBackImagePath: payload?.aadharBackImagePath,
        panImagePath: payload?.panImagePath,
        isDataSubmissionComplete: true,
        isPanConvert: false,
      });

      dispatch(postPersonalDetails(personalData, userDetails?.phoneNumber));
    }

    console.log(payload);

    if (!userData[BASE_URL.WORKPROFILE_URL][companyName]?.account) {
      const accountObj = {
        bankAcNo: payload?.bankAcNo,
        bankName: payload?.bankName,
        ifscCode: payload?.ifscCode,
      };
      const userDataObj = updateObject(
        userData[BASE_URL.WORKPROFILE_URL][companyName][BASE_URL.USERDATA],
        {
          designation: payload?.designation,
          doj: payload?.doj,
          emailId: payload?.officialEmailId,
          employeeId: payload?.employeeId,
        },
      );
      const workProfileData = {
        account: accountObj,
        userData: userDataObj,
      };
      dispatch(
        postWorkProfile(companyName, workProfileData, userDetails?.phoneNumber),
      );
    }

    if (!userData?.userProfile?.isDataSubmissionComplete) {
      const userProvidedObj =
        userData[BASE_URL.USER_PROFILE][BASE_URL.USERPROVIDEDDATA];
      const userProfileData = updateObject(userData?.userProfile, {
        firstName: payload?.firstName,
        middleName: payload?.middleName ?? '',
        lastName: payload?.lastName,
        dob: payload?.dob,
        gender: payload?.gender,
        mobileNumber: payload?.mobileNumber,
        matrialStatus: payload?.matrialStatus,
        address1: payload?.currentAddressLine1,
        address2: payload?.currentAddressLine2,
        city: payload?.currentCity,
        state: payload?.currentState,
        pinCode: payload?.currentPincode,
        addressType: payload?.currentAddressType,
        addressDuration: payload?.currentAddressDuration,
        doj: payload?.doj,
        isDataSubmissionComplete: true,
      });

      let data = {};

      if (userProvidedObj) {
        data = updateObject(userProfileData, userProvidedObj);
      }
      if (!userProvidedObj) {
        data = userProfileData;
      }

      console.log(userData.userProfile, userProfileData, data);

      dispatch(postDeviceDetails(data, userDetails?.phoneNumber));
    }

    let loanData = {
      amount: payload.amount ?? 0,
      totalRepayment: payload.totalRepayment ?? 0,
      employerName: payload.employerName,
      created_at: payload.created_at,
      id: payload.id,
      reason: payload.reason,
      officialEmail:
        payload?.officialEmailId ??
        userDetails?.selectedCompany?.userData?.emailId,
      accountNumber: payload?.bankAcNo,
      bankName: payload?.bankName,
      ifscCode: payload?.ifscCode,
      loanDocumentUrl: payload?.loanDocumentUrl,
    };

    if (!payload.isSubscriptionEmploye) {
      const obj = {
        interestRate: payload.interestRate ?? 0,
        processing_fee: payload.processing_fee ?? 0,
        interestAmount: payload.interestAmount ?? 0,
      };

      loanData = updateObject(loanData, obj);
    }

    if (payload.isSubscriptionEmploye) {
      const obj = {
        fee: payload.platformFee ?? 0,
      };

      loanData = updateObject(loanData, obj);
    }

    const baseLink =
      '/' + BASE_URL.USER_URL + '/' + userDetails?.phoneNumber + '/';

    const updatesLoanLink = baseLink + BASE_URL.LOAN + '/' + payload.id;

    const loanRef = Database_Ref.child(updatesLoanLink);
    loanRef.update(loanData, (error) => {
      if (error) {
        dispatch(failedFunction(error.message));
        setTimeout(() => {
          dispatch(failedFunction());
        }, 500 * 10);
        dispatch(onBackPreStep(currentStep));
      } else {
        dispatch(onUpdateStep(currentStep));
        dispatch(fetchUserProfile(userDetails?.phoneNumber, null));
        dispatch(fetchLoanData(userDetails?.phoneNumber));
        // Data saved successfully!
      }
      dispatch(onAuthEnd());
    });
  };
};

export const postNotificationStatus = (payload, userId) => {
  return (dispatch) => {
    dispatch(onAuthStart());
    const updatesLink =
      '/' + BASE_URL.NOTIFICATION + '/' + userId + '/' + payload.id;

    const profileRef = Database_Ref.child(updatesLink);
    profileRef.update(payload, (error) => {
      if (error) {
        dispatch(failedFunction());
      } else {
        // Data saved successfully!
      }
      dispatch(onAuthEnd());
    });
  };
};

export const createUIConstant = (payload, key) => {
  return (dispatch) => {
    dispatch(onAuthStart());
    const updatesLink = `${BASE_URL.CONSTANT}/${key}`;

    const constantRef = Database_Ref.child(updatesLink);
    constantRef.update(payload, (error) => {
      if (error) {
        dispatch(failedFunction());
      } else {
        // Data saved successfully!
        dispatch(fecthUIConstant());
      }
      dispatch(onAuthEnd());
    });
  };
};

export const onHandleProductType = (clientList, selectedCompany) => {
  const companyObj = _.keyBy(clientList, 'id');

  let findObj = {};
  if (companyObj) {
    findObj =
      companyObj[selectedCompany?.id][selectedCompany?.userData?.product_name];
  }
  let isSubscriptionEmploye = false;

  if (
    selectedCompany?.userData?.product_name ===
    PRODUCT_TYPE.ADVANCELOAN_SUBSCRIPTION
  ) {
    isSubscriptionEmploye = true;
    let rulesObj = findObj.rules;
    let salary = selectedCompany?.userData?.salary;

    for (let key in rulesObj) {
      const rule = rulesObj[key];
      if (salary <= parseInt(key)) {
        findObj = updateObject(findObj, rule);
        break;
      }
    }

    if (!findObj?.fee) {
      const rule = rulesObj.max;
      findObj = updateObject(findObj, rule);
    }
  }
  return {
    type: actionTypes.HANDLE_PRODUCT_TYPE,
    isSubscriptionEmploye,
    findObj,
  };
};

export const postSalary = (payload, userData, isDashboard) => {
  const number = userData?.phoneNumber;
  const companyId = userData?.selectedCompany?.id;

  payload.isDataSubmissionComplete = true;

  if (payload.client) {
    payload.client = payload.client.value;
  }

  return (dispatch) => {
    dispatch(onAuthStart());
    const updatesLink = `${BASE_URL.USER_URL}/${number}/${BASE_URL.USER_PROFILE}/${BASE_URL.USERPROVIDEDDATA}`;

    const postSalaryRef = Database_Ref.child(updatesLink);
    postSalaryRef.update(payload, (error) => {
      if (error) {
        dispatch(failedFunction());
        dispatch(onAuthEnd());
      } else {
        // Data saved successfully!
        dispatch(fetchClient());
        dispatch(fetchUserProfile(number, userData?.deviceToken));
        setTimeout(() => {
          dispatch(onHandleSuccessSalary(payload.client));
          dispatch(onAuthEnd());
        }, 120 * 50);
      }
    });
  };
};

export const onHandleSuccessSalary = (client) => {
  return {
    type: actionTypes.SET_SALARY,
    clientName: client,
  };
};

export const fetchLoanData = (number) => {
  return (dispatch) => {
    const loandataRef = Database_Ref.child(
      BASE_URL.USER_URL + '/' + number + '/' + BASE_URL.LOAN,
    );
    loandataRef.on(
      'value',
      (snapshot) => {
        const data = snapshot.val();
        console.log(data);
        if (data) {
          dispatch(onStoreLoanData(data));
        }
      },
      (error) => {
        console.log(error);
      },
    );
  };
};

export const onStoreLoanData = (loanData) => {
  return {
    type: actionTypes.STORE_LOAN_DATA,
    loanData,
  };
};
