import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/shared';
import { routes } from '../../routing/routes';
import storage from 'redux-persist/lib/storage';
import authConstant from '../../screens/auth/auth.constant.json';
import * as _ from 'lodash';
import { update } from 'lodash';
import { isFtechUserProfile } from '../actions/appActions';

const initialState = {
  userInfo: {
    phoneNumber: null,
    userId: null,
    deviceToken: null,
    refreshToken: null,
    accessToken: null,
    selectedCompany: null,
    expirationDate: null,
  },
  allSteps: {
    INITIAL_STEP: 0,
    FIRST_STEP: 1,
    SECOND_STEP: 2,
    THIRD_STEP: 3,
    FOURTH_STEP: 4,
    FIVETH_STEP: 5,
    SIXTH_STEP: 6,
  },
  activatedState: 0,
  loanData: {},
  formIsValid: false,
  isLoader: false,
  companies: [],
  usersList: [],
  clientList: [],
  userData: {},
  transactions: [],
  isGetOtp: false,
  isVerifyNumber: false,
  isChangePhone: false,
  isAuthenticated: false,
  isNextButtonDisable: false,
  authRedirectPath: routes.Login,
  verifyRedirectPath: null,
  isUserProfile: false,
  UIConstant: {},
  notifications: {},
  authState: 1,
  activePath: '',
  isInvalidCode: false,
  isConfirmCode: false,
  verifyOTP: '',
  helpDesk: [],
  helpDeskSubscription: [],
  isSubscriptionEmploye: false,
  productCompany: '',
  isDashboard: false,
  error: null,
  constant: {},
  isFetchLoan: false,
};

const setAuthRedirectPath = (state, action) => {
  return updateObject(state, { authRedirectPath: action.path });
};

const setPhoneNumber = (state, action) => {
  const updatedUser = updateObject(state.userInfo, {
    phoneNumber: action.phoneNumber,
  });

  const userDetails = updateObject(state, {
    isGetOtp: true,
    isVerifyNumber: true,
    isChangePhone: false,
    userInfo: updatedUser,
  });

  return userDetails;
};

const setUserId = (state, action) => {
  const updatedUser = updateObject(state.userInfo, {
    userId: action?.userInfo?.userId,
    refreshToken: action?.userInfo?.refreshToken ?? null,
    accessToken: action?.userInfo?.accessToken ?? null,
    selectedCompany: action?.userInfo?.selectedCompany ?? null,
    expirationDate: action?.userInfo?.expirationDate,
  });

  const userDetails = updateObject(state, {
    isGetOtp: true,
    userInfo: updatedUser,
  });

  return userDetails;
};

const onAuthStart = (state, action) => {
  return updateObject(state, { isLoader: true });
};

const onAuthEnd = (state, action) => {
  return updateObject(state, { isLoader: false });
};

const onSetChangeNumber = (state, action) => {
  return updateObject(state, { isChangePhone: true });
};

const fetchCompaniesSuccess = (state, action) => {
  const userInfo = state.userInfo;
  let selectedCompany = userInfo.selectedCompany;
  if (action?.userData?.workProfile?.length > 0) {
    const updatedSelectedCompany =
      action.userData?.workProfile[selectedCompany.id];
    if (selectedCompany !== {}) {
      selectedCompany = updateObject(selectedCompany, updatedSelectedCompany);
    }
  }

  const userDetails = updateObject(state, {
    userData: action.userData,
    userInfo: updateObject(userInfo, { selectedCompany }),
    loanData: updateObject(state.loanData, action.loanData),
  });

  return userDetails;
};

const fetchTransactionSuccess = (state, action) => {
  return updateObject(state, { transactions: action.transactions });
};

const failedFunction = (state, action) => {
  return updateObject(state, { error: action.errorMsg });
};

const onFetchUsers = (state, action) => {
  return updateObject(state, { usersList: action.usersList });
};

const onFetchClient = (state, action) => {
  return updateObject(state, { clientList: action.clientList });
};

const onSetUserQuery = (state, action) => {
  return updateObject(state, { query: action.payload });
};

const onSetCompany = (state, action) => {
  const companyData = action.selectedCompany;
  let isDashboard = state.isDashboard;

  if (authConstant.ADD_SALARY_COMPANY.includes(companyData.id)) {
    if (
      state?.userData?.userProfile?.userProvidedData?.isDataSubmissionComplete
    ) {
      isDashboard = true;
    }
  }

  const userDetails = updateObject(state, {
    userInfo: updateObject(state.userInfo, {
      selectedCompany: companyData,
    }),
    loanData: updateObject(state.loanData, {
      bankName: action?.selectedCompany?.account?.bankName ?? '',
      bankAcNo: action?.selectedCompany?.account?.bankAcNo ?? '',
      ifscCode: action?.selectedCompany?.account?.ifscCode ?? '',
    }),
    isDashboard,
  });

  return userDetails;
};

const updatedStep = (state, action) => {
  const activeStep = action.currentStep + 1;
  console.log(activeStep);
  return updateObject(state, { activatedState: activeStep });
};

const onBackToPreStep = (state, action) => {
  let activeStep = action.currentStep - 1;

  // if (action.currentStep > 2) {
  //   activeStep = action.currentStep - 1;
  // }

  return updateObject(state, {
    activatedState: activeStep,
    isNextButtonDisable: false,
  });
};

const onSetDeviceToken = (state, action) => {
  const userDetails = updateObject(state, {
    userInfo: updateObject(state.userInfo, {
      deviceToken: action.deviceToken,
    }),
  });

  return userDetails;
};

const onSetLoanData = (state, action) => {
  return updateObject(state, {
    loanData: action.payload,
  });
};

const onSetPersonalData = (state, action) => {
  const loanDetails = updateObject(state, {
    loanData: updateObject(state.loanData, {
      aadharNumber: action?.payload?.aadharNumber,
      panNumber: action?.payload?.panNumber,
      files: action?.files,
      aadharFrontImagePath: action?.payload?.aadharFrontImagePath,
      aadharBackImagePath: action?.payload?.aadharBackImagePath,
      panImagePath: action?.payload?.panImagePath,
    }),
  });

  return loanDetails;
};

const onLoanCalculation = (state, action) => {
  const loanDetails = updateObject(state, {
    loanData: updateObject(state.loanData, {
      amount: action.amount,
      interestAmount: action.interestAmount,
      interest_rate: action.interestRate,
      processing_fee: action.processing_fee,
      totalRepayment: action.totalRepayment,
      repaymentDate: action.repaymentDate,
      platformFee: action.platformFee,
      applyLoanAmount: action.applyLoanAmount,
      loanTenure: action.loanTenure,
      isSubscriptionEmploye: action.isSubscriptionEmploye,
    }),
  });

  return loanDetails;
};

const onSetReason = (state, action) => {
  const loanDetails = updateObject(state, {
    loanData: updateObject(state.loanData, {
      reason: action.reason,
      id:
        state.userInfo.selectedCompany.clientId +
        '_' +
        new Date().getTime() +
        '_' +
        state?.userInfo?.phoneNumber,
    }),
  });

  return loanDetails;
};

const onSetUIData = (state, action) => {
  return updateObject(state, {
    UIConstant: action.data.UI,
    constant: action.data,
  });
};

const onSetHelpDeskData = (state, action) => {
  return updateObject(state, { helpDesk: action.data });
};

const onSetHelpDeskSubscriptionData = (state, action) => {
  return updateObject(state, { helpDeskSubscription: action.data });
};

const onVerifyUserProfile = (state, action) => {
  return updateObject(state, {
    isUserProfile: true,
    verifyRedirectPath: action?.path,
    companies: action?.companies,
  });
};

const onSetVerifyCode = (state, action) => {
  return updateObject(state, {
    isConfirmCode: true,
    isInvalidCode: false,
    verifyOTP: action?.code,
  });
};

const onInvalidCode = (state, action) => {
  return updateObject(state, {
    isInvalidCode: action.isInvalidCode,
    isConfirmCode: action.isConfirmCode,
  });
};

const onResetLoanData = (state, action) => {
  return updateObject(state, {
    loanData: updateObject(state.loanData, {
      declaration: null,
      interest_rate: null,
      created_at: null,
      reason: null,
      processing_fee: null,
      interestAmount: null,
      amount: null,
    }),
    activatedState: 1,
  });
};

const onFetchNotification = (state, action) => {
  return updateObject(state, { notifications: action.data });
};

const onManageAuthState = (state, action) => {
  return updateObject(state, { authState: action.authState });
};

const onHandleActivePath = (state, action) => {
  return updateObject(state, { activePath: action.activePath });
};

const onSetDisable = (state, action) => {
  return updateObject(state, { isNextButtonDisable: false });
};

const onSetEnable = (state, action) => {
  return updateObject(state, { isNextButtonDisable: true, verifyOTP: '' });
};

const onSetProductType = (state, action) => {
  return updateObject(state, {
    productCompany: action.findObj,
    isSubscriptionEmploye: action.isSubscriptionEmploye,
  });
};

const onHandleSalary = (state, action) => {
  let { clientList, userData } = state;
  clientList = _.keyBy(clientList, 'id');
  let selectedCompany = clientList[action.clientName];
  let workProfile = userData?.workProfile[action.clientName] ?? {};

  selectedCompany = { ...selectedCompany, ...workProfile };

  return updateObject(state, {
    isDashboard: true,
    userInfo: updateObject(state.userInfo, { selectedCompany }),
  });
};

const onStoreLoanData = (state, action) => {
  let userData = { ...state.userData };
  userData = updateObject(userData, {
    Loan: updateObject(userData.Loan, action.loanData),
  });
  const data = updateObject(state, { userData, isFetchLoan: true });
  return data;
};

const onLogoutUser = (state) => {
  storage.removeItem('persist:global');
  return updateObject(state, { ...initialState });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_AUTH_REDIRECT_PATH:
      return setAuthRedirectPath(state, action);
    case actionTypes.SET_USER_PHONE_NUMBER:
      return setPhoneNumber(state, action);
    case actionTypes.AUTH_LOADER_START:
      return onAuthStart(state, action);
    case actionTypes.AUTH_LOADER_END:
      return onAuthEnd(state, action);
    case actionTypes.SET_USER_ID:
      return setUserId(state, action);
    case actionTypes.SELECT_CHANGE_NUMBER:
      return onSetChangeNumber(state, action);
    case actionTypes.FETCH_WORK_PROFILE_SUCCESS:
      return fetchCompaniesSuccess(state, action);
    case actionTypes.FAILED_API:
      return failedFunction(state, action);
    case actionTypes.FETCH_USERS_SUCCESS:
      return onFetchUsers(state, action);
    case actionTypes.FETCH_CLIENT_SUCCESS:
      return onFetchClient(state, action);
    case actionTypes.SET_SELECTED_COMPANY:
      return onSetCompany(state, action);
    case actionTypes.SET_USER_HELP_QUERY:
      return onSetUserQuery(state, action);
    case actionTypes.UPDATE_REQUEST_STEP:
      return updatedStep(state, action);
    case actionTypes.REQUEST_LOAN_DATA:
      return onSetLoanData(state, action);
    case actionTypes.GET_DEVICE_TOKEN_SUCESS:
      return onSetDeviceToken(state, action);
    case actionTypes.FETCH_USER_PROFILE:
      return onVerifyUserProfile(state, action);
    case actionTypes.PREVIOUS_REQUEST_STEP:
      return onBackToPreStep(state, action);
    case actionTypes.FETCH_TRANSACTION_SUCCESS:
      return fetchTransactionSuccess(state, action);
    case actionTypes.LOAN_DATA_CALCULATION:
      return onLoanCalculation(state, action);
    case actionTypes.SET_PERSONAL_DATA:
      return onSetPersonalData(state, action);
    case actionTypes.SELECTED_REASON:
      return onSetReason(state, action);
    case actionTypes.SUCCESSFULLY_FETCH_UI:
      return onSetUIData(state, action);
    case actionTypes.SUCCESSFULLY_FETCH_HELPDESK_CONSTANT:
      return onSetHelpDeskData(state, action);
    case actionTypes.SUCCESSFULLY_FETCH_HELPDESK_SUBSCRIPTION_CONSTANT:
      return onSetHelpDeskSubscriptionData(state, action);
    case actionTypes.SUCCESSFULLY_FETCH_NOTIFICATION:
      return onFetchNotification(state, action);
    case actionTypes.RESET_ALL_LOAN_DATA:
      return onResetLoanData(state, action);
    case actionTypes.MANAGE_AUTH_STATE:
      return onManageAuthState(state, action);
    case actionTypes.SET_ACTIVE_ROUTE:
      return onHandleActivePath(state, action);
    case actionTypes.HANDLE_CHECK_OTP:
      return onSetVerifyCode(state, action);
    case actionTypes.HANDLE_INVALID_OTP:
      return onInvalidCode(state, action);
    case actionTypes.LOG_OUT:
      return onLogoutUser(state);
    case actionTypes.HANDLE_DISABLE_BUTTON:
      return onSetDisable(state);
    case actionTypes.HANDLE_ENABLE_BUTTON:
      return onSetEnable(state);
    case actionTypes.HANDLE_PRODUCT_TYPE:
      return onSetProductType(state, action);
    case actionTypes.SET_SALARY:
      return onHandleSalary(state, action);
    case actionTypes.STORE_LOAN_DATA:
      return onStoreLoanData(state, action);
    default:
      return state;
  }
};

export default reducer;
