import React, { Fragment } from 'react';
import * as moment from 'moment';

import IMG from '../../utils/images';
import Aux from '../../hoc/auxiliary/auxiliary';
import { maskString } from '../../utils/common';

const RowData = React.memo(({ row, index, numberConverterFun }) => {
  const statusManage = (status, index) => {
    let element = '';

    if (status === 10) {
      element = (
        <Fragment>
          <img src={IMG.PENDING_CLOCK} className="table-icon" alt="" />
          <span className="yellow">Pending</span>
        </Fragment>
      );
    }

    if (status === 20) {
      element = (
        <Fragment>
          <img src={IMG.PENDING_CLOCK} className="table-icon" alt="" />
          <span className="yellow">Applied</span>
        </Fragment>
      );
    }

    if (status === 30) {
      element = (
        <Fragment key={index}>
          <img src={IMG.CONFIRMED} className="table-icon" alt="" />
          <span className="green">Approved</span>
        </Fragment>
      );
    }

    if (status === 40) {
      element = (
        <Fragment>
          <img src={IMG.PENDING_CLOCK} className="table-icon" alt="" />
          <span className="yellow">Rejected</span>
        </Fragment>
      );
    }

    if (status === 50) {
      element = (
        <Fragment key={index}>
          <img src={IMG.CONFIRMED} className="table-icon" alt="" />
          <span className="green">Disbursed</span>
        </Fragment>
      );
    }

    if (status === 60) {
      element = (
        <Fragment>
          <img src={IMG.PENDING_CLOCK} className="table-icon" alt="" />
          <span className="yellow">Claimed</span>
        </Fragment>
      );
    }

    if (status === 70) {
      element = (
        <Fragment>
          <img src={IMG.PENDING_CLOCK} className="table-icon" alt="" />
          <span className="yellow">Errored</span>
        </Fragment>
      );
    }

    if (status === 80) {
      element = (
        <Fragment key={index}>
          <img src={IMG.CONFIRMED} className="table-icon" alt="" />
          <span className="green">EMAILSENT</span>
        </Fragment>
      );
    }

    return element;
  };
  return (
    <Aux>
      <tr className="table-row" key={row.id.toString()}>
        {/* <td className="icon-wrapper">
          <img
            src={
              row.transfer_type === transferType.type_2
                ? IMG.DAILY
                : IMG.MONTHLY
            }
            className="table-icon"
          />
        </td> */}

        <td className="table-heading col-width">
          <span className="reason-row">{row.reason}</span>
          {/* <span className="table-date">
            {moment(row.update_at).format('DD-MM-YYYY')}
          </span> */}
        </td>
        <td className="col-width table-date">
          <span className="">
            {moment(row.created_at).format('DD MMM, YYYY')}
          </span>
          <span className="table-time">
            {moment(row.created_at).format('hh:mm A')}
          </span>
        </td>

        <td className="col-width table-amount">
          ₹{numberConverterFun(row.amount)}
        </td>

        <td className="col-width table-repayment-amount">
          ₹
          {row.repayment_amount
            ? numberConverterFun(row.totalRepayment)
            : 'N/A'}
        </td>

        <td className="col-width table-status">
          {row.status ? statusManage(row.status, index) : 'N/A'}
        </td>

        <td className="col-width transfer-mode">
          {maskString(row.accountNumber)}
        </td>
      </tr>
    </Aux>
  );
});

export default RowData;
