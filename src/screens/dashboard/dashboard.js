import React, { useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import './dashboard.scss';
import { Button } from '@material-ui/core';
import IMG from '../../utils/images';
import CustomTab from '../../components/UI/tab/tab';
import moment from 'moment';
import { routes } from '../../routing/routes';
import { BASE_URL } from '../../shared/apiConstant';
import _ from 'lodash';
import * as actions from '../../store/actions/index';
import { useEffect } from 'react';
import RowData from './rowData';
import { getFullName } from '../../utils/common';
import * as firebase from 'firebase';
import { PRODUCT_TYPE } from '../../app.constants.json';

function DashboardComponent() {
  const userData = useSelector((state) => state.global.userData);
  const userInfo = useSelector((state) => state.global.userInfo);
  const clientList = useSelector((state) => state.global.clientList);
  const companyProductType = useSelector((state) =>
    _.get(state.global, 'productCompany'),
  );
  const Loan = useSelector((state) => _.get(state.global, 'userData.Loan'));

  const dispatch = useDispatch();
  const stableDispatch = useCallback(dispatch, []); // it doesn't need to change

  const [rowElement, setRowElement] = useState('');
  const [refreshElm, setRefreshElm] = useState('');
  const [banner1, setBanner1] = useState('');
  const [banner2, setbanner2] = useState('');
  const [selectedTab, setSelectedTab] = useState(0);
  const [isDisableButton, setIsDisableButton] = useState(false);
  const [remainingDays, setRemainingDays] = useState(0);
  const [welcomeText, setWelcomeText] = useState('');

  const userProfile = userData.userProfile;
  let transcationArray = [];
  const selectedCompany = userInfo?.selectedCompany;
  console.log('[42 dashboard]', Loan);
  const transcationDataKeyArray = _.keys(userData?.Loan);
  const transcationData = { ...userData?.Loan };
  const tabs = [
    { name: 'All', value: 0 },
    { name: 'This Week', value: 1 },
    { name: 'This Month', value: 2 },
  ];
  const refreshDate = selectedCompany?.userData?.eligibility_date;
  const companyObj = _.keyBy(clientList, 'id');
  const isSubscription = companyObj[selectedCompany.id]?.isSubscription;

  const bannerArray = [
    {
      id: 1,
      bannerUrl: BASE_URL.OFFERBANNNER + '/webapp_banner_2.gif',
    },
    {
      id: 2,
      bannerUrl: BASE_URL.OFFERBANNNER + '/webapp_banner_1.gif',
    },
  ];

  const numberConverter = (number) => {
    return parseInt(number).toLocaleString('en');
  };

  const onHandleRefresh = useCallback(() => {
    stableDispatch(
      actions.fetchUserProfile(userInfo?.phoneNumber, userInfo?.deviceToken),
    );
  }, [userInfo, stableDispatch]);

  // for set welcome text
  useEffect(() => {
    let text = companyProductType?.welcomeText;
    let replaceText = text;
    if (text?.includes('TRANSACTIONCOUNT')) {
      replaceText = text
        .replace('TRANSACTIONCOUNT', companyProductType?.maxAllowedTransaction)
        .replace('FEE', companyProductType?.fee);
    }
    setWelcomeText(replaceText);
  }, [welcomeText, companyProductType]);

  useEffect(() => {
    // fetch product type details
    if (clientList.length > 0) {
      stableDispatch(actions.onHandleProductType(clientList, selectedCompany));
    }

    if (
      refreshDate &&
      moment(moment().format('YYYY-MM-DD')).isAfter(moment(refreshDate))
    ) {
      setRefreshElm(
        <span className="refresh" onClick={() => onHandleRefresh()}>
          Refresh
        </span>,
      );
    }
  }, [
    userData,
    clientList,
    stableDispatch,
    onHandleRefresh,
    refreshDate,
    selectedCompany,
  ]);

  // for getting blackout dates
  const getBlackoutSlot = useCallback(() => {
    let blackoutDates = {};
    const blackoutStartDate = companyProductType?.blackoutStartDate;
    const blackoutEndDate = companyProductType?.blackoutEndDate;
    blackoutDates.blackoutStartDate = blackoutStartDate;
    blackoutDates.blackoutEndDate = blackoutEndDate;
    return blackoutDates;
  }, [companyProductType]);

  const getProductName = useCallback(() => {
    let workProfile = userData && userData.workProfile[selectedCompany.id];
    let productName = workProfile?.userData?.product_name;
    return productName;
  }, [userData]);

  const getTransactionBasedState = useCallback(() => {
    const workProfile = userData && userData?.workProfile[selectedCompany.id];
    const salary = workProfile?.userData?.salary;
    if (getProductName() === PRODUCT_TYPE.ADVANCELOAN_SUBSCRIPTION) {
      const advanceLoanSubscription =
        companyObj[selectedCompany.id]?.advanceLoan_subscription;
      const rules = advanceLoanSubscription && advanceLoanSubscription.rules;
      const totalTransaction =
        workProfile.userData.totalSuccessfullApplication !== null
          ? workProfile.userData.totalSuccessfullApplication
          : 1;
      let maxAllowedTransaction = 0;
      let ruleKey = '';
      if (rules) {
        if (salary <= 15000) {
          ruleKey = '15000';
        } else if (salary <= 20000) {
          ruleKey = '20000';
        } else if (salary <= 25000) {
          ruleKey = '25000';
        } else {
          ruleKey = 'max';
        }
        maxAllowedTransaction = rules[ruleKey].maxAllowedTransaction;
      }
      if (maxAllowedTransaction <= totalTransaction) {
        return true;
      }
    }
    return false;
  }, [userData, companyObj]);

  const getAvailableDateState = useCallback(() => {
    const availableAmount = parseInt(
      selectedCompany?.userData?.available_amount,
    );
    if (availableAmount === 0) {
      return true;
    }
    return false;
  }, [selectedCompany]);

  const getBlackoutDateState = () => {
    const blackoutDates = getBlackoutSlot();
    console.log(blackoutDates);
    const today = moment().format('YYYY-MM-DD');

    if (
      moment(today).isBetween(
        blackoutDates.blackoutStartDate,
        blackoutDates.blackoutEndDate,
        undefined,
        '[]',
      )
    ) {
      console.log('181');
      const remainingDays = moment(moment().format('YYYY-MM-DD')).diff(
        moment(
          moment(blackoutDates.blackoutEndDate, 'YYYY-MM-DD').format(
            'YYYY-MM-DD',
          ),
        ),
        'days',
      );
      setRemainingDays(Math.abs(remainingDays));
      return true;
    }
  };

  // for disable get money now buttona and get remaining days for offer
  useEffect(() => {
    // for blocked get money now button
    console.log(
      getBlackoutDateState(),
      getTransactionBasedState(),
      getAvailableDateState(),
    );
    if (
      getBlackoutDateState() ||
      getTransactionBasedState() ||
      getAvailableDateState()
    ) {
      setIsDisableButton(true);
    } else {
      setIsDisableButton(false);
    }
  }, [
    isDisableButton,
    companyProductType,
    getBlackoutSlot,
    getTransactionBasedState,
    getAvailableDateState,
    userData,
    getBlackoutDateState,
  ]);

  useEffect(() => {
    stableDispatch(actions.onHandleActive('dashboard'));

    if (!banner1 || !banner2) {
      bannerArray.forEach((data) => {
        firebase
          .storage()
          .ref(data.bannerUrl)
          .getDownloadURL()
          .then((url) => {
            if (data.id === 1) {
              setBanner1(url);
            } else {
              setbanner2(url);
            }
          })
          .catch((error) => {
            console.error(error);
          });
      });
    }
  }, [isSubscription]);

  useEffect(() => {
    // const payload = [
    //   'quesscorp'
    // ]
    // stableDispatch(actions.createUIConstant(payload, 'clientList'));

    if (transcationData && transcationDataKeyArray.length > 0) {
      let amount = 0;
      // for (let key in transcationData.reverse()) {
      transcationDataKeyArray
        .sort()
        .reverse()
        .forEach((key) => {
          const row = transcationData[key];

          if (
            moment(row.created_at).isSame(moment(), 'month') &&
            (row.status === 30 || row.status === 50 || row.status === 20)
          ) {
            amount += row.total_loan_applied;
          }

          if (transcationArray.length <= 5) {
            row.time = new Date(row.created_at).getTime();
            if (selectedTab === 2) {
              if (moment(row.created_at).isSame(moment(), 'month')) {
                transcationArray.push({ id: key, ...row });
              }
            } else if (selectedTab === 1) {
              if (moment(row.created_at).isSame(moment(), 'week')) {
                transcationArray.push({ id: key, ...row });
              }
            } else {
              transcationArray.push({ id: key, ...row });
            }
          }
        });

      if (transcationArray.length > 0) {
        setRowElement(rowData());
      } else {
        setRowElement(<p>No data available.</p>);
      }
    } else {
      setRowElement(<p>No data available.</p>);
    }
  }, [userData, selectedTab]);

  const rowData = () => {
    const column = transcationArray.map((row, index) => {
      return (
        <RowData
          row={row}
          index={index}
          key={index}
          numberConverterFun={(num) => numberConverter(num)}
        />
      );
    });

    return column;
  };

  const onHandleTab = (tab) => {
    setSelectedTab(tab);
  };

  return (
    <section>
      <div className="dashboard-container">
        <div className="dashboard-card height-225">
          <div className="card user-card">
            <div className="card-header">
              <h5 className="heading">
                Welcome,{' '}
                {getFullName(userProfile?.firstName, userProfile?.lastNam)}
              </h5>
              <p className="sub-heading">
                <img src={IMG.INF0_ICON} className="info-icon" />
                <span>
                  As per {moment(refreshDate).format('DD MMMM, YYYY')} |
                </span>
                {refreshElm}
              </p>
            </div>
            <div className="card-content">
              <div className="card-content-left">
                <p className="text">{welcomeText}</p>
                {!isDisableButton ? (
                  <div className="button-box">
                    <Link to={routes.CreateRequest}>
                      <Button className="create-request-btn">
                        Get Money Now
                      </Button>
                    </Link>
                  </div>
                ) : (
                  <div className="button-box">
                    <Button className="create-request-btn" disabled>
                      {remainingDays} {remainingDays <= 1 ? `day` : `days`} more
                      for this offer
                    </Button>
                  </div>
                )}
              </div>

              <div className="card-content-right">
                <div className="column">
                  <div>
                    <p className="amount-heading">Monthly Salary</p>
                    <p className="amount amount-blue">
                      ₹{numberConverter(selectedCompany?.userData?.salary)}
                    </p>
                  </div>

                  <div>
                    <p className="amount-heading">Advance Taken</p>
                    <p className="amount amount-black">
                      ₹
                      {numberConverter(
                        selectedCompany?.userData?.applied_amount,
                      )}
                    </p>
                  </div>
                </div>

                <div className="column">
                  <div>
                    <p className="amount-heading">Total Salary Earned</p>
                    <p className="amount amount-red">
                      ₹
                      {numberConverter(
                        selectedCompany?.userData?.eligibility_amount,
                      )}
                    </p>
                  </div>

                  <div>
                    <p className="amount-heading">Available Salary</p>
                    <p className="amount amount-green">
                      ₹
                      {numberConverter(
                        selectedCompany?.userData?.available_amount,
                      )}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <Link
            to={routes.CreateRequest}
            className={`card banner ${!banner1 ? 'bannerColor' : ''}`}
          >
            <img alt="banner1" src={banner1} />
          </Link>
        </div>

        <div className="dashboard-card height-335">
          <div className="card request-card recent-card">
            <div className="card-header">
              <h5 className="heading">
                Recent Requests
                <div className="tab-menu">
                  <img src={IMG.TAB_MENU} className="tab-menu-icon" alt="" />

                  <div className="table-option">
                    <p></p>
                  </div>
                </div>
              </h5>

              <div className="sub-heading mr-0">
                <CustomTab
                  data={tabs}
                  selectedTab={selectedTab}
                  handleTabFun={(selectedTab) => onHandleTab(selectedTab)}
                />
              </div>
            </div>
            <div className="card-content table-height">
              <table>
                <tbody>
                  <tr className="table-row">
                    <td className="col-width">
                      <span className="color">Purpose</span>
                    </td>
                    <td className="table-date col-width">
                      <span className="color">Date</span>
                    </td>
                    <td className="col-width table-amount">
                      <span className="color">Amount</span>
                    </td>
                    <td className="col-width table-repayment-amount">
                      <span className="color">Repayment Amount</span>
                    </td>
                    <td className="col-width table-status">
                      <span className="color">Status</span>
                    </td>
                    {/* <td className="table-date col-width">
                      <span className="color">Time</span>
                    </td> */}
                    <td className="col-width transfer-mode">
                      <span className="color">Account number</span>
                    </td>
                  </tr>
                  {rowElement}
                </tbody>
              </table>
            </div>
          </div>

          <Link
            to={routes.CreateRequest}
            className={`card banner height-banner ${
              !banner2 ? 'bannerColor' : ''
            }`}
          >
            <img alt="banner2" src={banner2} />
          </Link>
        </div>
      </div>
    </section>
  );
}

export default DashboardComponent;
