import React, { useCallback } from 'react';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import * as actions from '../../store/actions/index';

function Setting() {

    const dispatch = useDispatch();
    const stableDispatch = useCallback(dispatch, []); // it doesn't need to change

    useEffect(() => {
        stableDispatch(actions.onHandleActive('settings'));
    }, [stableDispatch]);

    return (
        <section>
              setting
        </section>
    )
}

export default Setting;
