import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import AuthRouting from '../../routing/authRouting';
import logo from '../../assets/images/valyu-logo.png';
import * as actions from '../../store/actions/index';
import { routes } from '../../routing/routes';


class Auth extends Component {

    componentDidMount() {
        this.props.fecthUIConstant();
    }

    render() {
        let contentDiv = <div className="text-wrapper">
            <p className="banner-text">Financial Wellnes for You</p>
            <p className="banner-sub-text">Join our community that have more than 10000 subscribers and learn new things everyday</p>

            <div className="dots-wrapper">
                <p className={`dots ${this.props.authState >= 1 ? 'dots-bg' : ''}`}></p>
                <p className={`dots ${this.props.authState >= 2 ? 'dots-bg' : ''}`}></p>
                <p className={`dots ${this.props.authState >= 3 ? 'dots-bg' : ''}`}></p>
                <p className={`dots ${this.props.authState >= 4 ? 'dots-bg' : ''}`}></p>
                <p className="dots"></p>
            </div>
        </div>;
        return (
            <div className="main-bg">
                <div className="bg-container">
                    <div className={`bg-overlay-2 ${this.props.authState === 2 ? 'verify-bg' : this.props.authState === 3 ?  'company-bg' : 'login-bg'}`}>
                        <div className={this.props.authState !== 2 ? 'blue-overlay' : 'green-overlay'}>
                            {contentDiv}
                        </div>
                    </div>

                    <div className="bg-overlay-1">
                        <div className="logo-container">
                            <a href="/">
                                <img alt="valyu logo" src={logo} className="img-logo" />
                            </a>
                        </div>
                        <AuthRouting {...this.props} />
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        isGetOtp: _.get(state.global, 'isGetOtp'),
        userDetails: _.get(state.global, 'userInfo', {}),
        authState: _.get(state.global, 'authState', 1),
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onVerifyAuthentication: (data) => dispatch(actions.onSetUserData(data)),
        fecthUIConstant: () => dispatch(actions.fecthUIConstant()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
