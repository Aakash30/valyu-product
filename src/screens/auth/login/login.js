import React, { Component, Fragment } from 'react';
import 'firebase/auth';
import Button from '@material-ui/core/Button';

// import login scss
// import './login.scss';
import * as QueryString from 'query-string';
import '../auth.scss';
import { Redirect } from 'react-router-dom';

// import component for UI
import Input from '../../../components/UI/input/input';
import Loader from '../../../components/loader/loader';
import AlertDialog from '../../../components/UI/modal/modal';

import { checkValidity, updateObject } from '../../../shared/shared';
import { recaptchaFunction } from '../../../utils/firebase/firbaseRecaptcha';

// import all redux actions
import * as actions from '../../../store/actions/index';
import { connect } from 'react-redux';

import IMG from '../../../utils/images';

import _ from 'lodash';
import { routes } from '../../../routing/routes';

class LoginPage extends Component {
  state = {
    controls: {
      phoneNumber: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Enter phone number',
          maxLength: 10,
        },
        value: '',
        validation: {
          required: true,
          minLength: 10,
          maxLength: 13,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
    },
    isGetOtp: false,
    isSubmitted: true,
    formIsValid: false,
    isDialog: false,
    isClose: true,
    buttonName: 'Okay',
  };

  async componentDidMount() {
    // Check phone number available or not, if available then set in input field
    if (this.props?.userDetails?.phoneNumber) {
      this.onHandleResetField();
    }

    // Initialize firebase recaptcha for get OTP token from firebase
    recaptchaFunction();

    await this.props.onManageAuthState();
    const params = QueryString.parse(this.props.location.search);
    localStorage.setItem('clientRefferal', JSON.stringify(params));
    if (params.phone) {
      this.inputChangedHandler(
        { target: { value: params.phone } },
        'phoneNumber',
        true,
      );
    }
  }

  // function call when user enter value in input
  inputChangedHandler = async (event, controlName, skipProppgationCheck) => {
    if (!skipProppgationCheck) {
      event.stopPropagation();
    }
    const updatedControls = updateObject(this.state.controls, {
      [controlName]: updateObject(this.state.controls[controlName], {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          this.state.controls[controlName].validation,
        ),
        touched: true,
      }),
    });

    const formIsValid = updatedControls[controlName].valid;

    await this.setState({
      controls: updatedControls,
      formIsValid,
      isGetOtp: false,
    });

    if (skipProppgationCheck) {
      this.onHandleOtp(null);
    }
  };

  // function call for verify the number and get otp
  onHandleOtp = async (event) => {
    if (event) {
      event.preventDefault();
    }

    if (!this.state.formIsValid && !this.state.controls.phoneNumber.valid) {
      await this.setState({ isSubmitted: false });
      return;
    }

    this.props.onSetLoader();

    const formData = {};
    for (let formElementIdentifier in this.state.controls) {
      formData[formElementIdentifier] = this.state.controls[
        formElementIdentifier
      ].value;
    }

    this.setState({ isGetOtp: true });

    this.props.onSendOtpToPhone(formData?.phoneNumber);
  };

  // function when user click on unable to login
  // onHandleUnableLogin = () => {
  //   this.setState({
  //     isDialog: true,
  //     title: AuthConstant.UNABLE_TO_LOGIN.TITLE,
  //     text: AuthConstant.UNABLE_TO_LOGIN.TEXT,
  //   });
  // };

  // for reset form fields
  onHandleResetField = async () => {
    const { controls } = this.state;
    for (let formElementIdentifier in this.state.controls) {
      controls[
        formElementIdentifier
      ].value = this.props.userDetails.phoneNumber;
      controls[formElementIdentifier].valid = true;
      controls[formElementIdentifier].touched = true;
    }
    await this.setState({ isSubmitted: true, formIsValid: false, controls });
  };

  // close modal
  onHandleCloseAlert = () => {
    this.setState({ isDialog: false, text: '', title: '' });
  };

  validate = (event) => {
    const theEvent = event || window.event;
    // Handle key press
    let key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    const regex = /[0-9]|\./;
    if (!regex.test(key)) {
      theEvent.returnValue = false;
      if (theEvent.preventDefault) theEvent.preventDefault();
    }
  };

  render() {
    const {
      isSubmitted,
      controls,
      isDialog,
      title,
      text,
      buttonName,
      isClose,
    } = this.state;

    const formElementsArray = [];
    for (let key in this.state.controls) {
      formElementsArray.push({
        id: key,
        config: this.state.controls[key],
      });
    }

    let form = formElementsArray.map((formElement) => (
      <Input
        key={formElement.id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        class={`text-input addSpan`}
        divClass={`flag-input`}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        isSubmitted={true}
        changed={(event) => this.inputChangedHandler(event, formElement.id)}
        onKeyPress={() => this.validate()}
      />
    ));

    let errorElement = '';
    if (!isSubmitted && !controls.phoneNumber.valid) {
      errorElement = (
        <p className="error-msg">Please enter the valid mobile number.</p>
      );
    }

    let alertPopup = '';
    if (isDialog) {
      alertPopup = (
        <AlertDialog
          open={true}
          text={text}
          title={title}
          onHandleCloseAlert={() => this.onHandleCloseAlert()}
          buttonName={buttonName}
          isClose={isClose}
        />
      );
    }

    let authRedirect = null;
    if (this.props?.isVerifyNumber && !this.props?.isChangeNumber) {
      authRedirect = <Redirect to={routes.OTPPage} />;
    }

    if (
      this.props?.companiesList.length > 0 &&
      !this.props?.userDetails?.userId
    ) {
      authRedirect = <Redirect to={routes.SelectCompany} />;
    }

    if (this.props?.userDetails?.selectedCompany?.userData) {
      authRedirect = <Redirect to={routes.Dashboard} />;
    }

    return (
      <Fragment>
        {authRedirect}

        <div className="page-icon">
          <img src={IMG.HANDWAVE} className="img-logo" alt="HANDEWAVE" />
        </div>

        <div className="heading-container">
          <h1 className="login-h1">Let's get started</h1>
        </div>

        {this.props.isLoader && <Loader />}

        <form onSubmit={(e) => this.onHandleOtp(e)}>
          <div className="login-form-container">
            <p className="label">Enter your mobile number</p>

            <div className="change-num">
              <div className="phone-number-container">
                {/* <div className="flag"></div> */}
                {form}
              </div>
            </div>

            {errorElement}

            <div className="btn-wrapper">
              <Button
                id="sign-in-button"
                className="verify-btn mar-right"
                type="submit"
              >
                Request OTP
              </Button>

              {/* <Button
                className="resend color-text btn-bg"
                color="secondary"
                onClick={() => this.onHandleUnableLogin()}
              >
                Unable to Log In
              </Button> */}
            </div>

            <div id="recaptcha-container"></div>

            {alertPopup}
          </div>
        </form>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userDetails: _.get(state.global, 'userInfo', {}),
    isLoader: _.get(state.global, 'isLoader', false),
    isVerifyNumber: _.get(state.global, 'isVerifyNumber', false),
    isChangeNumber: _.get(state.global, 'isChangePhone'),
    companiesList: _.get(state.global, 'companies', []),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setNumber: (number) => dispatch(actions.onSetUserNumber(number)),
    onSetLoader: () => dispatch(actions.onAuthStart()),
    onUnsetLoader: () => dispatch(actions.onAuthEnd()),
    onSendOtpToPhone: (data) => dispatch(actions.sendOtpToPhone(data)),
    onPostPhoneNumber: (data) => dispatch(actions.postPhoneNumber(data)),
    getDeviceToken: (payload) => dispatch(actions.getDeviceToken(payload)),
    onManageAuthState: () => dispatch(actions.manageAuthState(1)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
