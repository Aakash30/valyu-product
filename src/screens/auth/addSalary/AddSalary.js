import React, { Component } from 'react';
import Button from '@material-ui/core/Button';

import '../auth.scss';
// import component for UI
import Input from '../../../components/UI/input/input';
import Loader from '../../../components/loader/loader';

// import all redux actions
import * as actions from '../../../store/actions/index';
import { connect } from 'react-redux';

import IMG from '../../../utils/images';

import _ from 'lodash';
import { Fragment } from 'react';
import { checkValidity, updateObject } from '../../../shared/shared';
import { routes } from '../../../routing/routes';
import { Redirect } from 'react-router-dom';

class AddSalary extends Component {
  state = {
    controls: {
      firstName: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'First Name',
        },
        value: '',
        validation: {
          required: true,
        },
        valid: false,
        touched: false,
      },
      lastName: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Last Name',
        },
        value: '',
        validation: {
          required: true,
        },
        valid: false,
        touched: false,
      },
      salary: {
        elementType: 'input',
        label: 'How much is your monthly salary?',
        elementConfig: {
          type: 'text',
          placeholder: 'Enter Salary',
          maxLength: 5,
        },
        value: '',
        validation: {
          required: true,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
      client: {
        elementType: 'select',
        label: 'When do you get your salary?',
        elementConfig: {
          name: 'reasonTitle',
          id: 'reasonTitle',
          className: 'select select-custom',
          classNamePrefix: 'select',
          placeholder: 'Salary Date',
        },
        value: '',
        validation: {
          required: true,
        },
        options: [],
        valid: false,
        touched: false,
      },
    },
    isSubmitted: true,
    formIsValid: false,
    isClose: true,
    buttonName: 'Okay',
  };

  async componentDidMount() {
    this.props.onManageAuthState();
    let { controls } = this.state;
    let options = [];

    try {
      const clientName =
        JSON.parse(localStorage.getItem('clientRefferal')).utm_source +
        'ClientList';
      const listObj = this.props?.constant[clientName];

      if (!listObj) {
        this.setState({ errorMsg: 'Please try again, client list not found.' });
        return;
      }

      for (let key in listObj) {
        if (listObj[key]) {
          options.push({ label: key, value: listObj[key] });
        }
      }

      const client = updateObject(controls.client, {
        options,
      });

      const updatedControls = updateObject(controls, { client });
      this.setState({ controls: updatedControls });

      this.forceUpdate();

      console.log(updatedControls);
    } catch (err) {
      this.setState({ errorMsg: 'Please try again.' });
    }
  }

  // function call for verify the number and get otp
  onHandleSalary = async (event) => {
    event.preventDefault();

    if (!this.state.formIsValid && !this.state.controls.salary.valid) {
      await this.setState({ isSubmitted: false });
      return;
    }

    this.props.onSetLoader();

    const formData = {};
    for (let formElementIdentifier in this.state.controls) {
      formData[formElementIdentifier] = this.state.controls[
        formElementIdentifier
      ].value;
    }

    formData.timeStamp = new Date().getTime();

    // store params
    const params = JSON.parse(localStorage.getItem('clientRefferal'));

    // generate employee id
    formData.employeeId = params.emp_id;
    formData.params = params;

    this.props.onPostSalary(
      formData,
      this.props?.userDetails,
      this.props?.isDashboard,
    );
  };

  inputChangedHandler = async (event, controlName) => {
    // event.stopPropagation();
    const updatedControls = updateObject(this.state.controls, {
      [controlName]: updateObject(this.state.controls[controlName], {
        value: event?.target?.value ?? event,
        valid: checkValidity(
          event?.target?.value ?? event?.value,
          this.state.controls[controlName].validation,
        ),
        touched: true,
      }),
    });

    const formIsValid = updatedControls[controlName].valid;

    await this.setState({
      controls: updatedControls,
      formIsValid,
    });
  };

  // For check input value is number
  validate = (event) => {
    const theEvent = event || window.event;
    // Handle key press
    let key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    const regex = /[0-9]|\./;
    if (!regex.test(key)) {
      theEvent.returnValue = false;
      if (theEvent.preventDefault) theEvent.preventDefault();
    }
  };

  render() {
    const { isSubmitted, controls } = this.state;

    const formElementsArray = [];
    for (let key in this.state.controls) {
      formElementsArray.push({
        id: key,
        config: this.state.controls[key],
      });
    }

    let form = formElementsArray.map((formElement) => (
      <Input
        key={formElement.id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        class={`text-input`}
        divClass={`flag-input`}
        divClasses={'phone-number-container add-salary'}
        isErrorDiv={true}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        isSubmitted={isSubmitted}
        label={formElement?.config?.label ?? ''}
        labelClass={'labelClass'}
        isAddSalary={true}
        options={formElement?.config?.options}
        changed={(event) => this.inputChangedHandler(event, formElement.id)}
        onKeyPress={() => (formElement.id === 'salary' ? this.validate() : '')}
      />
    ));

    let errorElement = '';
    if (this.state.errorMsg) {
      errorElement = <p className="error-msg">{this.state.errorMsg}</p>;
    }

    let authRedirect = null;
    if (this.props?.isDashboard) {
      authRedirect = <Redirect to={routes.Dashboard} />;
    }

    return (
      <Fragment>
        {authRedirect}

        {/* <div className="page-icon">
          <img src={IMG.HANDWAVE} className="img-logo" alt="HANDEWAVE" />
        </div> */}

        <div className="heading-container">
          <h1 className="login-h1">Welcome!</h1>
        </div>

        {this.props.isLoader && <Loader />}

        <form onSubmit={(e) => this.onHandleSalary(e)}>
          <div className="login-form-container">
            <h4 className="title">A few questions for you?</h4>

            <div className="salary-div">{form}</div>

            {errorElement}

            <div className="btn-wrapper">
              <Button
                id="sign-in-button"
                className="verify-btn mar-right"
                type="submit"
              >
                Proceed
              </Button>
            </div>

            <div className="notes">
              <h5>
                *All employment information will be verified by your employer
                during the advance approval process
              </h5>
            </div>
          </div>
        </form>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userDetails: _.get(state.global, 'userInfo', {}),
    isDashboard: _.get(state.global, 'isDashboard', false),
    isLoader: _.get(state.global, 'isLoader', false),
    constant: _.get(state.global, 'constant', {}),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSetLoader: () => dispatch(actions.onAuthStart()),
    onManageAuthState: () => dispatch(actions.manageAuthState(4)),
    onPostSalary: (payload, data, isDashboard) =>
      dispatch(actions.postSalary(payload, data, isDashboard)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddSalary);
