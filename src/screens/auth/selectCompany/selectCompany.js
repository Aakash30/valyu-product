import React, { Component, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as firebase from 'firebase';
import { Redirect } from 'react-router-dom';

import '../auth.scss';

import Loader from '../../../components/loader/loader';

import IMG from '../../../utils/images';

import { routes } from '../../../routing/routes';

import * as actions from '../../../store/actions/index';

import { updateObject } from '../../../shared/shared';

import authConstant from '../auth.constant.json';

class SelectCompany extends Component {
  state = {
    companiesList: [],
    isSelected: '',
    selectedCompany: '',
    isError: false,
    availableClient: [],
    isAddSalary: false,
    clientRefferal: JSON.parse(localStorage.getItem('clientRefferal')) ?? '',
  };

  async componentDidMount() {
    this.props.onManageAuthState();
    if (this.state?.clientRefferal?.utm_source) {
      this.setState({
        isAddSalary: this.props?.constant?.clientList?.includes(
          this.state?.clientRefferal?.utm_source,
        ),
      });
    }
  }

  async componentWillReceiveProps(newProps) {
    if (
      newProps.clientList.length > 0 &&
      this.state.availableClient.length === 0
    ) {
      const companies = _.keyBy(newProps.clientList, 'id');

      const availableClient = [...this.props.companiesList];

      availableClient.forEach((data, index) => {
        if (companies[data.id] !== undefined) {
          data.logoUrl = companies[data.id].logoUrl;
          this.fetchImage(data.logoUrl, index);
        }
      });

      this.setState({ companies, availableClient });
    }
  }

  // get image from storage
  fetchImage = (filePath, index) => {
    firebase
      .storage()
      .ref(filePath)
      .getDownloadURL()
      .then((url) => {
        let { availableClient } = this.state;

        availableClient[index].logoUrl = url;

        this.setState({ availableClient: _.values(availableClient) });
      })
      .catch((error) => {
        console.error(error);
        let { availableClient } = this.state;

        availableClient[index].logoUrl = null;

        this.setState({ availableClient: _.values(availableClient) });
      });

    if (this.state.availableClient.length === 1) {
      this.onHandleSelectedCompaany(this.state.availableClient[0]);
    }
  };

  // function call when user selected company then click on continue
  onHandleContinue = () => {
    if (!this.state.selectedCompany) {
      this.setState({ isError: true });
      return;
    }

    this.props.onSelectCompany(this.state.selectedCompany);

    if (
      authConstant.ADD_SALARY_COMPANY.includes(this.state.selectedCompany.id)
    ) {
      this.props.history.push(routes.AddSalary);
      return;
    }

    console.log('97');

    this.props.history.push(routes.Dashboard);
  };

  // function will when user select company
  onHandleSelectedCompaany = (company) => {
    let { selectedCompany } = this.state;

    selectedCompany = this.state?.companies[company.id];

    selectedCompany = updateObject(selectedCompany, company);

    this.setState({
      isSelected: company?.id,
      selectedCompany: selectedCompany,
      isError: false,
    });
  };

  render() {
    const { isSelected, availableClient, isError } = this.state;

    let companiesTab = '';

    let redirected = '';
    if (this.props?.verifyRedirectPath && !this.state.isAddSalary) {
      redirected = <Redirect to={this.props.verifyRedirectPath} />;
    }

    if (
      this.state.isAddSalary &&
      !this.props?.userDetails?.selectedCompany?.userData
    ) {
      redirected = <Redirect to={routes.AddSalary} />;
    }

    console.log(this.state.isAddSalary);

    if (this.props?.userDetails?.selectedCompany?.userData) {
      redirected = <Redirect to={routes.Dashboard} />;
    }

    console.log(
      this.state.isAddSalary,
      this.props?.userDetails?.selectedCompany,
    );

    if (
      !this.state.isAddSalary &&
      !this.props?.userDetails?.selectedCompany?.userData &&
      this.props?.clientList?.length === 0
    ) {
      return (
        <div clssName="notregisterd-container">
          <div className="form-icon">
            <img
              src={IMG.GOOGLE_FORMS}
              style={{ width: '68%', marginTop: '25px' }}
              alt="Compnay"
            />
          </div>

          <p className="hiText">Hi There!</p>
          <p className="companyText">
            Looks like you’re not a registered user with Valyu.ai
          </p>
          <p className="companyText">
            If you are, please reach out to your organisation HR/Finance
            department for assistance.
          </p>
          <p className="companyText">
            Valyu.ai is a B2B app and we provide our solutions to employees by
            partnering with their organisations. If you wish to have this
            awesome employee benefit in your organisation, write to us at{' '}
            <a
              className="modal-link size"
              href="mailto:hello@valyu.ai"
              target="_blank"
              rel="noopener noreferrer"
            >
              hello@valyu.ai
            </a>{' '}
            or click on the link below to leave us your details.
          </p>

          <div className="btn-wrapper m-top btn-top">
            <Button
              className="verify-btn"
              type="button"
              onClick={() =>
                (window.location.href =
                  'https://docs.google.com/forms/d/e/1FAIpQLSfTUUxd94BonRlCwRW2GD7BsbRd7K9qoaogP6ClD0ntV6VRGg/viewform?usp=pp_url&entry.1766755535=8860074158')
              }
            >
              {' '}
              Get Valyu.Ai{' '}
            </Button>
          </div>
        </div>
      );
    }

    if (this.props?.clientList?.length > 0 && availableClient) {
      companiesTab = availableClient.map((company, index) => (
        <Button
          className={`company-btn ${
            isSelected === company?.id ? 'active' : ''
          }`}
          key={`company${index}`}
          onClick={() => this.onHandleSelectedCompaany(company)}
        >
          {company.logoUrl !== undefined ? (
            <Fragment>
              {company.logoUrl !== null &&
              company.logoUrl.includes('firebasestorage.googleapis.com') ? (
                <img
                  src={company.logoUrl}
                  className="animationImg"
                  alt="company logo"
                />
              ) : company.logoUrl !== null ? (
                <div className="lds-ring">
                  <div></div>
                  <div></div>
                  <div></div>
                </div>
              ) : null}
            </Fragment>
          ) : (
            <div className="logo"></div>
          )}
          {company?.id}
        </Button>
      ));
    }

    return (
      <div className="otp-verification-container">
        {redirected}

        <div className="page-icon">
          <img
            src={IMG.COMPANY}
            style={{ width: '80%', marginTop: '25px' }}
            alt="Compnay"
          />
        </div>

        <div className="heading-container">
          <h1 className="login-h1">Select Company</h1>
        </div>

        {this.props.isLoader && <Loader />}

        <form>
          <p className="label label-com">
            You are registered with multiple companies, select one to continue
          </p>

          {this.props?.companiesList && (
            <div className="btn-wrapper btn-wrapper-width companies-btn">
              {companiesTab}
              {isError && <p className="error-msg">Please select company</p>}
            </div>
          )}
          <p className="modal-link red-color">My company is not listed</p>

          {/*.............opt-input...............  */}

          <div className="btn-wrapper m-top btn-top">
            <Button
              className="verify-btn"
              type="button"
              onClick={() => this.onHandleContinue()}
            >
              {' '}
              Verify{' '}
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userDetails: _.get(state.global, 'userInfo', {}),
    isLoader: _.get(state.global, 'isLoader'),
    companiesList: _.get(state.global, 'companies'),
    clientList: _.get(state.global, 'clientList'),
    verifyRedirectPath: _.get(state.global, 'verifyRedirectPath', null),
    constant: _.get(state.global, 'constant', {}),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSetLoader: () => dispatch(actions.onAuthStart()),
    onUnsetLoader: () => dispatch(actions.onAuthEnd()),
    onSelectCompany: (payload) => dispatch(actions.onSetCompany(payload)),
    onManageAuthState: () => dispatch(actions.manageAuthState(3)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectCompany);
