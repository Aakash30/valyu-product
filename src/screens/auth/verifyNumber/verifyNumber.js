import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import _ from 'lodash';
import queryString from 'query-string';

import '../auth.scss';

import Input from '../../../components/UI/input/input';
import Loader from '../../../components/loader/loader';

import { routes } from '../../../routing/routes';
import { checkValidity, updateObject } from '../../../shared/shared';

import { convertArrayToString } from '../../../utils/common';
import { recaptchaFunction } from '../../../utils/firebase/firbaseRecaptcha';
import IMG from '../../../utils/images';

import * as actions from '../../../store/actions/index';
import { Redirect } from 'react-router-dom';

import AlertDialog from '../../../components/UI/modal/modal';

class OTPComponent extends Component {
  state = {
    otpForm: {
      input1: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          maxLength: 1,
          id: 1,
          onKeyUp: (event) => this.inputKeyUpHandler(event),
        },
        value: '',
        validation: {
          required: true,
          minLength: 1,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
      input2: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          maxLength: 1,
          id: 2,
          onKeyUp: (event) => this.inputKeyUpHandler(event),
        },
        value: '',
        validation: {
          required: true,
          minLength: 1,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
      input3: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          maxLength: 1,
          id: 3,
          onKeyUp: (event) => this.inputKeyUpHandler(event),
        },
        value: '',
        validation: {
          required: true,
          minLength: 1,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
      input4: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          maxLength: 1,
          id: 4,
          onKeyUp: (event) => this.inputKeyUpHandler(event),
        },
        value: '',
        validation: {
          required: true,
          minLength: 1,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
      input5: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          maxLength: 1,
          id: 5,
          onKeyUp: (event) => this.inputKeyUpHandler(event),
        },
        value: '',
        validation: {
          required: true,
          minLength: 1,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
      input6: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          maxLength: 1,
          id: 6,
          onKeyUp: (event) => this.inputKeyUpHandler(event),
        },
        value: '',
        validation: {
          required: true,
          minLength: 1,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
    },
    formIsValid: false,
    isSubmitted: true,
    isResend: true,
    count: 0,
    text: '',
    title: '',
    buttonName: 'Okay',
    isClose: true,
    isRedirect: false,
    isCheckboxValid: false,
  };

  async componentDidMount() {
    recaptchaFunction();
    await this.props.onManageAuthState();
  }

  // handle resend otp after 25 sec
  onSetResend = _.debounce(() => {
    this.setState({ isResend: false });
  }, 15 * 1000);

  // on handle time interval
  onSetTimeInterval = () => {
    let secCount = 15;
    let intervaliFun = setInterval(() => {
      secCount--;
      this.setState({ resendSec: secCount });
      if (secCount === 0) {
        clearInterval(intervaliFun);
      }
    }, 1000);
  };

  // handle input change
  inputChangedHandler = async (event, inputIdentifier) => {
    if (isNaN(event.target.value)) {
      return false;
    }

    const updatedFormElement = updateObject(
      this.state.otpForm[inputIdentifier],
      {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          this.state.otpForm[inputIdentifier].validation,
        ),
        touched: true,
      },
    );

    const updatedOrderForm = updateObject(this.state.otpForm, {
      [inputIdentifier]: updatedFormElement,
    });

    let formIsValid = true;
    for (let inputIdentifier in updatedOrderForm) {
      formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
    }

    await this.setState({ otpForm: updatedOrderForm, formIsValid });
  };

  //for confirm the code
  onHandleVerify = async (event) => {
    event.preventDefault();

    if (!this.state.formIsValid) {
      await this.setState({ isSubmitted: false });
      return;
    }
    // if (!this.state.isCheckboxValid) {
    //   await this.setState({ isSubmitted: false });
    //   return;
    // }

    this.props.onSetLoader();

    const formData = [];
    for (let formElementIdentifier in this.state.otpForm) {
      formData.push(this.state.otpForm[formElementIdentifier].value);
    }

    const arrToStr = await convertArrayToString(formData);

    await this.props.onConfirmCode(
      arrToStr,
      this.props?.userDetails?.phoneNumber,
      this.props?.userDetails?.deviceToken,
    );
  };

  // when user change the number
  onHandleChangeNumber = () => {
    this.props.onHandleChangeNumber();
  };

  // when otp did not receive
  onHandleResend = () => {
    let { count } = this.state;
    count++;

    if (count === 4) {
      this.props.onHandleChangeNumber();
      return;
    }

    this.setState({ isResend: true, count });
    this.props.onSetLoader();
    this.props.sendOtpToPhone(this.props.userDetails.phoneNumber);
  };

  // close modal
  onHandleCloseAlert = () => {
    this.setState({ isDialog: false, text: '', title: '', isRedirect: true });
  };

  // on handle key up functionality
  inputKeyUpHandler = (event) => {
    if (event.keyCode === 8) {
      return;
    }

    if (event.target.nextSibling) {
      event.target.nextSibling.focus();
    }
  };

  onHandleCheckBox = (e, inputElm) => {
    this.setState({ isCheckboxValid: e.target.checked });
  };

  render() {
    const {
      isSubmitted,
      formIsValid,
      isDialog,
      text,
      title,
      isClose,
      buttonName,
      isRedirect,
    } = this.state;

    const formElementsArray = [];
    for (let key in this.state.otpForm) {
      formElementsArray.push({
        id: key,
        config: this.state.otpForm[key],
      });
    }

    let form = formElementsArray.map((formElement) => (
      <Input
        key={formElement.id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        class={`otp-input`}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        isSubmitted={true}
        changed={(event) => this.inputChangedHandler(event, formElement.id)}
      />
    ));

    let errorElement = '';

    if (this.props.isInvalidCode && isSubmitted && formIsValid) {
      errorElement = <p className="error-msg">{this.props.errorMsg}</p>;
    }

    if (!isSubmitted && !formIsValid) {
      errorElement = <p className="error-msg">OTP entered is not correct.</p>;
    }

    let authRedirect = null;
    if (
      this.props.isAuth &&
      !this.props.isChangeNumber &&
      this.props?.redirectPath !== null
    ) {
      authRedirect = <Redirect to={this.props.redirectPath} />;
    }

    if (this.props.isChangeNumber || isRedirect) {
      authRedirect = <Redirect to={routes.Login} />;
    }

    let alertPopup = '';
    if (isDialog) {
      alertPopup = (
        <AlertDialog
          open={true}
          text={text}
          title={title}
          onHandleCloseAlert={() => this.onHandleCloseAlert()}
          buttonName={buttonName}
          isClose={isClose}
        />
      );
    }

    return (
      <div className="otp-verification-container">
        {authRedirect}

        {alertPopup}

        <div className="page-icon">
          <img
            src={IMG.VERIFY_MOBILE}
            className="img-logo img-wid"
            alt="VERIFY_MOBILE"
          />
        </div>

        <div className="heading-container">
          <h1 className="login-h1">Verification</h1>
        </div>

        {this.props.isLoader && <Loader />}

        <form onSubmit={(e) => this.onHandleVerify(e)}>
          <p className="label">Enter the OTP sent to your mobile</p>
          <div className="otp-wrapper">{form}</div>

          {errorElement}

          <p
            className="modal-link mb-resend"
            onClick={() => this.onHandleResend()}
          >
            Send this code again
          </p>
          <p
            className="modal-link red-color"
            onClick={() => this.onHandleChangeNumber()}
          >
            Not your phone number?
          </p>

          {/* <p className="resend">Resending in {resendSec} seconds</p> */}

          {/*.............opt-input...............  */}

          <div className="btn-wrapper btn-top">
            <Button className="verify-btn " type="submit">
              {' '}
              Verify{' '}
            </Button>
            <p className="label full-width aggrement">
              By clicking verify you agree our{' '}
              <a
                className="modal-link size"
                href="https://valyu.ai/terms_and_condition.pdf"
                target="_blank"
                rel="noopener noreferrer"
              >
                Terms & Conditions
              </a>
              {''} and {''}
              <a
                className="modal-link size"
                href="https://valyu.ai/privacy_policy.pdf"
                target="_blank"
                rel="noopener noreferrer"
              >
                Privacy Policy
              </a>
              {''} and{' '}
              <a
                className="modal-link size"
                href="https://valyu.ai/Cookie_policy.pdf"
                target="_blank"
                rel="noopener noreferrer"
              >
                Cookie Policy
              </a>{' '}
              .
            </p>
          </div>
        </form>

        <div id="recaptcha-container"></div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userDetails: _.get(state.global, 'userInfo', {}),
    isLoader: _.get(state.global, 'isLoader'),
    isAuth: state.global.userInfo.userId !== null,
    isChangeNumber: _.get(state.global, 'isChangePhone'),
    redirectPath: _.get(state.global, 'verifyRedirectPath'),
    userProfileDetail: _.get(state.global, 'userData'),
    companiesList: _.get(state.global, 'companies'),
    isUserProfile: _.get(state.global, 'isUserProfile', false),
    isInvalidCode: _.get(state.global, 'isInvalidCode', false),
    errorMsg: _.get(state.global, 'error', null),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSetLoader: () => dispatch(actions.onAuthStart()),
    onUnsetLoader: () => dispatch(actions.onAuthEnd()),
    sendOtpToPhone: (data) => dispatch(actions.sendOtpToPhone(data)),
    onConfirmCode: (data, number, payload) =>
      dispatch(actions.confirmCode(data, number, payload)),
    onHandleChangeNumber: () => dispatch(actions.onChangeNumber()),
    onManageAuthState: () => dispatch(actions.manageAuthState(2)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(OTPComponent);
