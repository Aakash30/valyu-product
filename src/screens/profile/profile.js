import React, { useState, useEffect, useReducer } from 'react';
import './profile.scss';
import IMG from '../../utils/images';
import { useDispatch, useSelector } from 'react-redux';
import InfoField from '../../components/InfoField/InfoField';
import { getFullName, maskString, naCheck } from '../../utils/common';
import * as firebase from 'firebase';
import * as actions from '../../store/actions/index';
import { BASE_URL } from '../../shared/apiConstant';

function Profile() {
  const dispatch = useDispatch();
  const forceUpdate = useReducer((bool) => !bool)[1];
  const userData = useSelector((state) => state.global.userData);
  const userInfo = useSelector((state) => state.global.userInfo);
  const companyList = useSelector((state) => state.global.clientList);
  const global = useSelector((state) => state.global);
  const [aadharUrl, setAadharUrl] = useState('');
  const [panCardUrl, setPanCardUrl] = useState('');
  const { phoneNumber } = userInfo;
  console.log(userData);
  const {
    firstName,
    lastName,
    profilePicUrl,
    gender,
    fathername,
    email,
    dob,
    motherName,
    address1,
    address2,
  } = userData.userProfile;
  const {
    panNumber,
    aadharNumber,
    aadharImagePath,
    panImagePath,
  } = userData.personal;

  const [imageUrl, setImageUrl] = useState('');
  const [companiesData, setCompaniesData] = useState([]);

  useEffect(() => {
    let tempArr = [];
    companyList.forEach((item) => {
      if (item.logoUrl.includes('firebasestorage.googleapis.com')) {
        tempArr.push({
          name: item.id,
          companyLogoUrl: item.logoUrl,
        });
        setCompaniesData(tempArr);
      } else {
        firebase
          .storage()
          .ref(item.logoUrl)
          .getDownloadURL()
          .then((url) => {
            tempArr.push({
              name: item.id,
              companyLogoUrl: url,
            });
            setCompaniesData(tempArr, () => {
              forceUpdate();
            });
          })
          .catch((error) => {
            console.error(error);
          });
      }
    });
    if (aadharImagePath) {
      getDownloadUrlForAadhar(aadharImagePath);
    }
    if (panImagePath) {
      getDownloadUrlForPanCard(panImagePath);
    }
  }, [companyList]);

  const getDownloadUrlForAadhar = (path) => {
    firebase
      .storage()
      .ref(path)
      .getDownloadURL()
      .then((url) => {
        setAadharUrl(url);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const getDownloadUrlForPanCard = (path) => {
    firebase
      .storage()
      .ref(path)
      .getDownloadURL()
      .then((url) => {
        setPanCardUrl(url);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const getWorkProfileData = () => {
    let organisationData = Object.keys(userData && userData.workProfile);
    let tempArr = [];
    organisationData.forEach((item) => {
      tempArr.push({
        companyName: item,
        data: Object.values(userData && userData.workProfile),
      });
    });
    return tempArr;
  };

  useEffect(() => {
    if (!imageUrl && profilePicUrl) {
      firebase
        .storage()
        .ref(profilePicUrl)
        .getDownloadURL()
        .then((url) => {
          setImageUrl(url);
        })
        .catch((error) => {
          console.error(error);
        });
    }

    dispatch(actions.onHandleActive('profile'));
  }, []);

  return (
    <section>
      {/* <Header/>
          <Nav active="profile"/> */}
      <div className="root-container container-profiles">
        <div className="top-heading">About you</div>
        <div className="user-info-container">
          <div className="avatar-container">
            <img
              className="img"
              alt="Avatar"
              src={imageUrl || IMG.USER_AVATAR}
            ></img>
          </div>
          <div className="info-container">
            <div className="info-container first-column">
              <InfoField
                label="Full Name"
                value={naCheck(getFullName(firstName, lastName))}
              />
              <InfoField label="Mobile" value={naCheck(phoneNumber)} />
              <InfoField label="Gender" value={naCheck(gender)} />
              <InfoField label="Father’s Name" value={naCheck(fathername)} />
              <InfoField label="Aadhar Number" value={naCheck(aadharNumber)} />
              <InfoField label="Aadhar Link" type="link" link={aadharUrl} />
            </div>
            <div className="info-container second-column">
              <InfoField label="Email ID" value={naCheck(email)} />
              <InfoField label="DOB" value={naCheck(dob)} />
              <InfoField label="PAN Number" value={naCheck(panNumber)} />
              <InfoField label="Mother’s Name" value={naCheck(motherName)} />
              <InfoField
                label="Address"
                value={naCheck(getFullName(address1, address2))}
              />
              <InfoField label="Pan Card Link" type="link" link={panCardUrl} />
            </div>
          </div>
        </div>
        <div className="company-details">
          {getWorkProfileData().map((item, index) => {
            const {
              emailId,
              designation,
              city,
              employmentType,
              doj,
              salary,
              officePincode,
              employeeId,
              officeAddress,
            } = item.data[0].userData;
            const { bankName, bankAcNo } = item.data[0].account;

            return (
              <div className="company-details-container">
                <div className="company-details-heading">
                  <h1>Company Details</h1>
                </div>
                <div className="logo-container">
                  {companiesData.length > 0 &&
                  companiesData[index] &&
                  companiesData[index].name === item.companyName &&
                  companiesData[index].companyLogoUrl ? (
                    <img
                      className="logo-image"
                      src={
                        companiesData.length > 0 &&
                        companiesData[index] &&
                        companiesData[index].name === item.companyName &&
                        companiesData[index].companyLogoUrl
                      }
                      alt="img"
                    />
                  ) : (
                    <div className="circle"></div>
                  )}
                  <p className="company-name">{item.companyName}</p>
                </div>
                <div className="details-container">
                  <div className="column-first">
                    <InfoField label="Company Email" value={naCheck(emailId)} />
                    <InfoField
                      label="Designation"
                      value={naCheck(designation)}
                    />
                    <InfoField
                      label="Employee Code"
                      value={naCheck(employeeId)}
                    />
                    <InfoField label="City" value={naCheck(city)} />
                    <InfoField label="Bank Name" value={naCheck(bankName)} />
                    <InfoField label="Address" value={naCheck(officeAddress)} />
                  </div>
                  <div className="column-second">
                    <InfoField
                      label="Employment Type"
                      value={naCheck(employmentType)}
                    />
                    <InfoField label="Join Date" value={naCheck(doj)} />
                    <InfoField label="Monthly Salary" value={naCheck(salary)} />
                    <InfoField label="Pincode" value={naCheck(officePincode)} />
                    <InfoField
                      label="Bank A/C"
                      value={naCheck(maskString(bankAcNo))}
                    />
                    <InfoField
                      label="Virtual A/C"
                      value={naCheck(
                        maskString(
                          userInfo?.selectedCompany?.userData
                            ?.virtual_account_no,
                        ),
                      )}
                    />
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
}

export default Profile;
