import React, { useCallback } from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DataGrid from '../../components/dataGrid/DataGrid';
import * as actions from '../../store/actions/index';
import './transection.scss';

function Transactions(props) {
  const userData = useSelector((state) => state.global.userData);

  const dispatch = useDispatch();
  const stableDispatch = useCallback(dispatch, []); // it doesn't need to change

  useEffect(() => {
    stableDispatch(actions.onHandleActive('transection'));
  }, [stableDispatch]);

  return (
    <section>
      <div className="transection-container">
        <DataGrid data={userData.Loan} />
      </div>
    </section>
  );
}

export default Transactions;
