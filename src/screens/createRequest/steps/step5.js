import React, { Component } from 'react';
import * as moment from 'moment';
import _ from 'lodash';
import * as firebase from 'firebase';

import FormControlLabelPlacement from '../../../components/UI/radio&checkbox/radio-button';
import AlertDialog from '../../../components/UI/modal/modal';
import { checkValidity, updateObject } from '../../../shared/shared';
import Input from '../../../components/UI/input/input';
import { recaptchaFunction } from '../../../utils/firebase/firbaseRecaptcha';
import {
  convertArrayToString,
  base64StringtoFile,
  maskString,
} from '../../../utils/common';
import { BASE_URL } from '../../../shared/apiConstant';
import html2pdf from 'html2pdf.js';
import subscriptionHtml from '../../../assets/clickwrapSubscription.html';
import payperViewHtml from '../../../assets/clickWrap.html';

let html = '';

class RequestStep5 extends Component {
  async componentDidMount() {
    await recaptchaFunction();
    await this.props.onEnableButton();
    await this.props.onLoaderStart();
    if (this.props.isSubscriptionEmploye) {
      html = subscriptionHtml;
    } else {
      html = payperViewHtml;
    }
    setTimeout(async () => {
      await this.updateLoanAggrementData();
      await this.props.onLoaderStop();
    }, 300);
  }

  updateLoanAggrementData() {
    const companyObj = _.keyBy(this.props?.clientList, 'id');

    const selectedCompany = this.props?.userDetails?.selectedCompany;

    let findObj = companyObj[selectedCompany?.id];

    this.setState({ companyObj: findObj });

    html = html
      .replaceAll(
        'NAME_OF_BORROWER',
        this.props?.requestLoanData?.firstName +
          ' ' +
          this.props?.requestLoanData?.lastName,
      )
      .replaceAll('PAN_OF_BORROWER', this.props?.requestLoanData?.panNumber)
      .replaceAll(
        'AADHAR_OF_BORROWER',
        maskString(this.props?.requestLoanData?.aadharNumber),
      )
      .replaceAll(
        'DOB_OF_BORROWER',
        moment(this.props?.requestLoanData?.dob).format('DD-MMM-YYYY'),
      )
      .replaceAll('CONTACT_OF_BORROWER', this.props?.userDetails?.phoneNumber)
      .replaceAll(
        'MARITAL_STATUS_OF_BORROWER',
        this.props?.userData?.userProfile.matrialStatus ?? 'None',
      )
      .replaceAll(
        'EMAIL_ID_OF_BORROWER',
        this.props?.userDetails?.selectedCompany?.userData?.emailId ??
          this.props?.requestLoanData?.officialEmail,
      )
      .replaceAll(
        'GENDER_OF_BORROWER',
        this.props?.userData?.userProfile?.gender ??
          this.props?.requestLoanData?.gender,
      )
      .replaceAll(
        'LOAN_ADDRESS',
        this.props?.userDetails?.selectedCompany?.userData?.officeAddress ??
          this.props?.requestLoanData?.currentAddressLine1,
      )
      .replaceAll(
        'LANDMARK_ADDRESS',
        this.props?.requestLoanData?.currentAddressLine1 +
          ' ' +
          this.props?.requestLoanData?.currentAddressLine2,
      )
      .replaceAll(
        'LANDMARK_PINCODE',
        this.props?.requestLoanData?.currentPincode,
      )
      .replaceAll('LANDMARK_CITY', this.props?.requestLoanData?.city?.value)
      .replaceAll(
        'LANDMARK_STATE',
        this.props?.requestLoanData?.currentState?.value,
      )
      .replaceAll(
        'LOAN_AMOUNT',
        (this.props?.requestLoanData?.applyLoanAmount).toFixed(2),
      )
      .replaceAll(
        'ACTUAL_AMOUNT',
        (this.props?.requestLoanData?.amount).toFixed(2),
      )
      .replaceAll('PURPOSE_OF_LOAN', this.props?.requestLoanData?.reason?.value)
      .replaceAll(
        'LOAN_TENURE',
        this.props?.requestLoanData?.loanTenure + ' days',
      )
      .replaceAll('LOAN_BANK_NAME', this.props?.requestLoanData?.bankName)
      .replaceAll('LOAN_BANK_ACCOUNT', this.props?.requestLoanData?.bankAcNo)
      .replaceAll(
        'LOAN_BANK_IFSC',
        this.props?.userDetails?.selectedCompany?.account?.ifscCode ??
          this.props?.requestLoanData?.ifscCode,
      )
      .replaceAll('LOAN_PRODUCT_NAME', 'Valyu - Credit Line')
      .replaceAll(
        'LOAN_RATE_OF_INTEREST',
        this.props?.requestLoanData?.interest_rate + '%',
      )
      .replaceAll(
        'LOAN_DUE_DATE',
        moment(this.props?.requestLoanData?.repaymentDate).format(
          'DD-MMM-YYYY',
        ),
      )
      .replaceAll('INTERSET_METHOD', 'Daily Rest')
      .replaceAll('TIME_STAMP', moment().format('DD-MMM-YYYY hh:mm:ss'))
      .replaceAll('LOAN_OTP', this.props?.verifyOTP)
      .replaceAll('LOAN_ID', this.props?.requestLoanData.id)
      .replaceAll('LOAN_DATE', moment().format('DD-MMM-YYYY'))
      .replaceAll('EDI_DATE', moment().format('DD-MMM-YYYY'))
      .replaceAll(
        'LOAN_TOTAL_AMMOUNT',
        parseFloat(this.props?.requestLoanData?.totalRepayment).toFixed(2),
      )
      .replaceAll('LOAN_MOBILE_NUMBER', this.props?.userDetails?.phoneNumber)
      .replaceAll(
        'LOAN_ADDRESS',
        this.props?.userDetails?.selectedCompany?.userData?.officeAddress ??
          this.props?.requestLoanData?.currentAddressLine1,
      )
      .replaceAll('LOAN_AGREEMENT_NO', this.props?.requestLoanData.id)
      .replaceAll(
        'PROCESSING_FEES',
        (this.props?.requestLoanData?.processing_fee).toFixed(2),
      )
      .replaceAll(
        'PARTNER_FEE',
        (
          this.props?.requestLoanData?.processing_fee +
          this.props?.requestLoanData?.platformFee
        ).toFixed(2),
      )
      .replaceAll('BUSINESS_OF_BORROWER', findObj?.description)
      .replaceAll('STAMPING_CHARGES', 0)
      .replaceAll(
        'PLATFORM_FEES',
        (this.props?.requestLoanData?.platformFee).toFixed(2),
      )
      .replaceAll('LATE_PAYMENT_CHARGES', 0)
      .replaceAll('PREPAYMENT_CHARGES', 0)
      .replaceAll('LEGAL_CHARGES', 0)
      .replaceAll('NUMBER_OF_EDI', 1)
      .replaceAll('PAYMENT_MODE', 'Online')
      .replaceAll('LOAN_RATE_OF_INTERESET_PER_ANNUM', 0)
      .replaceAll('LOAN_USER_LOCATION', '---')
      .replaceAll('BUSINESS_PHONE', 'BUSINESS_PHONE')
      .replaceAll('LOAN_MONTH_DATE', moment().format('MMMM DD, YYYY'));
  }

  state = {
    forms: {
      condition_1: {
        elementType: 'checkbox',
        elementConfig: {
          name: 'isOTP',
        },
        value: false,
        options: [
          {
            value: 'isOTP',
            label: this.props?.UIConstant?.loanAgreement?.condition1,
          },
        ],
        validation: {
          required: true,
        },
        valid: false,
        touched: false,
      },
      condition_2: {
        elementType: 'checkbox',
        elementConfig: {
          name: 'isAgree',
        },
        value: false,
        options: [
          {
            value: 'isAgree',
            label: this.props?.UIConstant?.loanAgreement?.condition2
              .replace(
                '[Client Name]',
                this.props?.userDetails?.selectedCompany?.name,
              )
              .replace(
                '[Total Settelment Amount]',
                this.props?.requestLoanData?.totalRepayment,
              ),
          },
        ],
        validation: {
          required: true,
        },
        valid: false,
        touched: false,
      },
      condition_3: {
        elementType: 'checkbox',
        elementConfig: {
          name: 'isAgree',
        },
        value: false,
        options: [
          {
            value: 'isAgree',
            label: this.props?.UIConstant?.loanAgreement?.condition3,
          },
        ],
        validation: {
          required: true,
        },
        valid: false,
        touched: false,
      },
    },
    otpForm: {
      input1: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          maxLength: 1,
          id: 1,
          onKeyUp: (event) => this.inputKeyUpHandler(event),
        },
        value: '',
        validation: {
          required: true,
          minLength: 1,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
      input2: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          maxLength: 1,
          id: 2,
          onKeyUp: (event) => this.inputKeyUpHandler(event),
        },
        value: '',
        validation: {
          required: true,
          minLength: 1,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
      input3: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          maxLength: 1,
          id: 3,
          onKeyUp: (event) => this.inputKeyUpHandler(event),
        },
        value: '',
        validation: {
          required: true,
          minLength: 1,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
      input4: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          maxLength: 1,
          id: 4,
          onKeyUp: (event) => this.inputKeyUpHandler(event),
        },
        value: '',
        validation: {
          required: true,
          minLength: 1,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
      input5: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          maxLength: 1,
          id: 5,
          onKeyUp: (event) => this.inputKeyUpHandler(event),
        },
        value: '',
        validation: {
          required: true,
          minLength: 1,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
      input6: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          maxLength: 1,
          id: 6,
          onKeyUp: (event) => this.inputKeyUpHandler(event),
        },
        value: '',
        validation: {
          required: true,
          minLength: 1,
          isNumeric: true,
        },
        valid: false,
        touched: false,
      },
    },
    isSubmitted: true,
    formIsValid: false,
    isDialog: false,
    isClose: false,
    title: `You are just 30 seconds away!`,
    text: 'Enter verification code',
    text_1:
      'We have a just sent a verification code to your phone number +91' +
      this.props?.userDetails?.phoneNumber,
    text_2: 'Send this code again',
    text_3: 'Not your phone number?',
    buttonName: 'Verify',
    isUpdateComponent: false,
  };

  // on handle key up functionality
  inputKeyUpHandler = (event) => {
    if (event.keyCode === 8) {
      return;
    }

    if (event.target.nextSibling) {
      event.target.nextSibling.focus();
    }
  };

  // function call when user enter value in input
  inputChangedHandler = async (event, controlName, inputIdentifier) => {
    let { isClose } = this.state;

    const updatedFormElement = updateObject(
      this.state[controlName][inputIdentifier],
      {
        value: event.target.value ?? event.target.checked,
        valid: checkValidity(
          event.target.value ?? event.target.checked,
          this.state[controlName][inputIdentifier].validation,
        ),
        touched: true,
      },
    );

    const updatedOTPForm = updateObject(this.state[controlName], {
      [inputIdentifier]: updatedFormElement,
    });

    let formIsValid = true;
    for (let inputIdentifier in updatedOTPForm) {
      formIsValid = updatedOTPForm[inputIdentifier].valid && formIsValid;
    }

    if (controlName === 'otpForm') {
      isClose = formIsValid;
    }

    await this.setState({ otpForm: updatedOTPForm, formIsValid, isClose });
  };

  // Function for check step is valid or not
  onHandleContinue = () => {
    if (this.state.formIsValid) {
      this.setState({ isDialog: true, isInvalid: false });
      this.props.onSentOTPFun();
    } else {
      this.setState({ isInvalid: true });
    }

    return this.state.formIsValid && this.state.isClose;
  };

  // Function for handle check box
  onHandleCheckBox = (e, inputElm) => {
    let forms = { ...this.state.forms };
    forms[inputElm].value = e.target.checked;
    forms[inputElm].valid = e.target.checked;

    let formIsValid = true;
    for (let inputIdentifier in forms) {
      formIsValid = forms[inputIdentifier].valid && formIsValid;
    }

    this.setState({ forms, formIsValid });
  };

  // download and store pdf file
  onHandleVerifyAlert = () => {
    this.props.onLoaderStart();
    this.updateLoanAggrementData();
    this.setState({ isDialog: false, text: '', title: '' });
    this.downloadFile();
  };

  // send OTP on number
  onConfirmCode = async () => {
    let { otpForm } = this.state;

    let formData = [];
    for (let inputIdentifier in otpForm) {
      formData.push(otpForm[inputIdentifier].value);
    }

    const arrayToString = convertArrayToString(formData);

    this.props.onHandleConfirmCode(arrayToString);
  };

  downloadFile = async () => {
    this.props.onLoaderStart();

    const element = document.getElementById('wrapperContent');

    let opt = {
      margin: 0.46,
      filename: `${this.props?.requestLoanData.id}_aggrement.pdf`,
      image: { type: 'jpeg', quality: 0.98 },
      html2canvas: { scale: 2 },
      jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' },
    };

    let isDownloadPDF = false;

    html2pdf()
      .set(opt)
      .from(element)
      .outputPdf()
      .then((pdf) => {
        // This logs the right base64
        if (!isDownloadPDF) {
          isDownloadPDF = true;
          this.storePDF(btoa(pdf));
        }
      })
      .catch((error) => {
        this.props.onLoaderStop();
      })
      .save();
  };

  storePDF = async (pdf) => {
    this.props.onLoaderStart();

    const fileName = `${this.props.requestLoanData.id}.pdf`;

    const link = `/${BASE_URL.LOANDOCUMENTS}/${this.props.userDetails.phoneNumber}/${fileName}`;

    const blob = await base64StringtoFile(pdf, fileName);

    const uploadTask = firebase.storage().ref(link).put(blob, {});
    //initiates the firebase side uploading
    uploadTask.on(
      'state_changed',
      (snapShot) => {
        //takes a snap shot of the process as it is happening
      },
      (err) => {
        this.props.onLoaderStop();
        //catches the errors
      },
      () => {
        this.props.onSetLoanData(link, this.state.companyObj);
      },
    );
  };

  // Function for handle scroll and activate next step button
  handleScroll = (event) => {
    const target = event.target;

    // Check current scroll position
    if (target.clientHeight + target.scrollTop === target.scrollHeight) {
      this.props.onDisableButton();
    }
  };

  render() {
    const {
      forms,
      otpForm,
      isDialog,
      text,
      text_2,
      title,
      isClose,
      buttonName,
      text_1,
      isInvalid,
    } = this.state;

    const formElementsArray = [];
    for (let key in otpForm) {
      formElementsArray.push({
        id: key,
        config: otpForm[key],
      });
    }

    let form = formElementsArray.map((formElement) => (
      <Input
        key={formElement.id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        class={`otp-input`}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        isSubmitted={true}
        changed={(event) =>
          this.inputChangedHandler(event, 'otpForm', formElement.id)
        }
      />
    ));

    let alertPopup = '';

    if (isDialog) {
      alertPopup = (
        <AlertDialog
          open={true}
          text={text}
          text_1={text_1}
          title={title}
          onHandleCloseAlert={() => this.onHandleVerifyAlert()}
          buttonName={buttonName}
          isClose={isClose}
          isOTP={true}
          form={form}
          {...this.props}
          errorMsg={this.props.errorMsg}
          confirmCodeFun={() => this.onConfirmCode()}
          isInvalidCode={this.props.isInvalidCode}
          isConfirmCode={this.props.isConfirmCode}
          text_2={text_2}
          onSendOTPToFun={() => this.props.onSentOTPFun()}
        />
      );
    }

    let errorElement = '';
    if (isInvalid) {
      errorElement = <p className="error-msg">Required</p>;
    }

    return (
      <div className="five-step">
        {/* onClick={() => this.downloadFile()} */}
        <div className="mainDiv">
          <div className="contentDiv" onScroll={this.handleScroll}>
            {/* <p>Date: {moment().format('DD/MM/YYYY')}</p> */}
            <div>
              {/* <p id="ignorePDF" onClick={this.downloadFile}>LOAN AGREEMENT</p> */}
              <div id="loanData" dangerouslySetInnerHTML={{ __html: html }} />
            </div>
          </div>
        </div>

        <FormControlLabelPlacement
          change={(event) => this.onHandleCheckBox(event, 'condition_1')}
          {...forms.condition_1}
        />
        {!forms.condition_1.valid && isInvalid && errorElement}
        <FormControlLabelPlacement
          change={(event) => this.onHandleCheckBox(event, 'condition_2')}
          {...forms.condition_2}
        />
        {!forms.condition_2.valid && isInvalid && errorElement}
        <FormControlLabelPlacement
          change={(event) => this.onHandleCheckBox(event, 'condition_3')}
          {...forms.condition_3}
        />
        {!forms.condition_3.valid && isInvalid && errorElement}

        {alertPopup}

        <div id="recaptcha-container"></div>
      </div>
    );
  }
}

export default RequestStep5;
