import React, { memo } from 'react';
import { Fragment } from 'react';
import * as moment from 'moment';

const PerPayComponent = memo((props) => {
  const {
    interestAmount,
    processing_fee,
    plateform_fees,
    interestRate,
    plateform_fees_value,
    totalRepayment,
    payOutDate,
    processing_fee_value,
  } = props;

  return (
    <Fragment>
      <div className="main-input-box main-input-box-custom justify-content">
        <div className="input-wrapper input-wrapper-custom input-min-wrapper">
          <div className="heading">Applicable Charges</div>
        </div>
        <div className="input-wrapper input-wrapper-custom ">
          <div className="amount">
            ₹{' '}
            {parseFloat(
              interestAmount + processing_fee + plateform_fees,
            ).toFixed(2)}
          </div>
        </div>
      </div>
      <div className="box-charges">
        <div className="box-charges-inner">
          <div className="input-wrapper input-wrapper-custom input-min-wrapper">
            <div className="heading">Interest Fee</div>
          </div>
          <div className="input-wrapper input-wrapper-custom">
            <div className="amount">₹ {interestAmount}</div>
          </div>
          <div className="input-wrapper input-wrapper-custom">
            <div className="sub-heading">
              {interestRate}% per/day of Withdrawn amount
            </div>
          </div>
        </div>
        <div className="box-charges-inner">
          <div className="input-wrapper input-wrapper-custom input-min-wrapper">
            <div className="heading">Processing Fees</div>
          </div>
          <div className="input-wrapper input-wrapper-custom">
            <div className="amount">
              ₹ {parseFloat(processing_fee).toFixed(2)}
            </div>
          </div>
          <div className="input-wrapper input-wrapper-custom">
            <div className="sub-heading">
              {processing_fee_value}% of Transaction value (incl. GST)
            </div>
          </div>
        </div>
        <div className="box-charges-inner">
          <div className="input-wrapper input-wrapper-custom input-min-wrapper">
            <div className="heading">Platform Fee</div>
          </div>
          <div className="input-wrapper input-wrapper-custom">
            <div className="amount">
              ₹ {parseFloat(plateform_fees).toFixed(2)}
            </div>
          </div>
          <div className="input-wrapper input-wrapper-custom">
            <div className="sub-heading">
              Flat ₹ {plateform_fees_value}/Transaction (incl. GST)
            </div>
          </div>
        </div>
        <div className="box-charges-inner">
          <div className="input-wrapper input-wrapper-custom input-min-wrapper">
            <div className="heading">
              Repayment of {totalRepayment} will be due on{' '}
              {moment(payOutDate).format('DD MMMM, YYYY')}
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
});

export default PerPayComponent;
