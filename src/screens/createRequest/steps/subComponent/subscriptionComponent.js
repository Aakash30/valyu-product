import React, { memo } from 'react';
import * as moment from 'moment';
import * as _ from 'lodash';

const SubscriptionComponent = memo((props) => {
  const {
    interestAmount,
    processing_fee,
    plateform_fees,
    settlementDate,
    plateform_fees_value,
    totalRepayment,
    payOutDate,
    UIConstant,
    totalSuccessfullApplication,
    clientObj,
  } = props;

  let transactionText = '';
  let replaceText = '';
  let loanLength = totalSuccessfullApplication;
  if (loanLength === 0) {
    transactionText = _.get(
      UIConstant,
      'transactionHistoryText.transactionText1',
      '',
    );
    replaceText = transactionText
      .replace('[ALLOWEDTRANSACTION]', clientObj.maxAllowedTransaction)
      .replace('[TRANSCATIONCOUNT]', loanLength + 1)
      .replace(
        '[STARTDATE]',
        moment(clientObj.blackoutEndDate).format('Do MMM'),
      )
      .replace(
        '[ENDDATE]',
        moment(clientObj.blackoutStartDate).format('Do MMM'),
      );
  } else {
    transactionText = _.get(
      UIConstant,
      'transactionHistoryText.transactionText2',
      '',
    );
    replaceText = transactionText.replace('[TRANSCATIONCOUNT]', loanLength + 1);
  }

  return (
    <div className="interest-paragrah">
      <div className="">
        <span className="bold-text">
          Monthly flat fee for {moment(settlementDate).format('MMMM YYYY')} is
        </span>
        <span className="feeCss">
          {loanLength === 0 ? ' ₹ ' + Math.ceil(clientObj.fee) : ' Its free!'}
        </span>
        <p className="text-line-height">{replaceText}</p>
        {/* <span className="infoIcon"><img alt="icon" src={IMG.INF0_ICON} /></span> */}
      </div>
      <div className="">
        <p className="bold-text">
          The total repayment amount of{' '}
          <span className="feeCss">₹ {Math.ceil(totalRepayment)}</span>
        </p>
        <p className="text-line-height">
          will be deducted from your next payroll (due on{' '}
          <span className="feeCss">
            {moment(payOutDate).format('DD MMM YYYY')}
          </span>
          )
        </p>
      </div>
    </div>
  );
});

export default SubscriptionComponent;
