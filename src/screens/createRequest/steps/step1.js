import React, { Component } from 'react';

import _ from 'lodash';
import * as moment from 'moment';

// UI component
import Input from '../../../components/UI/input/input';
import { checkValidity, updateObject } from '../../../shared/shared';
import RequestConstant from '../createRequestConstant.json';
import IMG from '../../../utils/images';


class RequestStep1 extends Component {

    render() {

        return (
            <div className="first-step">
                <div className="table-wrapper">
                    <div className="wid-40">
                        <div className="main-box">
                            <div className="category">Loan Type</div>
                            <div className="value text-trasform">{this.props?.requestLoadData?.selectLoanType}</div>
                        </div>
                        <div className="main-box">
                            <div className="category">Request Amount</div>
                            <div className="value">
                                &#8377; {this.props?.requestLoadData?.amount}
                            </div>
                        </div>
                        <div className="main-box">
                            <div className="category">Fees (Process, Platform etc)</div>
                            <div className="value">&#x20B9;50</div>
                        </div>
                        <div className="main-box">
                            <div className="category">Interset Amount <sup style={{ fontSize: '9px' }}>0.065% per day</sup></div>
                            <div className="value">&#x20B9;50</div>
                        </div>
                        <div className="main-box">
                            <div className="category">Repayable Amount</div>
                            <div className="value">&#x20B9; 10,100</div>
                        </div>
                    </div>

                    <div className="wid-40">
                        <div className="main-box">
                            <div className="category">Purpose</div>
                            <div className="value text-trasform">{this.props?.requestLoadData?.reason?.value}</div>
                        </div>
                        <div className="main-box">
                            <div className="category">Date</div>
                            <div className="value">{moment().format('DD MMM YYYY')}</div>
                        </div>
                        <div className="main-box">
                            <div className="category">Time</div>
                            <div className="value">{moment().format('hh:mm a')}</div>
                        </div>
                        <div className="main-box">
                            <div className="category">Settlement Date</div>
                            <div className="value">30 May 2020</div>

                        </div>
                        <div className="main-box">
                            <div className="category">Freeze Period</div>
                            <div className="value">20 Days</div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

export default RequestStep1;