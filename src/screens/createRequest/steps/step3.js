import React, { Component } from 'react';

import { checkValidity, updateObject } from '../../../shared/shared';

import Input from '../../../components/UI/input/input';

import RequestConstant from '../createRequestConstant.json';

import * as firebase from 'firebase';

import IMG from '../../../utils/images';

import { BASE_URL } from '../../../shared/apiConstant';

class RequestStep3 extends Component {
  state = {
    otpVerification: {
      elementType: 'radio',
      value: '',
      options: [
        { value: 'isOTP', label: RequestConstant.AADHAR_VERIFICATION.TEXT },
      ],
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    personal_form: {
      aadharNumber: {
        elementType: 'input',
        label: 'Aadhar Number',
        isRequired: true,
        elementConfig: {
          type: 'text',
          placeholder: 'Aadhar Number',
          minLength: 14,
          maxLength: 14,
          readOnly: this.props.requestLoanData?.aadharNumber ? true : false,
        },
        value: this.props.requestLoanData?.aadharNumber ?? '',
        validation: {
          required: true,
          minLength: 12,
          maxLenght: 12,
          isTrim: true,
        },
        valid: false,
        touched: false,
      },
      panNumber: {
        elementType: 'input',
        label: 'PAN Number',
        isRequired: true,
        elementConfig: {
          type: 'text',
          placeholder: 'PAN Number',
          readOnly: this.props.requestLoanData?.panNumber ? true : false,
        },
        value: this.props.requestLoanData?.panNumber ?? '',
        validation: {
          required: true,
        },
        valid: false,
        touched: false,
      },
    },
    isSubmitted: true,
    formIsValid: false,
    imagePathObj: {},
    // fileArray: this.props.requestLoanData?.files ?? [],
    fileArray: [],
  };

  componentDidMount() {
    let { personal_form } = this.state;

    let formIsValid = true;

    for (let inputIdentifier in personal_form) {
      if (personal_form[inputIdentifier].value) {
        personal_form[inputIdentifier].valid = true;
      }
      formIsValid = personal_form[inputIdentifier].valid && formIsValid;
    }

    this.setState({ formIsValid });
  }

  inputChangedHandler = async (event, controlName) => {
    const updatedRequestForm = updateObject(this.state.personal_form, {
      [controlName]: updateObject(this.state.personal_form[controlName], {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          this.state.personal_form[controlName].validation,
        ),
        touched: true,
      }),
    });

    if (controlName === 'aadharNumber') {
      const value = updatedRequestForm[controlName].value;
      updatedRequestForm[controlName].value = value
        .replace(/\D/g, '')
        .split(/(?:([\d]{4}))/g)
        .filter((s) => s.length > 0)
        .join(' ');
    }

    let formIsValid = true;

    for (let inputIdentifier in updatedRequestForm) {
      formIsValid =
        (await updatedRequestForm[inputIdentifier].valid) && formIsValid;
    }

    await this.setState({
      personal_form: updatedRequestForm,
      formIsValid,
    });
  };

  onHandleContinue = () => {
    let isValid = true;

    if (!this.state.formIsValid) {
      this.setState({ isSubmitted: false });
      isValid = false;
      return;
    }

    if (this.state.fileArray.length < 3) {
      this.setState({ error: true });
      isValid = false;
      return;
    }

    this.setState({ error: false });

    let formData = {};
    for (let inputElementIdentifier in this.state.personal_form) {
      formData[inputElementIdentifier] = this.state.personal_form[
        inputElementIdentifier
      ].value;
    }

    formData = updateObject(formData, this.state.imagePathObj);

    this.props.onSubmitPersonalData(formData, this.state.fileArray);

    return isValid;
  };

  onHandleFile = async (event, filedStorage, fieldSuccess, pathField) => {
    if (event?.target?.files?.length > 0) {
      const fileName =
        event.target.name + '.' + event.target.files[0].name.split('.').pop();
      let imageAsFile = event.target.files[0];

      const link = `/${BASE_URL.USERDOCUMENTS}/${this.props.userDetails.phoneNumber}/${fileName}`;

      let { imagePathObj } = this.state;
      imagePathObj[pathField] = link;

      const uploadTask = firebase.storage().ref(link).put(imageAsFile);
      //initiates the firebase side uploading
      uploadTask.on(
        'state_changed',
        (snapShot) => {
          //takes a snap shot of the process as it is happening
        },
        (err) => {
          this.setState({ [filedStorage]: true, [fieldSuccess]: false });
          //catches the errors
        },
        () => {
          let { fileArray } = this.state;
          fileArray.push(imageAsFile);
          this.setState({
            [filedStorage]: true,
            [fieldSuccess]: true,
            fileArray,
          });
        },
      );
    }
  };

  render() {
    const {
      aadharCardBackStroage,
      aadharCardBackSuccess,
      aadharCardFrontStroage,
      aadharCardFrontSuccess,
      panCardStroage,
      panCardSuccess,
      fileArray,
      error,
      isSubmitted,
    } = this.state;

    const formElementsArray = [];
    for (let key in this.state.personal_form) {
      formElementsArray.push({
        id: key,
        config: this.state.personal_form[key],
      });
    }

    const form = formElementsArray.map((formElement) => {
      return (
        <Input
          key={formElement.id}
          elementType={formElement.config.elementType}
          elementConfig={formElement.config.elementConfig}
          value={formElement.config.value}
          class={
            formElement?.config?.elementConfig?.readOnly === true
              ? `input-area read-only`
              : `input-area`
          }
          divClass={`input-wrapper`}
          invalid={!formElement.config.valid}
          shouldValidate={formElement.config.validation}
          touched={formElement.config.touched}
          isSubmitted={isSubmitted}
          changed={(event) => this.inputChangedHandler(event, formElement.id)}
          inDivError={true}
          label={formElement?.config?.label}
          labelClass={'labelClass'}
          isRequired={formElement?.config?.isRequired}
        />
      );
    });

    return (
      <div className="second-step">
        <div className="sub-head-forth">Upload Documents as mentioned</div>

        <div className="main-input-box">{form}</div>

        <div className="main-input-box">
          {/* <div className="input-hide"> */}
          <div className="id-card-box">
            <label htmlFor="aadhar-input-front">
              <div className="status-clock-wrapper">
                {aadharCardFrontStroage && (
                  <img
                    src={
                      aadharCardFrontSuccess
                        ? IMG.UPLOAD_CLOCK
                        : IMG.FAILED_CLOCK
                    }
                    alt="Upload Clock"
                  />
                )}
                {aadharCardFrontStroage && (
                  <span
                    className={`status-text color-text-${
                      aadharCardFrontSuccess ? 'upload' : 'pending'
                    }`}
                  >
                    {aadharCardFrontSuccess ? 'Uploaded' : 'Failed'}
                  </span>
                )}
                {!aadharCardFrontStroage && (
                  <span className="status-text color-text-pending">
                    {'Not Found'}
                  </span>
                )}
              </div>
              <div className="document-name">
                <div className="card-img card-img-upload">
                  <img src={IMG.UPLOAD} alt="Upload" />
                </div>
                <div className="document-details">
                  <p className="card-text">Aadhar Card Front</p>
                  <p className="tap-text">Tap to Upload</p>
                  <p className="type-text">Upload .png, .jpg or .pdf only.</p>
                </div>
              </div>
              <p className="bottom-text">
                Upload your Aadhar card’s scanned copy in color for front.
              </p>
            </label>
            <input
              type="file"
              id="aadhar-input-front"
              name="aadharCardFront"
              accept="image/*"
              onChange={(event) =>
                this.onHandleFile(
                  event,
                  'aadharCardFrontStroage',
                  'aadharCardFrontSuccess',
                  'aadharFrontImagePath',
                )
              }
            />
          </div>

          {/* </div> */}

          {/* <div className="input-hide"> */}
          <div className="id-card-box">
            <label htmlFor="aadhar-input-back">
              <div className="status-clock-wrapper">
                {aadharCardBackStroage && (
                  <img
                    src={
                      aadharCardBackSuccess
                        ? IMG.UPLOAD_CLOCK
                        : IMG.FAILED_CLOCK
                    }
                    alt="Upload Clock"
                  />
                )}
                {aadharCardBackStroage && (
                  <span
                    className={`status-text color-text-${
                      aadharCardBackSuccess ? 'upload' : 'pending'
                    }`}
                  >
                    {aadharCardBackSuccess ? 'Uploaded' : 'Failed'}
                  </span>
                )}
                {!aadharCardBackStroage && (
                  <span className="status-text color-text-pending">
                    {'Not Found'}
                  </span>
                )}
              </div>
              <div className="document-name">
                <div className="card-img card-img-upload">
                  <img src={IMG.UPLOAD} alt="Upload" />
                </div>
                <div className="document-details">
                  <p className="card-text">Aadhar Card Back</p>
                  <p className="tap-text">Tap to Upload</p>
                  <p className="type-text">Upload .png, .jpg or .pdf only.</p>
                </div>
              </div>
              <p className="bottom-text">
                Upload your Aadhar card’s scanned copy in color for back.
              </p>
            </label>
            <input
              type="file"
              id="aadhar-input-back"
              name="aadharCardBack"
              accept="image/*,application/pdf"
              onChange={(event) =>
                this.onHandleFile(
                  event,
                  'aadharCardBackStroage',
                  'aadharCardBackSuccess',
                  'aadharBackImagePath',
                )
              }
            />
          </div>
          {/* </div> */}
        </div>

        <div className="main-input-box">
          {/* <div className="input-hide"> */}
          <div className="id-card-box">
            <label htmlFor="panCard-input">
              <div className="status-clock-wrapper">
                {panCardStroage && (
                  <img
                    src={panCardSuccess ? IMG.UPLOAD_CLOCK : IMG.FAILED_CLOCK}
                    alt="Upload Clock"
                  />
                )}
                {panCardStroage && (
                  <span
                    className={`status-text color-text-${
                      panCardSuccess ? 'upload' : 'pending'
                    }`}
                  >
                    {panCardSuccess ? 'Uploaded' : 'Failed'}
                  </span>
                )}
                {!panCardStroage && (
                  <span className="status-text color-text-pending">
                    {'Not Found'}
                  </span>
                )}
              </div>
              <div className="document-name">
                <div className="card-img card-img-failed">
                  <img src={IMG.FAILED} alt="Failed" />
                </div>
                <div className="document-details">
                  <p className="card-text">PAN Card</p>
                  <p className="tap-text">Tap to Upload</p>
                  <p className="type-text">Upload .png, .jpg or .pdf only.</p>
                </div>
              </div>
              <p className="bottom-text">
                Upload your PAN card’s scanned copy in color both front and
                back.
              </p>
            </label>
            <input
              type="file"
              accept="image/*,application/pdf"
              id="panCard-input"
              name="panCard"
              onChange={(event) =>
                this.onHandleFile(
                  event,
                  'panCardStroage',
                  'panCardSuccess',
                  'panImagePath',
                )
              }
            />
          </div>
          {/* </div> */}
        </div>

        {error && <p className="error-msg">Upload all documents.</p>}

        {fileArray?.length > 0 &&
          fileArray.map((file, index) => (
            <div className="google-doc-wrapper" key={`file${index}`}>
              <img
                src={IMG.GOOGLEFILE}
                className="google-file"
                alt={'google-file'}
              />
              <p className="file-name">{file.name}</p>
            </div>
          ))}
        {/* <div className="google-doc-wrapper">
                    <img src={IMG.GOOGLEFILE} className="google-file" />
                    <p className="file-name">FileNameFront.jpg</p>
                </div> */}

        {/* <FormControlLabelPlacement {...otpVerification} /> */}

        {/* <div className="main-input-box width-98">
                    <div class="input-wrapper">
                        <input class="input-area" type="text" placeholder="" value=""/>
                    </div>

                    <div className="btn-wrapper">
                        <Button className="resend btn-bg" type="button">Enter Recaptcha</Button>
                        <Button className="resend btn-bg">Recaptcha </Button>
                    </div>
                </div> */}
      </div>
    );
  }
}

export default RequestStep3;
