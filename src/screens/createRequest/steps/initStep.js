import React, { memo, useEffect, useState } from "react";

import moment from 'moment';


const InitialStep = memo((props) => {

    const [isInSuficientAmount, setIsInSuficientAmount] = useState(false);

    useEffect(() => {
        if (props?.userDetails?.selectedCompany?.userData?.available_amount === 0) {
            setIsInSuficientAmount(true);
        }
    }, [props]);

    return (
        <div className="initial-step">
            <h6 style={{ fontWeight: '600' }}>Advance Central is currently unavailable</h6>
            <p style={{ fontSize: '12px', lineHeight: '1.5', letterSpacing: '0px', width: '89%' }}>
                {!isInSuficientAmount && <span>Its currently unavailable due to “Either you’re trying to access it during <strong>{`${moment(props?.blackoutDates?.blackoutStartDate).format('Do MMMM')} to ${moment(props?.blackoutDates?.blackoutEndDate).format('Do MMMM')}`}</strong> (or) you’ve reached the maximum number of transactions for this month”. Please visit us again on <strong>{moment(props?.blackoutDates?.blackoutEndDate).format('Do MMMM YYYY')}</strong>.</span>}
                {isInSuficientAmount && <span>Advance Central is currently unavailable due to "Insufficient funds in your account". Please visit us tomorrow and withdraw your salary in advance!!</span>}
            </p>
            {!isInSuficientAmount && <p style={{ fontSize: '12px', lineHeight: '1.5', letterSpacing: '0px' }}>Write to us at <a className="help-link" href="mailto:hello@valyu.ai" target="_blank"
              rel="noopener noreferrer">hello@valyu.ai</a></p>}
        </div>
    )
});

export default InitialStep;