import React, { Component } from 'react';

import * as moment from 'moment';

// UI component
import Input from '../../../components/UI/input/input';
import { checkValidity, updateObject, getMonths } from '../../../shared/shared';
import { STATE_DATA } from '../createRequestConstant.json';

class RequestStep2 extends Component {
  state = {
    requestForm: {
      basic_informtion: {
        first_column: {
          firstName: {
            elementType: 'input',
            label: 'First Name',
            isRequired: true,
            elementConfig: {
              type: 'text',
              placeholder: 'First Name',
              readOnly: this.props?.requestLoanData?.firstName ? true : false,
            },
            value: this.props?.requestLoanData?.firstName ?? '',
            validation: {
              required: true,
            },
            valid: false,
            touched: false,
          },
          middleName: {
            elementType: 'input',
            label: 'Middle Name',
            elementConfig: {
              type: 'text',
              placeholder: 'Middle Name',
              readOnly: this.props?.requestLoanData?.middleName ? true : false,
            },
            value: this.props?.requestLoanData?.middleName ?? '',
            validation: {
              required: false,
            },
            valid: true,
            touched: false,
          },
        },
        second_column: {
          lastName: {
            elementType: 'input',
            label: 'Last Name',
            isRequired: true,
            elementConfig: {
              type: 'text',
              placeholder: 'Last Name',
              readOnly: this.props?.requestLoanData?.lastName ? true : false,
            },
            value: this.props?.requestLoanData?.lastName ?? '',
            validation: {
              required: false,
            },
            valid: false,
            touched: false,
          },
          dob: {
            elementType: 'input',
            label: 'Date of Birth',
            isRequired: true,
            elementConfig: {
              type: 'date',
              placeholder: 'Date of Birth',
              readOnly: this.props?.requestLoanData?.dob ? true : false,
            },
            value: this.props?.requestLoanData?.dob ?? '',
            validation: {
              required: true,
            },
            valid: false,
            touched: false,
          },
        },
        third_column: {
          gender: {
            elementType: 'radio',
            label: 'Gender',
            isRequired: true,
            value: this.props?.requestLoanData?.gender ?? 'Disclosed',
            options: [],
            validation: {
              required: true,
            },
            valid: false,
            touched: false,
            isMargin: true,
            isFullInput: true,
          },
        },
      },
      contact_details: {
        first_column: {
          mobileNumber: {
            elementType: 'input',
            label: 'Phone Number',
            isRequired: true,
            elementConfig: {
              type: 'number',
              placeholder: 'Phone Number',
              minLength: 10,
              maxLength: 13,
              readOnly: true,
            },
            value:
              this.props?.requestLoanData?.mobileNumber ??
              this.props?.userDetails?.phoneNumber,
            validation: {
              required: true,
              minLength: 10,
              maxLength: 13,
              isNumeric: true,
            },
            valid: false,
            touched: false,
          },
          personalEmailId: {
            elementType: 'input',
            label: 'Personal Email ID',
            elementConfig: {
              type: 'text',
              placeholder: 'Personal Email ID',
              readOnly: this.props.userDetails.selectedCompany.userData.emailId
                ? true
                : false,
            },
            value:
              this.props.userDetails.selectedCompany.userData.emailId ?? '',
            validation: {
              required: false,
            },
            valid: true,
            touched: false,
            isEmail: true,
          },
        },
      },
      family_Information: {
        first_column: {
          matrialStatus: {
            elementType: 'radio',
            label: 'Marital Status',
            isRequired: true,
            value: 'Not Disclosed',
            options: [],
            validation: {
              required: true,
            },
            valid: false,
            touched: false,
            isMargin: true,
            isFullInput: true,
          },
        },
      },
      current_address: {
        first_column: {
          currentAddressLine1: {
            elementType: 'input',
            label: 'Line 1',
            isRequired: true,
            elementConfig: {
              type: 'text',
              placeholder: ' Line 1',
              readOnly: this.props?.requestLoanData?.currentAddressLine1
                ? true
                : false,
            },
            value: this.props?.requestLoanData?.currentAddressLine1 ?? '',
            validation: {
              required: false,
            },
            valid: false,
            touched: false,
          },
          currentAddressLine2: {
            elementType: 'input',
            label: 'Line 2',
            elementConfig: {
              type: 'text',
              placeholder: 'Line 2',
              readOnly: this.props?.requestLoanData?.addressLine2
                ? true
                : false,
            },
            value: '',
            validation: {
              required: false,
            },
            valid: true,
            touched: false,
          },
        },
        second_column: {
          currentCity: {
            elementType: 'select',
            label: 'City',
            isRequired: true,
            elementConfig: {
              name: 'reasonTitle',
              id: 'reasonTitle',
              className: 'select select-custom',
              classNamePrefix: 'select',
              placeholder: 'City',
            },
            value:
              this.props?.requestLoanData?.city?.length > 0
                ? this.props?.requestLoanData?.city
                : '',
            validation: {
              required: false,
            },
            options: [],
            valid: false,
            touched: false,
          },
          currentState: {
            elementType: 'select',
            label: 'State',
            isRequired: true,
            elementConfig: {
              name: 'reasonTitle',
              id: 'reasonTitle',
              className: 'select select-custom',
              classNamePrefix: 'select',
              placeholder: 'State',
              readOnly: this.props?.requestLoanData?.state ? true : false,
            },
            value: '',
            validation: {
              required: false,
            },
            options: [],
            valid: false,
            touched: false,
          },
        },
        third_column: {
          currentPincode: {
            elementType: 'input',
            label: 'Pincode',
            isRequired: true,
            elementConfig: {
              type: 'number',
              placeholder: 'Pincode',
              readOnly: this.props?.requestLoanData?.currentPincode
                ? true
                : false,
            },
            value: this.props?.requestLoanData?.currentPincode ?? '',
            validation: {
              required: true,
            },
            valid: false,
            touched: false,
          },
          currentAddressType: {
            elementType: 'radio',
            label: 'Type',
            isRequired: true,
            value: 'Not Known',
            options: [],
            validation: {
              required: true,
            },
            valid: false,
            touched: false,
            isMargin: true,
          },
        },
        fourth_column: {
          currentAddressDuration: {
            elementType: 'select',
            label: 'Years of staying at current address',
            elementConfig: {
              name: 'monthTitle',
              id: 'monthTitle',
              className: 'select select-custom',
              classNamePrefix: 'select',
              placeholder: 'Duration',
              isDisabled: false,
            },
            value: '',
            options: [],
            validation: {
              required: false,
            },
            valid: true,
            touched: false,
          },
        },
      },
      work_Information: {
        first_column: {
          emploerName: {
            elementType: 'input',
            label: 'Name of the Employer',
            isRequired: true,
            elementConfig: {
              type: 'text',
              placeholder: 'Name of the Employer',
              readOnly: this.props.userDetails.selectedCompany.id
                ? true
                : false,
            },
            value: this.props.userDetails.selectedCompany.id ?? '',
            validation: {
              required: false,
            },
            valid: false,
            touched: false,
          },
          doj: {
            elementType: 'input',
            label: 'Date of Joining',
            isRequired: true,
            elementConfig: {
              type: 'date',
              placeholder: 'Date of Joining',
              readOnly: this.props.userDetails.selectedCompany.userData.doj
                ? true
                : false,
            },
            value:
              moment(
                this.props.userDetails.selectedCompany.userData.doj,
              ).format('YYYY-MM-DD') ?? '',
            validation: {
              required: true,
            },
            valid: false,
            touched: false,
          },
        },
        second_column: {
          designation: {
            elementType: 'input',
            label: 'Designation',
            isRequired: true,
            elementConfig: {
              type: 'text',
              placeholder: 'Designation',
              readOnly: this.props.userDetails.selectedCompany.userData
                .designation
                ? true
                : false,
            },
            value:
              this.props.userDetails.selectedCompany.userData.designation ?? '',
            validation: {
              required: true,
            },
            valid: false,
            touched: false,
          },
          employeeId: {
            elementType: 'input',
            label: 'Employee ID',
            isRequired: true,
            elementConfig: {
              type: 'text',
              placeholder: 'Employee ID',
              readOnly: this.props.userDetails.selectedCompany.userData
                .employeeId
                ? true
                : false,
            },
            value:
              this.props.userDetails.selectedCompany.userData.employeeId ?? '',
            validation: {
              required: false,
            },
            valid: false,
            touched: false,
          },
        },
        third_column: {
          officialEmailId: {
            elementType: 'input',
            label: 'Office Email ID',
            isRequired: true,
            elementConfig: {
              type: 'text',
              placeholder: 'Office Email ID',
              readOnly: this.props.userDetails.selectedCompany.userData.emailId
                ? true
                : false,
            },
            value:
              this.props.userDetails.selectedCompany.userData.emailId ?? '',
            validation: {
              required: true,
              isEmail: true,
            },
            valid: false,
            touched: false,
          },
        },
      },
      bank_Information: {
        first_column: {
          bankName: {
            elementType: 'input',
            label: 'Bank Name',
            isRequired: true,
            elementConfig: {
              type: 'text',
              placeholder: 'Bank Name',
              readOnly: this.props?.requestLoanData?.bankName ? true : false,
            },
            value: this.props?.requestLoanData?.bankName ?? '',
            validation: {
              required: true,
            },
            valid: false,
            touched: false,
          },
          bankAcNo: {
            elementType: 'input',
            label: 'Bank Account Number',
            isRequired: true,
            elementConfig: {
              type: 'text',
              placeholder: 'Account Number',
              readOnly: this.props?.requestLoanData?.bankAcNo ? true : false,
            },
            value: this.props?.requestLoanData?.bankAcNo ?? '',
            validation: {
              required: true,
            },
            valid: false,
            touched: false,
          },
        },
        third_column: {
          ifscCode: {
            elementType: 'input',
            label: 'IFSC CODE',
            isRequired: true,
            elementConfig: {
              type: 'text',
              placeholder: 'Bank IFSC Code',
              readOnly: this.props?.requestLoanData?.ifscCode ? true : false,
            },
            value: this.props?.requestLoanData?.ifscCode ?? '',
            validation: {
              required: true,
            },
            valid: false,
            touched: false,
          },
        },
      },
      declare: {
        first_column: {
          declaration: {
            elementType: 'radio',
            value: this.props?.requestLoanData?.declaration ?? '',
            options: [
              { value: 'isAccept', label: this.props?.UIConstant?.decalartion },
            ],
            validation: {
              required: true,
            },
            valid: false,
            touched: false,
            isFullInput: true,
          },
        },
      },
    },
    isSubmitted: true,
    formIsValid: false,
  };

  async componentDidMount() {
    let citiesArray = [];
    this.props.UIConstant.cities.split(',').forEach((data) => {
      citiesArray.push({ value: data, label: data });
    });

    let stateArray = [];
    STATE_DATA.forEach((data) => {
      stateArray.push({ value: data.value, label: data.value });
    });

    let genderArray = [];
    let matrialArray = [];
    let addressTypeArr = [];
    for (let genderKey in this.props?.UIConstant?.gender) {
      genderArray.push({
        value: genderKey,
        label: this.props.UIConstant.gender[genderKey],
      });
    }
    for (let matrialKey in this.props?.UIConstant?.matrialStatus) {
      matrialArray.push({
        value: this.props.UIConstant.matrialStatus[matrialKey],
        label: this.props.UIConstant.matrialStatus[matrialKey],
      });
    }
    for (let addressTypeKey in this.props?.UIConstant?.addressType) {
      addressTypeArr.push({
        value: this.props.UIConstant.addressType[addressTypeKey],
        label: this.props.UIConstant.addressType[addressTypeKey],
      });
    }

    let requestForm = { ...this.state.requestForm };
    let formIsValid = true;
    for (let divIdentifier in requestForm) {
      for (let formIdentifier in requestForm[divIdentifier]) {
        for (let inputIdentifier in requestForm[divIdentifier][
          formIdentifier
        ]) {
          if (
            requestForm[divIdentifier][formIdentifier][inputIdentifier].value
          ) {
            requestForm[divIdentifier][formIdentifier][
              inputIdentifier
            ].valid = true;
          }
          if (inputIdentifier === 'currentCity') {
            requestForm[divIdentifier][formIdentifier][
              inputIdentifier
            ].options = citiesArray;
          }
          if (inputIdentifier === 'currentState') {
            requestForm[divIdentifier][formIdentifier][
              inputIdentifier
            ].options = stateArray;
          }

          if (inputIdentifier === 'gender') {
            requestForm[divIdentifier][formIdentifier][
              inputIdentifier
            ].options = genderArray;
          }
          if (inputIdentifier === 'matrialStatus') {
            requestForm[divIdentifier][formIdentifier][
              inputIdentifier
            ].options = matrialArray;
          }
          if (inputIdentifier === 'currentAddressType') {
            requestForm[divIdentifier][formIdentifier][
              inputIdentifier
            ].options = addressTypeArr;
          }
          if (inputIdentifier === 'currentAddressDuration') {
            requestForm[divIdentifier][formIdentifier][
              inputIdentifier
            ].options = getMonths();
          }
          formIsValid =
            requestForm[divIdentifier][formIdentifier][inputIdentifier].valid &&
            formIsValid;
        }
      }
    }

    this.setState({ formIsValid, requestForm });
  }

  // function call when user enter value in input
  inputChangedHandler = async (
    event,
    sectionIdentifier,
    inputIdentifier,
    divIdentifier,
  ) => {
    const updatedFormElement = await updateObject(
      this.state.requestForm[sectionIdentifier][divIdentifier][inputIdentifier],
      {
        value: event?.target?.value ?? event,
        valid: checkValidity(
          event?.target?.value ?? event?.value,
          this.state.requestForm[sectionIdentifier][divIdentifier][
            inputIdentifier
          ].validation,
        ),
        touched: true,
      },
    );

    if (inputIdentifier === 'aadhar_number') {
      const value = updatedFormElement.value;
      updatedFormElement.value = value
        .replace(/\D/g, '')
        .split(/(?:([\d]{4}))/g)
        .filter((s) => s.length > 0)
        .join(' ');
    }

    const updatedRequestForm = await updateObject(this.state.requestForm, {
      [sectionIdentifier]: updateObject(
        this.state.requestForm[sectionIdentifier],
        {
          [divIdentifier]: updateObject(
            this.state.requestForm[sectionIdentifier][divIdentifier],
            {
              [inputIdentifier]: updatedFormElement,
            },
          ),
        },
      ),
    });

    this.setState({ requestForm: updatedRequestForm });

    let formIsValid = true;
    for (let sectionIdentifier in updatedRequestForm) {
      for (let divIdentifier in updatedRequestForm[sectionIdentifier]) {
        for (let inputIdentifier in updatedRequestForm[sectionIdentifier][
          divIdentifier
        ]) {
          formIsValid =
            (await updatedRequestForm[sectionIdentifier][divIdentifier][
              inputIdentifier
            ].valid) && formIsValid;
        }
      }
    }

    await this.setState({ formIsValid });
  };

  // function for check form value is fill and not
  onHandleSubmitDetails = () => {
    let requestForm = { ...this.state.requestForm };
    let { formIsValid } = this.state;

    if (!formIsValid) {
      this.setState({ isSubmitted: false });
      return formIsValid;
    }

    let formData = {};
    for (let sectionElementIdentifier in requestForm) {
      for (let formElementIdentifier in requestForm[sectionElementIdentifier]) {
        for (let inputElementIdentifier in requestForm[
          sectionElementIdentifier
        ][formElementIdentifier]) {
          formData[inputElementIdentifier] =
            requestForm[sectionElementIdentifier][formElementIdentifier][
              inputElementIdentifier
            ].value;
        }
      }
    }

    console.log(formData);

    formData = updateObject(this.props.requestLoanData, formData);

    this.props.submitFun(formData, formIsValid);

    return formIsValid;
  };

  render() {
    const { requestForm, isSubmitted } = this.state;

    const formElementsArray = [];
    for (let key in requestForm) {
      formElementsArray.push({
        id: key,
        name: key.split('_')[0] + ' ' + key.split('_')[1],
        divArray: { ...requestForm[key] },
      });
    }

    for (let key in formElementsArray) {
      formElementsArray[key]['divConfig'] = [];
      for (let divkey in formElementsArray[key].divArray) {
        formElementsArray[key]['divConfig'].push({
          id: divkey,
          inputConfig: { ...formElementsArray[key]['divArray'][divkey] },
        });
        for (let columnKey in formElementsArray[key].divConfig) {
          formElementsArray[key].divConfig[columnKey]['columnConfig'] = [];
          for (let inputKey in formElementsArray[key].divConfig[columnKey]
            .inputConfig) {
            formElementsArray[key].divConfig[columnKey]['columnConfig'].push({
              id: inputKey,
              config: {
                ...formElementsArray[key].divConfig[columnKey].inputConfig[
                  inputKey
                ],
              },
            });
          }
        }
      }
    }

    let form = formElementsArray.map((divElement) => (
      <div key={divElement.id}>
        {divElement.id !== 'declare' && (
          <p className="secationClass">{divElement.name}</p>
        )}
        {divElement.divConfig.map((inputElement) => (
          <div key={inputElement.id} className="main-input-box">
            {inputElement.columnConfig.map((formElement) => (
              <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                class={
                  formElement &&
                  formElement.config &&
                  formElement.config.elementConfig &&
                  formElement.config.elementConfig.readOnly === true
                    ? `input-area read-only ${
                        formElement.id === 'mobileNumber' ? 'addSpan' : ''
                      } ${
                        formElement.id === 'residental_address'
                          ? 'text-area'
                          : ''
                      }`
                    : `input-area ${
                        formElement.id === 'mobileNumber' ? 'addSpan' : ''
                      } ${
                        formElement.id === 'residental_address'
                          ? 'text-area'
                          : ''
                      }`
                }
                divClass={`input-wrapper ${
                  formElement?.config?.isFullInput ? 'full-input' : ''
                }`}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                isSubmitted={isSubmitted}
                options={formElement?.config?.options}
                changed={(event) =>
                  this.inputChangedHandler(
                    event,
                    divElement.id,
                    formElement.id,
                    inputElement.id,
                  )
                }
                inDivError={true}
                isIconInput={formElement?.config?.isIconInput ?? false}
                iconImg={formElement?.config?.iconImg}
                iconClass={'input-icon'}
                isAddDiv={formElement?.config?.span ? true : false}
                isMargin={formElement?.config?.isMargin ? true : false}
                label={formElement?.config?.label}
                labelClass={'labelClass'}
                isRequired={formElement?.config?.isRequired}
              />
            ))}
          </div>
        ))}
      </div>
    ));

    return (
      <div className="first-step">
        <div className="sub-head-forth">
          Enter your details as they appear on your identification document.
        </div>

        {form}
      </div>
    );
  }
}

export default RequestStep2;
