import React from 'react';
import * as moment from 'moment';
import { getFullName } from '../../../utils/common';
import { useSelector } from 'react-redux';

function RequestStep6(props) {
  const userData = useSelector((state) => state.global.userData);
  const getEmployeeId = () => {
    let workProfile = Object.values(userData && userData.workProfile);
    let employeeId = workProfile[0]?.userData?.employeeId;
    return employeeId;
  };
  return (
    <div className="sixth-step">
      <h5 className="step-heading">Money is just 2 minutes away!</h5>
      <div className="widt-70 mainDiv">
        <div className="table-wrapper">
          <div className="width-100">
            <div className="main-box">
              <div className="category">Request ID:</div>
              <div className="value id">{props?.requestLoanData?.id}</div>
            </div>
            <div className="main-box">
              <div className="category">Status:</div>
              <div className="value status">Sent</div>
            </div>
            {/* <div className="main-box">
                                <div className="category">Purpose:</div>
                                <div className="value text-trasform">{props?.requestLoanData?.reason?.value}</div>
                            </div> */}
            <div className="main-box">
              <div className="category">Employee Name</div>
              <div className="value">
                {getFullName(
                  props?.requestLoanData?.firstName,
                  props?.requestLoanData?.lastName,
                )}
              </div>
            </div>
            <div className="main-box">
              <div className="category">Employee ID</div>
              <div className="value">{getEmployeeId()}</div>
            </div>
            <div className="main-box">
              <div className="category">Date & Time</div>
              <div className="value">
                {moment().format('DD MMM YYYY | hh:mm a')}
              </div>
            </div>
            <div className="main-box">
              <div className="category">Deposited to Bank</div>
              <div className="value">{props?.requestLoanData?.bankName}</div>
            </div>
            <div className="main-box">
              <div className="category">Account Number</div>
              <div className="value">
                {props?.requestLoanData?.bankAcNo}
              </div>
            </div>
            <div className="main-box">
              <div className="category">Estd Disbursement Time</div>
              <div className="value">{moment().format('DD MMM YYYY')}</div>
            </div>
            <div className="main-box">
              <div className="category">Disbursement Amount</div>
              <div className="value">
                &#x20B9;{props?.requestLoanData?.applyLoanAmount}
              </div>
            </div>
            <div className="main-box border-none">
              <div className="category">Repayment Amount</div>
              <div className="value">
                &#x20B9;{props?.requestLoanData?.totalRepayment}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default RequestStep6;
