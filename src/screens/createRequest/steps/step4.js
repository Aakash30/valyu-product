import React, { Component, Fragment } from 'react';

import Slider from '@material-ui/core/Slider';

import _ from 'lodash';
import moment from 'moment';

import IMG from '../../../utils/images';

import Input from '../../../components/UI/input/input';
import { updateObject } from '../../../shared/shared';
import Tooltip from '@material-ui/core/Tooltip';
import SubscriptionComponent from './subComponent/subscriptionComponent';
import PerPayComponent from './subComponent/perPayComponent';

class RequestStep4 extends Component {
  state = {
    amount: this.props.requestLoanData.amount ?? 0,
    interestRate: 0,
    maxAmount: 0,
    minAmount: 0,
    processing_fee: 0,
    plateform_fees: 0,
    plateform_fees_value: 0,
    processing_fee_value: 0,
    interestAmount: 0,
    totalRepayment: 0,
    isError: false,
    isConfirm: false,
    settlementDate: null,
    isSubscriptionEmploye: null,
    reason: {
      elementType: 'select',
      elementConfig: {
        name: 'reasonTitle',
        id: 'reasonTitle',
        className: 'select select-custom',
        classNamePrefix: 'select',
      },
      value: this.props?.requestLoanData?.reason ?? '',
      validation: {
        required: true,
      },
      options: [],
      valid: false,
      touched: false,
    },
    selectedCompany: this.props?.userDetails?.selectedCompany,
  };

  async componentDidMount() {
    let { isConfirm, amount, reason } = await this.state;

    let options = this.props?.UIConstant?.reason_list;
    let newOptions = [];
    for (let key in options) {
      newOptions.push({
        label: options[key],
        value: key,
      });
    }

    if (amount) {
      isConfirm = true;
    }

    reason.options = newOptions;

    this.setState({
      reason,
      isConfirm,
    });
  }

  async componentDidUpdate(props, state) {
    if (
      props?.productCompany &&
      state.isSubscriptionEmploye === null &&
      state.settlementDate === null
    ) {
      let {
        amount,
        interestRate,
        totalRepayment,
        interestAmount,
        processing_fee_value,
        plateform_fees_value,
        processing_fee,
        minAmount,
        maxAmount,
        selectedCompany,
        isSubscriptionEmploye,
      } = state;

      let findObj = props.productCompany;
      isSubscriptionEmploye = props.isSubscriptionEmploye;

      interestRate = _.get(findObj, 'interest_rate', 0);
      minAmount = _.get(findObj, 'min_loan_amount', 0);
      maxAmount =
        amount ??
        _.get(
          props.userDetails.selectedCompany,
          'userData.eligibility_amount',
          100,
        );

      processing_fee_value = _.get(findObj, 'processing_fee', 0);
      plateform_fees_value = _.get(findObj, 'platform_fees', 0);

      amount = selectedCompany?.userData?.available_amount;

      const {
        total_processing_amount,
        total_plateform_fees,
        repayment_amount,
        SI,
        payOutDate,
        settlementDate,
        date_diff,
      } = this.getCalculativeValue(amount, findObj, isSubscriptionEmploye);

      processing_fee = total_processing_amount;
      totalRepayment = repayment_amount.toFixed(2);
      interestAmount = SI;

      await this.setState(
        {
          maxAmount: selectedCompany?.userData?.available_amount,
          amount,
          interestRate,
          interestAmount,
          totalRepayment,
          processing_fee_value,
          plateform_fees_value,
          processing_fee,
          plateform_fees: total_plateform_fees,
          minAmount,
          isSubscriptionEmploye,
          totalSuccessfullApplication:
            selectedCompany.userData.totalSuccessfullApplication ?? 0,
          settlementDate,
          payOutDate,
          clientObj: findObj,
          isSubscriptionEmploye,
          date_diff,
        },
        () => this.onHandleConfirm(),
      );
    }
  }

  // loan calculation
  getCalculativeValue = (amount, findObj, isSubscriptionEmploye) => {
    let repayment_amount = 0;
    let SI = 0;

    let {
      total_processing_amount,
      total_plateform_fees,
      total_loan_applied,
    } = 0;

    // Repayment Calculation
    let date_diff = moment(moment(findObj.settlementDate)).diff(
      moment(moment().format('YYYY-MM-DD')),
      'days',
    );

    const feeData = this.onGetFeeCalculation(amount, findObj);
    total_processing_amount = feeData.total_processing_amount;
    total_plateform_fees = feeData.total_plateform_fees;
    total_loan_applied = feeData.total_loan_applied;

    // calculation of data, if prroduct type is "advance_per_pay"
    if (!isSubscriptionEmploye) {
      SI = parseFloat(
        (
          (total_loan_applied * findObj.interest_rate * date_diff) /
          100
        ).toFixed(2),
      );
      repayment_amount = total_loan_applied + SI;
    }

    if (isSubscriptionEmploye) {
      repayment_amount = total_loan_applied;
    }

    // if(!this.state.isSubscriptionEmploye){
    //   repayment_amount = amount + _.get(clientObj).
    // }

    return {
      total_processing_amount,
      total_plateform_fees,
      repayment_amount,
      SI,
      settlementDate: findObj.settlementDate,
      payOutDate: findObj.payDate,
      total_loan_applied,
      date_diff,
    };
  };

  // For platform fee and processing fee calculation
  onGetFeeCalculation = (amount, findObj) => {
    let fee = findObj.processing_fee ?? findObj.fee;
    let cgst = findObj.cgst ?? 9;
    let sgst = findObj.sgst ?? 9;
    let cgst_on_platform_fees = findObj.cgst_on_platform_fee ?? 0;
    let sgst_on_platform_fees = findObj.sgst_on_platform_fee ?? 0;
    let platform_fees = findObj.platform_fees ?? 0;

    const { isSubscriptionEmploye } = this.state;

    // Processing Fees Calculation
    let processing_amount = !isSubscriptionEmploye ? (fee * amount) / 100 : fee;
    let cgst_on_processing_amount = Math.round(
      (processing_amount * cgst) / 100,
    );
    let sgst_on_processing_amount = Math.round(
      (processing_amount * sgst) / 100,
    );

    const total_processing_amount =
      processing_amount + cgst_on_processing_amount + sgst_on_processing_amount;

    // Plateform Fees calculation
    let cgst_on_platform_fee = Math.round(
      (platform_fees * cgst_on_platform_fees) / 100,
    );
    let sgst_on_platform_fee = Math.round(
      (platform_fees * sgst_on_platform_fees) / 100,
    );

    const total_gst_on_platform_fee =
      cgst_on_platform_fee + sgst_on_platform_fee;
    const total_plateform_fees = platform_fees + total_gst_on_platform_fee;

    // // Repayment Date Calclation
    // let first_emi_date;
    // if (findObj.payoutDay === 'LDM') {
    //   //30/11/2020
    //   first_emi_date = moment().endOf('month').format('DD/MM/YYYY');
    // } else {
    //   let get_month_Date = moment().add(1, 'M').format('DD/MM/YYYY');
    //   let split_data = get_month_Date.split('/');
    //   first_emi_date =
    //     findObj.settlementDate + '/' + split_data[1] + '/' + split_data[2];
    // }

    let total_loan_applied =
      amount + total_processing_amount + total_plateform_fees;

    return {
      processing_amount,
      cgst_on_processing_amount,
      sgst_on_processing_amount,
      total_processing_amount,
      cgst_on_platform_fee,
      sgst_on_platform_fee,
      total_gst_on_platform_fee,
      total_plateform_fees,
      total_loan_applied,
    };
  };

  // Function to handle slider bar
  handleChange = async (event, newValue) => {
    const { selectedCompany, isSubscriptionEmploye } = this.state;

    let findObj = { ...this.state.clientObj };

    const {
      total_processing_amount,
      total_plateform_fees,
      repayment_amount,
      SI,
      total_loan_applied,
      date_diff,
      settlementDate,
      payOutDate,
    } = this.getCalculativeValue(newValue, findObj, isSubscriptionEmploye);

    await this.setState(
      {
        amount: newValue,
        isError: false,
        processing_fee: total_processing_amount,
        plateform_fees: total_plateform_fees,
        totalRepayment: repayment_amount,
        interestAmount: SI,
        total_loan_applied,
        date_diff,
        settlementDate,
        payOutDate,
      },
      () => this.onHandleConfirm(),
    );
  };

  // function call when user enter value in input
  inputChangedHandler = async (event) => {
    let reason = updateObject(this.state.reason, {
      value: event,
    });

    await this.props.submitReason(reason.value);

    await this.setState({ reason });
  };

  // Disable slider if user enter amount
  onHandleConfirm = async () => {
    let {
      amount,
      processing_fee,
      interestRate,
      totalRepayment,
      settlementDate,
      plateform_fees,
      total_loan_applied,
      date_diff,
      isSubscriptionEmploye,
    } = this.state;

    const calculateData = {
      amount,
      interestRate,
      processing_fee,
      totalRepayment,
      settlementDate,
      plateform_fees,
      total_loan_applied,
      date_diff,
      isSubscriptionEmploye,
    };

    await this.props.submitAmount(calculateData);
    await this.props.onLoaderStop();
  };

  // function for check validation then add step
  onHandleContinue = () => {
    const { amount, reason } = this.state;
    let isValid = true;

    if (!amount || !reason.value) {
      isValid = false;
      this.setState({ isError: true });
    }

    // if (isValid) {
    //     this.props.submitReason(reason.value);
    // }

    return isValid;
  };

  render() {
    const {
      amount,
      reason,
      first_emi_date,
      interestAmount,
      totalRepayment,
      processing_fee_value,
      plateform_fees_value,
      maxAmount,
      isError,
      processing_fee,
      plateform_fees,
      interestRate,
      minAmount,
      isSubscriptionEmploye,
      transactionText,
    } = this.state;

    let divElement = '',
      errorElement = '';

    if (isError) {
      errorElement = <p className="error-msg">Required</p>;
    }

    divElement = (
      <Fragment>
        <div className="main-input-box main-input-box-custom justify-content">
          <div className="input-wrapper input-wrapper-custom input-min-wrapper">
            <div className="heading">
              Purpose for the advance {''}
              <span className="required">*</span>
            </div>
          </div>
          <div className="input-wrapper reason-input">
            <Input
              {...reason}
              changed={(event) => this.inputChangedHandler(event)}
            />
            {!reason.value && errorElement}
          </div>
        </div>

        {isSubscriptionEmploye ? (
          <SubscriptionComponent {...this.state} {...this.props} />
        ) : (
          <PerPayComponent {...this.state} {...this.props} />
        )}
      </Fragment>
    );

    return (
      <div className="fourth-step">
        <div className="rs-range-wrapper">
          <div className="range-Heading">
            <p className="mobile-none">Minimum</p>
            <p className="range-amount">₹ {minAmount}</p>
          </div>

          <div className="range-Heading">
            <p className="mobile-none">Maximum</p>

            <p className="range-amount">₹ {parseInt(maxAmount)}</p>
          </div>
        </div>
        <div className="main-input-box main-input-slider">
          <div className="slider-aside-wrapper">
            <div className="slider-left"></div>
            <div className="slider-left-circle"></div>
          </div>

          <div className="input-wrapper full-width slider-input">
            <Slider
              value={parseInt(typeof amount === 'number' ? amount : 0)}
              onChange={(e, value) => this.handleChange(e, value)}
              aria-labelledby="input-slider"
              max={parseInt(maxAmount)}
              step={100}
              marks={false}
            />
          </div>

          <div className="slider-aside-wrapper slider-right">
            <div className="slider-left-circle slider-right-circle"></div>
            <div className="slider-left"></div>
          </div>
        </div>
        <div className="main-input-box main-input-box-custom justify-content">
          <div className="input-wrapper input-wrapper-custom input-min-wrapper">
            <div className="heading">
              Please enter an Amount {''}
              <span className="required">*</span>
            </div>
          </div>

          <div className="input-wrapper input-wrapper-custom reason-input">
            <input
              value={'₹ ' + amount}
              readOnly={true}
              className="input-area input-area-txt"
              minLength="0"
              maxLength={`${maxAmount}`}
              type="text"
            />
            <div>
              <Tooltip
                title={
                  <p
                    style={{
                      fontSize: '10px',
                      maxHeight: '10px',
                    }}
                  >
                    Amount you applied for
                  </p>
                }
                placement="right-end"
                aria-label="Amount you applied for"
              >
                <img
                  src={IMG.INF0_ICON}
                  alt="INF0_ICON"
                  className="input-icon"
                />
              </Tooltip>
            </div>

            {/* display amount error */}
            {!amount && errorElement}
            {/* {(amount > 0 && !reason.value && !isConfirm && isError) && <p className="error-msg">Confirm amount</p>} */}
          </div>

          {/* <div className="input-wrapper input-wrapper-btn">
                        <Button className="confirm-btn" onClick={() => this.onHandleConfirm()}>Confirm</Button>
                    </div> */}
        </div>

        {/* Display interest rate and reason, after confirm amount */}

        {divElement}
      </div>
    );
  }
}

export default RequestStep4;
