
import React from 'react';

import IMG from '../../utils/images';

const RightSideTracker = (props) => {

    let image = '',
        textElement = '';

    if (props.currentStep === props.availableSteps.THIRD_STEP) {
        image = IMG.CREATE_REQUEST_1;
        // textElement = <div className="illustration-text-wrapper flex-icon ">
        //     <img src={IMG.TEXT_ICON} alt="" className="flex-img" />
        //     <p className="illustration-subtext width-90">
        //         Enables us to understand the customers and their financial dealings to be able to serve them better
        //                 </p>
        // </div>;
    }

    if (props.currentStep === props.availableSteps.FIRST_STEP) {
        image = IMG.CREATE_REQUEST_2;
        // textElement = <div className="illustration-text-wrapper">
        //     <div className="flex-icon">
        //         <img src={IMG.TEXT_ICON} alt="" className="flex-img-sm" />
        //         <p className="illustration-subtext width-90">
        //             Select the loan amount by moving the slider.
        //         </p>
        //     </div>

        //     <div className="flex-icon">
        //         <img src={IMG.TEXT_ICON} alt="" className="flex-img-sm" />
        //         <p className="illustration-subtext width-90">
        //             Confirm loan amount.
        //                 </p>
        //     </div>

        //     <div className="flex-icon ">
        //         <img src={IMG.TEXT_ICON} alt="" className="flex-img-sm" />
        //         <p className="illustration-subtext width-90">
        //             Select the reason.
        //         </p>
        //     </div>

        //     <div className="flex-icon ">
        //         <img src={IMG.TEXT_ICON} alt="" className="flex-img-sm" />
        //         <p className="illustration-subtext width-90">
        //             Study the loan details and click on Continue Button.
        //         </p>
        //     </div>
        // </div>


    }

    if (props.currentStep === props.availableSteps.SECOND_STEP) {
        image = IMG.CREATE_REQUEST_3;
        // textElement = <div className="illustration-text-wrapper">
        //     <p className="illustration-text">Let’s get formal!</p>
        //     <p className="illustration-subtext">
        //         We need some document to facilitate your Financial process.
        //     </p>
        // </div>;
    }

    
    if (props.currentStep === props.availableSteps.FOURTH_STEP) {
        image = IMG.CREATE_REQUEST_4;
        // textElement = <div className="illustration-text-wrapper">
        //     <p className="illustration-text">Some Text Here</p>
        // </div>;
    }

    if (props.currentStep === props.availableSteps.FIVETH_STEP) {
        image = IMG.CREATE_REQUEST_6;
        // textElement = <div className="illustration-text-wrapper">
        //     <p className="illustration-text">All done!</p>
        //     <p className="illustration-subtext">
        //         Everything is done. We are just counting the i’s and the T’s and getting it all sorted for you.
        //     </p>
        // </div>;
    }

    return (
        <div className="create-illustration-wrapper">

            <div className="" style={{height: '100%'}}>
                <img src={image} alt="CREATE_REQUEST_4" />
            </div>

            {textElement}

        </div>
    )
}

export default RightSideTracker;