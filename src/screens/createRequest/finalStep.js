import { Button } from '@material-ui/core';
import React, { Fragment } from 'react';
import { useEffect } from 'react';
import { Redirect } from 'react-router-dom';

import { routes } from '../../routing/routes';

import IMG from '../../utils/images';

const FinalStep = (props) => {
  const onResetFun = props.onReset;
  const [redirectPath, setRedirectPath] = React.useState('');
  let [redireactDiv] = React.useState('');
  const [isRedirect, setIsRedirect] = React.useState(false);

  const handleDahboardRedirect = () => {
    setRedirectPath(routes.Dashboard);
    setIsRedirect(true);
  };

  const handleRedirect = () => {
    setRedirectPath(routes.Transactions);
    setIsRedirect(true);
  };

  useEffect(() => {
    // returned function will be called on component unmount
    return () => {
      onResetFun();
    };
  }, []);

  if (isRedirect) {
    redireactDiv = <Redirect to={redirectPath} />;
  }

  return (
    <Fragment>
      {/* <h1 className="final-step-text">
                Please download VALYU app to know more
            </h1>
            <div className="button-box">
                <a
                    href={AppConstants.URLS.GOOGLE}
                    className="badge-btn"
                    target="parent"
                >
                    <img
                        src={IMG.GOOGLE_BADGE}
                        alt="google badge"
                        className="status-badge"
                    />
                </a>
                <a
                    href={AppConstants.URLS.APPLE}
                    className="badge-btn"
                    target="parent"
                >
                    <img
                        src={IMG.APP_BADGE}
                        alt="apple badge"
                        className="status-badge"
                    />
                </a>
            </div> */}

      {redireactDiv}

      <div className="button-box ">
        <Button className="home-btn" onClick={handleDahboardRedirect}>
          <img alt="" src={IMG.HOME_REDIRECT} className="icon-home" />
          Back To Home
        </Button>
        <Button className="transcation-btn" onClick={handleRedirect}>
          <img alt="" src={IMG.TRANSCATION_REDIRECT} className="icon-home" />
          Check My transactions
        </Button>
      </div>
    </Fragment>
  );
};

export default FinalStep;
