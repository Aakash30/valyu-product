import React, { Component, Fragment } from 'react';
import { Button } from '@material-ui/core';

import './createRequest.scss';

import _ from 'lodash';
import * as moment from 'moment';

import { connect } from 'react-redux';

import * as actions from '../../store/actions/index';
import Loader from '../../components/loader/loader';
import { PRODUCT_TYPE } from '../../app.constants.json';

import RequestStep2 from './steps/step2';
import RequestStep3 from './steps/step3';
import RequestStep4 from './steps/step4';
import RequestStep5 from './steps/step5';
import RequestStep6 from './steps/step6';
import FinalStep from './finalStep';
import IMG from '../../utils/images';
import RightSideTracker from './rightSideTracker';
import { routes } from '../../routing/routes';
import InitialStep from './steps/initStep';

class CreateRequestComponent extends Component {
  constructor(props) {
    super(props);
    this.child1 = React.createRef();
    this.child2 = React.createRef();
    this.child3 = React.createRef();
    this.child4 = React.createRef();
    this.child5 = React.createRef();
  }

  state = {
    blackoutDates: {},
  };

  async componentDidMount() {
    await this.props.onSetActiveRoute();
    this.props.onHandleProductType(
      this.props.clientList,
      this.props.userDetails.selectedCompany,
    );

    if (this.onHandleUpdateState() && this.props.currentStep === 0) {
      this.props.onUpdateNextStep(this.props.currentStep);
    }
  }

  onHandleUpdateState = () => {
    if (
      !this.onGetBlockDate() &&
      !this.onGetAvailableDateState() &&
      !this.onGetTransactionBasedState()
    ) {
      return true;
    }
    return false;
  };

  // Check Available Funds
  onGetAvailableDateState = () => {
    const availableAmount = parseInt(
      this.props?.userDetails?.selectedCompany?.userData?.available_amount,
    );
    if (availableAmount === 0) {
      return true;
    }
    return false;
  };

  // Check transaction
  onGetTransactionBasedState = () => {
    const totalTransaction = this.props?.userDetails?.selectedCompany?.userData
      ?.totalSuccessfullApplication;
    if (
      this.props.productCompany?.maxAllowedTransaction <= totalTransaction &&
      this.props.selectedCompany?.userData?.product_name ===
        PRODUCT_TYPE.ADVANCELOAN_SUBSCRIPTION
    ) {
      return true;
    }
    return false;
  };

  // Manage block end nd start date
  onGetBlockDate = () => {
    let blackoutDates = {};
    const blackoutStartDate = this.props.productCompany?.blackoutStartDate;
    const blackoutEndDate = this.props.productCompany?.blackoutEndDate;
    blackoutDates.blackoutStartDate = blackoutStartDate;
    blackoutDates.blackoutEndDate = blackoutEndDate;

    this.setState({ blackoutDates });
    const today = moment().format('YYYY-MM-DD');
    if (
      moment(today).isBetween(
        blackoutDates.blackoutStartDate,
        blackoutDates.blackoutEndDate,
        undefined,
        '[]',
      )
    ) {
      return true;
    }
    return false;
  };

  // Function for update step fro redux and also save data
  onHandleRequest = () => {
    if (this.child1?.current && !this.child1?.current?.onHandleContinue()) {
      return;
    }

    if (
      this.child2?.current &&
      !this.child2?.current?.onHandleSubmitDetails()
    ) {
      return;
    }

    if (this.child3?.current) {
      if (!this.child3?.current?.onHandleContinue()) {
        return;
      }
    }

    if (this.child4?.current) {
      if (!this.child4?.current?.onHandleContinue()) {
        return;
      }
      return;
    }

    if (
      this.props?.userData?.userProfile?.isDataSubmissionComplete ||
      this.props?.userData?.personal?.isDataSubmissionComplete
    ) {
      if (this.props?.userData?.personal?.isDataSubmissionComplete) {
        this.props.onUpdateNextStep(this.props?.availableSteps?.SECOND_STEP);
      }

      if (this.props?.userData?.userProfile?.isDataSubmissionComplete) {
        this.props.onUpdateNextStep(this.props?.availableSteps?.THIRD_STEP);
      }
    } else {
      this.props.onUpdateNextStep(this.props?.currentStep);
    }

    console.log(this.props.currentStep);
  };

  // on submit data
  onHandleSubmitLoan = async (link, obj) => {
    let { requestLoanData } = this.props;
    requestLoanData.loanDocumentUrl = link;
    requestLoanData.companyObj = obj;

    await this.props.onSetLoadData(
      this.props.userDetails,
      requestLoanData,
      this.props?.currentStep,
      this.props.userData,
    );
  };

  // Function call when user click on back
  onHandleBack = async () => {
    if (
      this.props?.userData?.userProfile?.isDataSubmissionComplete ||
      this.props?.userData?.personal?.isDataSubmissionComplete
    ) {
      if (this.props?.userData?.userProfile?.isDataSubmissionComplete) {
        await this.props.onUpdatePreviousStep(
          this.props?.availableSteps?.THIRD_STEP,
        );
      }
      if (this.props?.userData?.personal?.isDataSubmissionComplete) {
        await this.props.onUpdatePreviousStep(
          this.props?.availableSteps?.SECOND_STEP,
        );
      }
    } else {
      await this.props.onUpdatePreviousStep(this.props?.currentStep);
    }
  };

  onHandleRedirect = async () => {
    await this.props.onResetLoanFields();
  };

  // disable next task button, after submit the details
  onHandleSubmitThird = async (data) => {
    await this.props.onSetReason(data);
  };

  // send OTP for verify
  onHandleOTP = async () => {
    await this.props.onSentOTP(this.props.userDetails.phoneNumber);
  };

  // check OTP
  onHandleCode = async (code) => {
    await this.props.onSetConfirmCode(code, this.props.userDetails.phoneNumber);
  };

  render() {
    const { blackoutDates } = this.state;

    let errorElement = '';
    if (this.props.errorMsg) {
      errorElement = <p className="error-msg">{this.props.errorMsg}</p>;
    }

    return (
      <div>
        <div className="create-requests-container">
          {/* <div className="top-bars"></div> */}
          <div className="card">
            <div className="create-step-first">
              <h5 className="step-heading">
                {this.props?.currentStep !==
                  this.props?.availableSteps.FIVETH_STEP &&
                  this.props?.currentStep !==
                    this.props?.availableSteps.FIRST_STEP &&
                  this.props?.currentStep !==
                    this.props.availableSteps.INITIAL_STEP && (
                    <div
                      className="back-arrow-box"
                      onClick={() => this.onHandleBack()}
                    >
                      <img src={IMG.BACK_ARROW} alt="backaArrow" width="8px" />
                    </div>
                  )}
                {/* {this.props?.currentStep === this.props?.availableSteps?.FIRST_STEP && 'Terms & Conditions'} */}
                {this.props?.currentStep ===
                  this.props?.availableSteps?.THIRD_STEP &&
                  'Tell us something about yourself!'}
                {/* {this.props?.currentStep === this.props?.availableSteps?.THIRD_STEP && 'Upload your documents'} */}
                {this.props?.currentStep ===
                  this.props?.availableSteps?.FIRST_STEP &&
                  'How much you want?'}
                {this.props?.currentStep ===
                  this.props?.availableSteps?.SECOND_STEP &&
                  'Upload your documents'}
                {this.props?.currentStep ===
                  this.props?.availableSteps?.FOURTH_STEP && (
                  <div className="final-heading">Loan Agreement</div>
                )}
                {/* {this.props?.currentStep === this.props?.availableSteps.FIVETH_STEP && 'Continue to get money NOW!'} */}
                {this.props?.currentStep ===
                  this.props?.availableSteps.FIVETH_STEP && (
                  <div className="final-heading">
                    <div className="awesome-heading">
                      Awesome!
                      <img src={IMG.CELEBRATION} alt="CELEBRATION" />
                    </div>
                    <div className="heading-btn">
                      <div className="CTA-text">
                        <img
                          className="CTA-icon"
                          alt=""
                          src={IMG.DOWNLOAD_BTN}
                        />
                        <span className="CTA-text hide">Download</span>
                      </div>
                      <div className="CTA-text">
                        <img className="CTA-icon" alt="" src={IMG.PRINT_BTN} />
                        <span className="CTA-text hide">Print</span>
                      </div>
                    </div>
                  </div>
                )}
              </h5>

              {/* {(this.props?.currentStep !== this.props?.availableSteps?.SIXTH_STEP && this.props?.currentStep !== this.props?.availableSteps?.FIRST_STEP) && <StepTracker currentStep={this.props?.currentStep} />} */}

              <div className="form-step">
                {this.props.isLoader && <Loader />}
                {errorElement}
                {/* {this.props?.currentStep === this.props?.availableSteps?.SECOND_STEP && (
                                    <RequestStep1 ref={this.child1} {...this.props} />
                                )} */}

                {this.props?.currentStep ===
                  this.props?.availableSteps?.INITIAL_STEP && (
                  <InitialStep
                    userDetails={this.props.userDetails}
                    blackoutDates={blackoutDates}
                  />
                )}

                {this.props?.currentStep ===
                  this.props?.availableSteps?.THIRD_STEP && (
                  <RequestStep2
                    ref={this.child2}
                    {...this.props}
                    requestLoanData={this.props?.requestLoanData}
                    submitFun={(data) => this.props.onUpdateLoadData(data)}
                  />
                )}
                {this.props?.currentStep ===
                  this.props?.availableSteps?.SECOND_STEP && (
                  <RequestStep3
                    ref={this.child1}
                    {...this.props}
                    onSubmitPersonalData={(data, file) =>
                      this.props.onSubmitPersonalData(data, file)
                    }
                  />
                )}
                {this.props?.currentStep ===
                  this.props?.availableSteps?.FIRST_STEP &&
                  this.props.productCompany && (
                    <RequestStep4
                      ref={this.child3}
                      {...this.props}
                      requestLoanData={this.props?.requestLoanData}
                      submitAmount={(data) => this.props.onSetAmount(data)}
                      submitReason={(data) => this.onHandleSubmitThird(data)}
                      userDetails={this.props?.userDetails}
                      companiesList={this.props?.companiesList}
                      isSubscriptionEmploye={this.props.isSubscriptionEmploye}
                      productCompany={this.props.productCompany}
                    />
                  )}
                {this.props?.currentStep ===
                  this.props?.availableSteps?.FOURTH_STEP && (
                  <RequestStep5
                    ref={this.child4}
                    {...this.props}
                    onDisableButton={() => this.props.onDisableButton()}
                    onEnableButton={() => this.props.onEnableButton()}
                    requestLoanData={this.props?.requestLoanData}
                    onSetLoanData={(link, obj) =>
                      this.onHandleSubmitLoan(link, obj)
                    }
                    onSentOTPFun={() => this.onHandleOTP()}
                    isInvalidCode={this.props.isInvalidCode}
                    isConfirmCode={this.props.isConfirmCode}
                    onHandleConfirmCode={(code) => this.onHandleCode(code)}
                    verifyOTP={this.props.verifyOTP}
                  />
                )}
                {this.props?.currentStep ===
                  this.props?.availableSteps?.FIVETH_STEP && (
                  <RequestStep6 {...this.props} />
                )}
              </div>

              {this.props?.currentStep !==
              this.props?.availableSteps?.INITIAL_STEP ? (
                <Fragment>
                  {this.props?.currentStep !==
                  this.props?.availableSteps?.FIVETH_STEP ? (
                    <div className="button-box">
                      {!this.props.isNextButtonDisable ? (
                        <Button
                          className="blue-btn create-request-btn"
                          onClick={() => this.onHandleRequest()}
                        >
                          {this.props?.currentStep ===
                          this.props?.availableSteps?.FOURTH_STEP
                            ? 'Sign with OTP'
                            : 'Next'}
                        </Button>
                      ) : (
                        <Button
                          className="blue-btn create-request-btn"
                          disabled
                        >
                          {this.props?.currentStep ===
                          this.props?.availableSteps?.FOURTH_STEP
                            ? 'Sign with OTP'
                            : 'Next'}
                        </Button>
                      )}
                    </div>
                  ) : (
                    <FinalStep onReset={() => this.onHandleRedirect()} />
                  )}
                </Fragment>
              ) : (
                <div className="button-box">
                  <Button
                    type="button"
                    onClick={() => this.props.history.push(routes.Dashboard)}
                    className="home-btn"
                  >
                    Go to Dashboard
                  </Button>
                </div>
              )}
            </div>

            {this.props?.currentStep !==
              this.props.availableSteps.INITIAL_STEP && (
              <RightSideTracker {...this.props} />
            )}

            {this.props?.currentStep ===
              this.props.availableSteps.INITIAL_STEP && (
              <div className="error-div">
                <img className="error-img" alt="ErrorMsg" src={IMG.ERROR_IMG} />
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    availableSteps: _.get(state.global, 'allSteps', { FIRST_STEP: 1 }),
    currentStep: _.get(state.global, 'activatedState', 1),
    requestLoanData: _.get(state.global, 'loanData'),
    userDetails: _.get(state.global, 'userInfo'),
    userData: _.get(state.global, 'userData'),
    companiesList: _.get(state.global, 'companies'),
    UIConstant: _.get(state.global, 'UIConstant'),
    clientList: _.get(state.global, 'clientList'),
    isNextButtonDisable: _.get(state.global, 'isNextButtonDisable', false),
    isLoader: _.get(state.global, 'isLoader', false),
    isConfirmCode: _.get(state.global, 'isConfirmCode', false),
    isInvalidCode: _.get(state.global, 'isInvalidCode', false),
    verifyOTP: _.get(state.global, 'verifyOTP'),
    errorMsg: _.get(state.global, 'error'),
    isSubscriptionEmploye: _.get(state.global, 'isSubscriptionEmploye'),
    productCompany: _.get(state.global, 'productCompany'),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onUpdateNextStep: (step) => dispatch(actions.onUpdateStep(step)),
    onUpdatePreviousStep: (step) => dispatch(actions.onBackPreStep(step)),
    onUpdateLoadData: (formData) =>
      dispatch(actions.onRequestLoanData(formData)),
    onSetAmount: (data) => dispatch(actions.getLoanCalculation(data)),
    onSetReason: (reason) => dispatch(actions.onSelectReason(reason)),
    onSetLoadData: (number, data, step, userData) =>
      dispatch(actions.postLoanData(number, data, step, userData)),
    fecthUIConstant: () => dispatch(actions.fecthUIConstant()),
    onResetLoanFields: () => dispatch(actions.onResteLoanFields()),
    onSubmitPersonalData: (payload, files) =>
      dispatch(actions.onSubmitPersonalData(payload, files)),
    onSetActiveRoute: () => dispatch(actions.onHandleActive('request')),
    onSentOTP: (number) => dispatch(actions.sendOtpToPhone(number)),
    onSetConfirmCode: (code, number) =>
      dispatch(actions.confirmCode(code, number, '')),
    onLoaderStart: () => dispatch(actions.onAuthStart()),
    onLoaderStop: () => dispatch(actions.onAuthEnd()),
    onDisableButton: () => dispatch(actions.onDisableButton()),
    onEnableButton: () => dispatch(actions.onEnableButton()),
    onCreateUIConstant: (payload, key) =>
      dispatch(actions.createUIConstant(payload, key)),
    onHandleProductType: (clientObj, companyObj) =>
      dispatch(actions.onHandleProductType(clientObj, companyObj)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateRequestComponent);
