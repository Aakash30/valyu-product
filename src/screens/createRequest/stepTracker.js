import React from 'react';


const StepTracker = (props) => {
    return (
        <div className="tracker">
            <div className="handle-line ">
                <div className= "line activate"></div>
            </div>
            <div className="handle-line">
                <div
                    className={`line ${props.currentStep > 3 ? 'activate' : ''}`}
                />
            </div>
            <div className="handle-line">
            <div
                    className={`line ${props.currentStep > 4 ? 'activate' : ''}`}
                />
            </div>
            <div className="handle-line">
            <div
                    className={`line ${props.currentStep > 5 ? 'activate' : ''}`}
                />
            </div>
        </div>
    )
}

export default StepTracker;