import React, { useState, useEffect, useCallback } from 'react';
import IMG from '../../utils/images';
import './help.scss';
import * as actions from '../../store/actions/index';
import { useDispatch, useSelector } from 'react-redux';
import { onSetUserQuery } from '../../store/actions';
import _ from 'lodash';
import { Fragment } from 'react';

function Help() {
  const state = useSelector((state) => state.global.userInfo);
  const global = useSelector((state) => state.global);
  const userData = useSelector((state) => state.global.userData);
  const clientList = useSelector((state) => state.global.clientList);
  const companyObj = _.keyBy(clientList, 'id');
  const selectedCompany = state?.selectedCompany;

  const getProductName = () => {
    let workProfile = userData && userData.workProfile[selectedCompany.id];
    let productName = workProfile?.userData?.product_name;
    return productName;
  };
  const [phone] = useState(state.phoneNumber);
  const [message, setMessage] = useState('');
  const [error, setError] = useState(false);

  const dispatch = useDispatch();
  const stableDispatch = useCallback(dispatch, []); // it doesn't need to change

  const [selectedIndex, setSelectedIndex] = useState(0);

  const validate = (event) => {
    const theEvent = event || window.event;
    // Handle key press
    let key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    const regex = /[0-9]|\./;
    if (!regex.test(key)) {
      theEvent.returnValue = false;
      if (theEvent.preventDefault) theEvent.preventDefault();
    }
  };
  const onSubmit = () => {
    let queryObj = {
      phoneNumber: phone,
      query: message,
    };
    if (phone && phone.length > 0 && message && message.length > 0) {
      dispatch(onSetUserQuery(queryObj));
      window.alert('Thanks! We have recieved your message.');
      // setPhone('');
      setMessage('');
    } else {
      setError(true);
    }
  };

  useEffect(() => {
    stableDispatch(actions.onHandleActive('help'));
  }, [stableDispatch]);

  return (
    <section>
      <div className="help-content-container">
        <div className="help-section">
          <p className="help-section-title">Help</p>
          <div className="faq-section">
            <div className="heading-primary">
              <img src={IMG.HEADING_BULLET} alt="img" />
              <p className="text">Frequently asked questions</p>
            </div>
            {/* <div style={{ display: 'flex', flexDirection: 'column' }}> */}
            {getProductName() === 'advanceLoan_pay_per_use'
              ? global &&
                global.helpDesk &&
                global.helpDesk.map((item, index) => {
                  if (index !== 0) {
                    return (
                      <>
                        <div className="heading-secondary">
                          <img src={IMG.HEADING_BULLET} alt="img" />
                          <p
                            onClick={() => setSelectedIndex(index)}
                            className="text"
                          >
                            {item.question}
                          </p>
                        </div>
                        {selectedIndex === index && (
                          <div className="answer-container">
                            <p>{item.answer}</p>
                          </div>
                        )}
                      </>
                    );
                  }
                })
              : global &&
                global.helpDesk &&
                global.helpDeskSubscription.map((item, index) => {
                  if (index !== 0) {
                    return (
                      <>
                        <div className="heading-secondary">
                          <img src={IMG.HEADING_BULLET} alt="img" />
                          <p
                            onClick={() => setSelectedIndex(index)}
                            className="text"
                          >
                            {item.question}
                          </p>
                        </div>
                        {selectedIndex === index && (
                          <div className="answer-container">
                            <p>{item.answer}</p>
                          </div>
                        )}
                      </>
                    );
                  }
                })}
          </div>
        </div>
        <div className="support-section">
          <p className="help-section-title">Support</p>
          <div className="support-phrase-container">
            <p className="support-phrase-container-question">
              Still searching for answers?
            </p>
            <p className="support-phrase-container-answer">
              Connect our specialist team for assistance
            </p>
          </div>
          <div className="support-form-container">
            <div className="support-form">
              <p className="support-form-title">
                Please enter your registered mobile number
              </p>
              <div className="number-container">
                <p className="label">Registered Mobile Number</p>
                <input
                  className="input-field"
                  minLength="10"
                  type="text"
                  placeholder="+91 8130677980"
                  value={phone}
                  // onChange={(e) => setPhone(e.target.value)}
                  onKeyPress={validate}
                />
              </div>
              <div className="message-container">
                <p className="label">Message</p>
                <textarea
                  className="input-area"
                  type="text"
                  placeholder="Hello, I would you to update my info on my profile. It is incorrect."
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                />
                {error && <p className="error-msg">Please enter message.</p>}
              </div>
              <div className="submit-btn-container">
                <button
                  id="submit"
                  className="submit-btn"
                  type="submit"
                  onClick={() => {
                    onSubmit();
                  }}
                >
                  Submit Support
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Help;
