import React, { Component, Suspense, Fragment } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import MomentUtils from '@date-io/moment';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import Loader from '../components/loader/loader';
import { lazyRouting } from './lazyLoading';
import { routes, componentArr } from './routes';
import withAuthCheck from '../hoc/authCheck/withAuthCheck';
import Header from '../components/UI/header/header';
import Nav from '../components/UI/nav/nav';

import { connect } from 'react-redux';
import _ from 'lodash';
import NotFound from '../components/notFound/notFound';

// set theme of material UI provider
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#0961ad',
    },
  },
  typography: {
    // ...
    // Tell Material-UI what's the font-size on the html element is.
    htmlFontSize: 10,
    fontFamily: 'Muli-SemiBold',
    // ...
  },
});

// export all routing
class BasicRouting extends Component {
  render() {
    let routing = (
      <Switch>
        <Route exact path={routes.Login} component={lazyRouting.AuthPage} />
        <Route strict path={routes.Auth} component={lazyRouting.AuthPage} />
        <Redirect to={routes.Login} />
      </Switch>
    );

    const rotuingWithHeader = (
      <Fragment>
        <Header />
        <Nav />
        <Route
          exact
          path={routes.Dashboard}
          component={lazyRouting.DashboardPage}
        />
        <Route
          exact
          path={routes.CreateRequest}
          component={lazyRouting.CreateRequestPage}
        />
        <Route
          exact
          path={routes.Transactions}
          component={lazyRouting.TransactionsPage}
        />
        <Route
          exact
          path={routes.Profile}
          component={lazyRouting.ProfilePage}
        />
        <Route
          exact
          path={routes.Setting}
          component={lazyRouting.SettingPage}
        />
        <Route exact path={routes.Help} component={lazyRouting.HelpPage} />
      </Fragment>
    );

    if (
      !_.isEmpty(this.props?.userProfile) &&
      this.props?.userDetails?.selectedCompany?.userData
    ) {
      routing = (
        <Switch>
          <Route exact path={routes.Login} component={lazyRouting.AuthPage} />
          <Route strict path={routes.Auth} component={lazyRouting.AuthPage} />
          {rotuingWithHeader}
          <Route excat path={routes.NotFoundUrl} component={NotFound} />
          <Redirect to={routes.NotFoundUrl} />
        </Switch>
      );
    }

    return (
      <MuiThemeProvider theme={theme}>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <Suspense fallback={<Loader />}>{routing}</Suspense>
        </MuiPickersUtilsProvider>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userDetails: state?.global?.userInfo,
    userProfile: state?.global?.userData,
  };
};

export default connect(mapStateToProps)(withAuthCheck(BasicRouting));
