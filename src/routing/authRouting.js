import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import NotFound from '../components/notFound/notFound';
import { lazyRouting } from './lazyLoading';
import { routes } from './routes';

export default function AuthRouting(props) {
  const isAuth = props?.isGetOtp
    ? props.isGetOtp
    : props?.userDetails?.userId !== null;

  // Routing will be work if user not enter phone number
  let activeRouting = (
    <Switch>
      <Route exact path={props.match.path} component={lazyRouting.LoginPage} />
      <Route excat path={routes.NotFoundUrl} component={NotFound} />
      <Redirect to={routes.NotFoundUrl} />
    </Switch>
  );

  // if user will enter phone number then this routing if work
  // isGetOtp set by redux when user click on Get OTp on login page

  try {
    if (isAuth) {
      activeRouting = (
        <Switch>
          <Route
            exact
            path={props.match.path}
            component={lazyRouting.LoginPage}
          />
          <Route exact path={routes.OTPPage} component={lazyRouting.OTPPage} />
          <Route
            exact
            path={routes.SelectCompany}
            component={lazyRouting.SelectCompanyPage}
          />
          <Route
            exact
            path={routes.AddSalary}
            component={lazyRouting.AddSalaryPage}
          />
        </Switch>
      );
    }
  } catch (err) {
    console.log(err);
  }

  return activeRouting;
}
