import { lazy } from 'react';

// use lazy loading for render component
export const lazyRouting = {
    AuthPage: lazy(() => import('../screens/auth/auth')),
    LoginPage: lazy(() => import('../screens/auth/login/login')),
    DashboardPage: lazy(() => import('../screens/dashboard/dashboard')),
    OTPPage: lazy(() => import('../screens/auth/verifyNumber/verifyNumber')),
    SelectCompanyPage: lazy(() => import('../screens/auth/selectCompany/selectCompany')),
    AddSalaryPage: lazy(() => import('../screens/auth/addSalary/AddSalary')),
    CreateRequestPage: lazy(() => import('../screens/createRequest/createRequest')),
    TransactionsPage: lazy(() => import('../screens/transactions/transactions')),
    ProfilePage: lazy(() => import('../screens/profile/profile')),
    SettingPage: lazy(() => import('../screens/settings/setting')),
    HelpPage: lazy(() => import('../screens/help/help'))
};
