export const routes = {
  Login: '/',
  Auth: '/auth',
  Dashboard: '/dashboard',
  OTPPage: '/auth/verify-number',
  SelectCompany: '/auth/select-company',
  AddSalary: '/auth/addSalary',
  CreateRequest: '/create-request',
  Transactions: '/transactions',
  Profile: '/profile',
  Setting: '/settings',
  Help: '/help',
  NotFoundUrl: '/not-found',
};

export const componentArr = [
  {
    path_name: routes.Dashboard,
    component: 'DashboardPage',
    active: 'dashboard',
  },
  {
    path_name: routes.CreateRequest,
    component: 'CreateRequestPage',
    active: 'request',
  },
  {
    path_name: routes.Help,
    component: 'HelpPage',
    active: 'help',
  },
  {
    path_name: routes.Profile,
    component: 'ProfilePage',
    active: 'profile',
  },
  {
    path_name: routes.Setting,
    component: 'SettingPage',
    active: 'settings',
  },
  {
    path_name: routes.Transactions,
    component: 'TransactionsPage',
    active: 'transection',
  },
];
