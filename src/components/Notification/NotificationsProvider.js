import React from 'react';
import { connect } from 'react-redux';

import firebase from '../../utils/firebase/firebase';
import IMG from '../../utils/images';

import FirebaseAnalyticsUtil from '../../utils/firebase/firebase';
import * as actions from '../../store/actions/index';

export class NotificationsProvider extends React.PureComponent {
  async componentDidMount() {
    const messaging = firebase.messaging();
    this.initFirebaseMessaging(messaging);
  }

  async initFirebaseMessaging(messaging) {

    try {
      await messaging.requestPermission();
      console.log('Notification permission granted.');

      await this.getTokenFromFirebase(messaging);

      messaging.onTokenRefresh(async () => {
        await this.getTokenFromFirebase(messaging);
      });

      navigator.serviceWorker.addEventListener('message', (message) =>
        console.log(message),
      );

      messaging.onMessage((payload) => {
        let visibleNotification;
        if (payload.notification) {
          visibleNotification = payload.notification;
        } else if (payload.data.notification) {
          visibleNotification = JSON.parse(payload.data.notification);
        }
        if (visibleNotification) {
          visibleNotification.icon = IMG.APP_ICON;
          FirebaseAnalyticsUtil.sendNotification(visibleNotification);
        }
      });
    } catch (err) {
      console.log(
        'Error occurred while setting up Firebase notifications',
        err,
      );
    }
  }

  async getTokenFromFirebase(messaging) {
    try {
      const token = await messaging.getToken();
      if (token) {
        console.log('Token registered/refreshed', token);
        this.props.getDeviceToken({
          deviceToken: token,
        });
      }
    } catch (err) {
      throw new Error('Unable to receive token', err);
    }
  }

  render() {
    return this.props.children;
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDeviceToken: (payload) => dispatch(actions.getDeviceToken(payload)),
  };
};

export default connect(null, mapDispatchToProps)(NotificationsProvider);
