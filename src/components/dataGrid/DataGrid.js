import React, { useState } from 'react';
import './DataGrid.scss';
import IMG from '../../utils/images';
import { COLUMN_DATA } from './DataGrid.constants';
import * as moment from 'moment';
import { routes } from '../../routing/routes';
import { maskString, naCheck } from '../../utils/common';
import * as firebase from 'firebase';
var FileSaver = require('file-saver');

function DataGrid(props) {
  const { data } = props;
  const [page, setPage] = useState(1);
  let pagination = 10;
  const pageCount =
    data && Object.keys(data).length > 0
      ? Object.keys(data).length > 0
        ? Math.ceil(Object.keys(data).length / pagination)
        : null
      : 0;

  const showColumnHeader = () =>
    COLUMN_DATA.map((column) => {
      return (
        <th className="column-header" key={column.key}>
          {column.label}
        </th>
      );
    });

  const statusManage = (status) => {
    let element = '';

    if (status === 10) {
      element = (
        <td>
          {/* <img src={IMG.PENDING_CLOCK} className="table-icon" /> */}
          <span className="yellow">Pending</span>
        </td>
      );
    }

    if (status === 20) {
      element = (
        <td>
          {/* <img src={IMG.PENDING_CLOCK} className="table-icon" /> */}
          <span className="yellow">Applied</span>
        </td>
      );
    }

    if (status === 30) {
      element = (
        <td>
          {/* <img src={IMG.CONFIRMED} className="table-icon" /> */}
          <span className="green">Approved</span>
        </td>
      );
    }

    if (status === 40) {
      element = (
        <td>
          {/* <img src={IMG.PENDING_CLOCK} className="table-icon" /> */}
          <span className="yellow">Rejected</span>
        </td>
      );
    }

    if (status === 50) {
      element = (
        <td>
          {/* <img src={IMG.CONFIRMED} className="table-icon" /> */}
          <span className="green">Disbursed</span>
        </td>
      );
    }

    if (status === 60) {
      element = (
        <td>
          <span className="yellow">Claimed</span>
        </td>
      );
    }

    if (status === 70) {
      element = (
        <td>
          <span className="yellow">Errored</span>
        </td>
      );
    }

    if (status === 80) {
      element = (
        <td>
          <span className="green">EMAILSENT</span>
        </td>
      );
    }

    return element;
  };

  const renderRowData = (title) => {
    return (
      <td className={props.dashboardView ? 'row-item-dashboard' : 'row-item'}>
        {title}
      </td>
    );
  };

  const renderDownloadButton = (row) => {
    return (
      <td onClick={() => onDownloadPdf(row.loanDocumentUrl)}>
        <img
          src={IMG.IC_PDF}
          style={{
            height: '16px',
            width: '18px',
            cursor: 'pointer',
          }}
          alt="ic_pdf"
        />
      </td>
    );
  };

  const transformDate = (date) => {
    return moment(date).format('DD/MM/YYYY');
  };

  const callPrev = () => {
    setPage((prevPage) => (prevPage > 1 ? prevPage - 1 : 1));
  };

  const callNext = () => {
    setPage((prevPage) => (prevPage < pageCount ? prevPage + 1 : pageCount));
  };

  const onDownloadPdf = (url) => {
    firebase
      .storage()
      .ref(url)
      .getDownloadURL()
      .then((url) => {
        FileSaver.saveAs(url, '.pdf');
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <>
      <div className="datagrid-container">
        <div className="datagrid-heading-container">
          <h1 className="datagrid-heading">Recent Requests</h1>
          {props.dashboardView && (
            <a className="see-all" href={routes.Transactions}>
              See All
            </a>
          )}
        </div>
        <table className="datagridTable">
          <tbody>
            {!props.dashboardView && (
              <tr className="header-container">{showColumnHeader()}</tr>
            )}
            {data && Object.keys(data).length > 0 ? (
              Object.keys(data)
                .slice((page - 1) * pagination, page * pagination)
                .map((keyName) => {
                  const row = data[keyName];
                  return (
                    <tr className="row" key={keyName}>
                      {props.dashboardView ? (
                        <React.Fragment>
                          {renderRowData(transformDate(row.created_at))}
                          {renderRowData(row.transfer_type)}
                          {renderRowData(row.status)}
                          {renderRowData(row.service_amount)}
                        </React.Fragment>
                      ) : (
                        <React.Fragment>
                          {renderRowData(naCheck(row.id))}
                          {renderRowData(
                            transformDate(naCheck(row.created_at)),
                          )}
                          {renderRowData(
                            naCheck(moment(row.created_at).format('hh:mm A')),
                          )}
                          {renderRowData(row.amount)}
                          {renderRowData(naCheck(row.first_emi_date))}
                          {row.repayment_amount
                            ? renderRowData(
                                parseFloat(row.repayment_amount).toFixed(2),
                              )
                            : renderRowData('-')}
                          {row.status
                            ? statusManage(row.status)
                            : renderRowData('N/A')}
                          {renderRowData(naCheck(row.reason))}
                          {renderRowData(naCheck(row.bankName))}
                          {renderRowData(
                            maskString(naCheck(row.accountNumber)),
                          )}
                          {renderDownloadButton(row)}
                        </React.Fragment>
                      )}
                      {/* <tr>
                        <img
                          src={IMG.IC_PDF}
                          style={{
                            height: '20px',
                            width: '20px',
                            position: 'relative',
                            top: 32,
                            right: 6,
                          }}
                          alt="ic_pdf"
                        />
                      </tr> */}
                    </tr>
                  );
                })
            ) : (
              <tr className="row">
                <td>No Data Available</td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
      {!props.dashboardView && (
        <div className="footer">
          <div className="datagrid-footer">
            <button
              id="prev"
              type="button"
              className={
                page === 1 ? 'footer-button-disabled' : 'footer-button'
              }
              disabled={page === 1}
              onClick={callPrev}
            >
              Prev
            </button>
            <span className="separator">|</span>
            <button
              id="next"
              type="button"
              className={
                page === pageCount ? 'footer-button-disabled' : 'footer-button'
              }
              disabled={!!(page === pageCount)}
              onClick={callNext}
            >
              Next
            </button>
          </div>
        </div>
      )}
    </>
  );
}

export default DataGrid;
