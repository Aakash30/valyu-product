export const COLUMN_DATA = [
  {
    key: 'serviceId',
    label: 'Service Id',
    sort: false,
  },
  {
    key: 'serviceDate',
    label: 'Request Date',
    sort: false,
  },
  {
    key: 'serviceTime',
    label: 'Request Time',
    sort: false,
  },
  {
    key: 'serviceAmount',
    label: 'Request Amount',
    sort: false,
  },
  {
    key: 'submitDate',
    label: 'Repayment Date',
    sort: true,
  },
  {
    key: 'submitAmount',
    label: 'Repayment Amount',
    sort: true,
  },
  {
    key: 'status',
    label: 'Status',
    sort: true,
  },
  {
    key: 'reason',
    label: 'Purpose',
    sort: false,
  },
  {
    key: 'bankName',
    label: 'Bank Name',
    sort: false,
  },
  {
    key: 'depositeTo',
    label: 'Deposit To',
    sort: false,
  },
  {
    key: 'download',
    label: '',
    sort: false,
  },
];

export const ROWS_DATA = [
  {
    serviceId: '#123445',
    serviceDate: '10-07-2020',
    serviceAmount: '₹5,000',
    submitDate: '10-07-2020',
    submitAmount: '₹5,000',
    transferType: 'Bank Transfer',
    depositeTo: '1234 XXXX XXXX 089',
  },
  {
    serviceId: '#123445',
    serviceDate: '10-07-2020',
    serviceAmount: '₹5,000',
    submitDate: '10-07-2020',
    submitAmount: '₹5,000',
    transferType: 'Bank Transfer',
    depositeTo: '1234 XXXX XXXX 089',
  },
  {
    serviceId: '#123445',
    serviceDate: '10-07-2020',
    serviceAmount: '₹5,000',
    submitDate: '10-07-2020',
    submitAmount: '₹5,000',
    transferType: 'Bank Transfer',
    depositeTo: '1234 XXXX XXXX 089',
  },
  {
    serviceId: '#123445',
    serviceDate: '10-07-2020',
    serviceAmount: '₹5,000',
    submitDate: '10-07-2020',
    submitAmount: '₹5,000',
    transferType: 'Bank Transfer',
    depositeTo: '1234 XXXX XXXX 089',
  },
  {
    serviceId: '#123445',
    serviceDate: '10-07-2020',
    serviceAmount: '₹5,000',
    submitDate: '10-07-2020',
    submitAmount: '₹5,000',
    transferType: 'Bank Transfer',
    depositeTo: '1234 XXXX XXXX 089',
  },
];
