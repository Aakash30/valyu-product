import React from 'react';
import IMG from '../../assets/images/spinner.svg';
import './Loader.scss';

export default function Loader() {
  return (
    <div className="loading-container">
      <img src={IMG} alt="loader" />
    </div>
  );
}