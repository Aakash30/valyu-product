import React from 'react';
import PropTypes from 'prop-types';
import './InfoField.scss';
import IMG from '../../utils/images';

export default function InfoField({
  label,
  value,
  sup = '',
  isEditable,
  type,
  link,
  onValueChange,
  onEditClick = () => {},
}) {
  const handleChange = (event) => {
    const val = event.target.value;
    onValueChange(val);
  };
  return (
    <div className="info-container">
      <div className="label-container">
        <p className="label">
          {label}
          <sup>{sup}</sup>
        </p>
      </div>
      <div className="value-container">
        {type && type.toLowerCase() === 'input' ? (
          <input className="input-box" value={value} onChange={handleChange} />
        ) : (
          <React.Fragment>
            <p className="value">{value}</p>
            {isEditable && (
              <img
                src={IMG.IC_EDIT}
                alt="edit"
                className="edit-icon"
                onClick={onEditClick}
                role="presentation"
              />
            )}
          </React.Fragment>
        )}
        {type && type.toLowerCase() === 'link' && (
          <React.Fragment>
            <p className="value">
              {link ? (
                <a target="_blank" rel="noopener noreferrer" href={link}>
                  Click to open
                </a>
              ) : (
                <p>N/A</p>
              )}
            </p>
          </React.Fragment>
        )}
      </div>
    </div>
  );
}

InfoField.propTypes = {
  label: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  sup: PropTypes.string,
  isEditable: PropTypes.bool,
  onEditClick: PropTypes.func,
  type: PropTypes.string,
  onValueChange: PropTypes.func,
};
