import React from 'react';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

export default function CustomTab({ data, selectedTab, handleTabFun }) {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
    handleTabFun(newValue);
  };

  return (
    <Paper square>
      <Tabs
        value={selectedTab}
        indicatorColor="white"
        textColor="primary"
        onChange={handleChange}
        aria-label=" tabs example"
      >
        {data &&
          data.map((elm, index) => {
            return (
              <Tab
                className={elm.value === selectedTab ? 'tab-active' : ''}
                label={elm.name}
                key={'tab' + index}
              />
            );
          })}
      </Tabs>
    </Paper>
  );
}
