import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import './sidebar.scss';
import IMG from '../../../utils/images.js';
import { routes } from '../../../routing/routes';


function Sidebar(props) {

  const { userProfile: { firstName, lastName }, userImage } = props;

  const [menuList] = React.useState([
    { name: 'Dashboard', image: IMG.MOB_DASHBOARD, value: 'dashboard', link: routes.Dashboard, },
    { name: 'Apply', image: IMG.MOB_REQUEST, value: 'request', link: routes.CreateRequest, },
    { name: 'Transections', image: IMG.MOB_TRANSCATION, value: 'transection', link: routes.Transactions, },
    { name: 'Profile', image: IMG.MOB_PROFILE, value: 'profile', link: routes.Profile, },
    { name: 'Help', image: IMG.MOB_HELP, value: 'help', link: routes.Help, },
    // { name: 'Settings', image: IMG.MOB_SETTINGS, value: 'settings', link: routes.Setting, },
  ]);

  const [navLink, setNavLink] = React.useState('');

  useEffect(() => {

    const navLink = menuList.map((data, index) => {
      return <div className="sidebar-menu-row" key={'navLink' + index}>
        <img src={data.image} alt={`sidebarIcon${index}`} className="sidebar-icon" />
        <p className="sidebar-option">{data.name}</p>
        <Link to={data.link}>
          <img src={IMG.MOB_ARROW} alt={`sidebarArrow${index}`} className="sidebar-arrow" />
        </Link>
      </div>
    });

    setNavLink(navLink);

  }, [menuList]);

  return (

    <div className="sidebar">
      <div>
        <div className="sidebar-menu-row">
          <img src={userImage || IMG.MOB_USER} alt="userImg" className="sidebar-icon-img" />
          <p className="sidebar-option fw-600">{`${firstName} ${lastName}`}</p>
        </div>

        {navLink}

        <div className="sidebar-menu-row" onClick={() => props.logoutFun()}>
          <img src={IMG.LOGOUT} alt="Logout" className="sidebar-icon" />
          <p className="sidebar-option">Logout</p>
        </div>

        <div className="sidebar-menu-row">
          <img src={IMG.TERMS} alt="TermsConditions" className="sidebar-icon" />
          <p className="sidebar-option">Terms & Conditions</p>
        </div>

        <div className="sidebar-menu-row">
          <img src={IMG.PRIVATE_POLICY} alt="privacyPolicy" className="sidebar-icon" />
          <p className="sidebar-option">Privacy Policy</p>
        </div>
      </div>

      <div style={{ marginTop: '1.5rem', marginLeft: '0.8rem' }}>
        <div>
          <p className="sidebar-note sidebar-note-width">Available on</p>
          <span>
            <img src={IMG.MOB_PLAYSTORE} alt="playStore" className="sidebar-img" />
          </span>
          <span>
            <img src={IMG.APP_STORE} alt="appStore" className="sidebar-img" />
          </span>
          <p className="sidebar-note">Valyu © 2020 | All rights reserved</p>
        </div>
      </div>
    </div>
  )
}

export default Sidebar