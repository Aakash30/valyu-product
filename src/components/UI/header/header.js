import React, { Component } from 'react';
import { connect } from 'react-redux';
import './header.scss';
import * as firebase from 'firebase';
import { get } from 'lodash';
import { Redirect } from 'react-router-dom';
import _ from 'lodash';

import IMG from '../../../utils/images.js';
import * as actions from '../../../store/actions/index';
import Sidebar from '../sidebar/sidebar';
import { routes } from '../../../routing/routes';
import NotificationPopup from '../notification/notification';

class Header extends Component {
  state = {
    companiesList: [],
    isSelected: '',
    companyUrl: '',
    userImage: '',
    logout: false,
    isSidebar: false,
    isFetchSuccess: false,
    logoUrl: this.props?.userInfo?.selectedCompany?.logoUrl,
  };

  async componentDidMount() {
    await this.props.fetchNotification(this.props?.userInfo?.phoneNumber);

    window.addEventListener('resize', this.handleResize);

    this.onHandleLogo();
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.clientList.length !== this.props.clientList.length) {
      this.onHandleLogo();
    }
  }

  // on handle logo url
  onHandleLogo = async () => {
    // if (!this.state.logoUrl) {
    const companyData = await this.props?.userInfo?.selectedCompany;

    if (!companyData?.logoUrl) {
      const companyObj = _.keyBy(this.props.clientList, 'id');

      if (companyObj[companyData.id]?.logoUrl) {
        companyData.logoUrl = companyObj[companyData.id].logoUrl;
      }

      await this.props.onSetCompany(companyData);
    }

    if (companyData?.logoUrl) {
      if (!companyData?.logoUrl.includes('firebasestorage.googleapis.com')) {
        await this.fetchImage(companyData.logoUrl, 'logoUrl');
      }
    }
    // }
  };

  // on handle resize
  handleResize = () => {
    if (window.innerWidth > 767) {
      this.setState({ isSidebar: false });
    }
  };

  // get image from storage
  fetchImage = (filePath, keyname) => {
    let { isFetchSuccess } = this.state;
    firebase
      .storage()
      .ref(filePath)
      .getDownloadURL()
      .then((url) => {
        if (keyname === 'logoUrl') {
          isFetchSuccess = true;
        }
        this.setState({ [keyname]: url, isFetchSuccess });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  logoutUser = () => {
    this.props.onLogoutUser();
    console.log('94');
    this.setState({
      logout: true,
    });
  };

  // functin call for open and close sidebar
  onHandleSidebar = () => {
    const { isSidebar } = this.state;
    this.setState({ isSidebar: !isSidebar });
  };

  render() {
    if (this.state.logout) {
      console.log('106');
      return <Redirect to={'/auth/login'} />;
    }

    const { userProfile } = this.props?.userData;
    const { profilePicUrl, firstName, lastName } = userProfile ?? {};

    // const {
    //   userProfile: { profilePicUrl },
    //   userProfile: { firstName, lastName },
    // } = this.props?.userData;

    const { userImage, isSidebar, logoUrl } = this.state;

    if (!userImage && profilePicUrl) {
      this.fetchImage(profilePicUrl, 'userImage');
    }

    return (
      <div>
        <header className="header">
          <div className="logo">
            <img src="/valyu-logo.png" alt="valyu logo" />
          </div>
          <div className="company-logo">
            {logoUrl ? <img alt="company name" src={logoUrl} /> : null}
          </div>
          <div className={`profile`}>
            <div className="user-img">
              <img src={userImage || '/static/images/user.png'} alt="" />
            </div>
            <span className="name">{`${firstName} ${lastName}`}</span>
            <span className="icon"></span>
            <div className={`options`}>
              <ul>
                <li onClick={this.logoutUser}>
                  <img src={IMG.LOGOUT} className="icon-head" alt="" />
                  Logout
                </li>
              </ul>
            </div>
          </div>

          {/* Menu bar for mobile */}

          <div className="nav-icon">
            <input
              type="checkbox"
              checked={isSidebar}
              id="checkbox3"
              className="checkbox3 visuallyHidden"
              onChange={() => this.onHandleSidebar()}
            />
            <label htmlFor="checkbox3">
              <div className="hamburger hamburger3">
                <span className="bar bar1"></span>
                <span className="bar bar2"></span>
                <span className="bar bar3"></span>
              </div>
            </label>
          </div>

          <NotificationPopup data={this.props.notifications} />
        </header>

        {isSidebar && (
          <Sidebar
            userProfile={this.props?.userData?.userProfile}
            userImage={userImage}
            logoutFun={() => this.logoutUser()}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userData: get(state.global, 'userData', {}),
    userInfo: get(state.global, 'userInfo', {}),
    UIConstant: get(state.global, 'UIConstant', {}),
    notifications: get(state.global, 'notifications', {}),
    clientList: get(state.global, 'clientList', []),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onLogoutUser: () => dispatch(actions.logOutUser()),
    fetchNotification: (number) =>
      dispatch(actions.onFetchNotifications(number)),
    onSetCompany: (payload) => dispatch(actions.onSetCompany(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
