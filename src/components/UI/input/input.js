/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

import * as _ from 'lodash';

import FormControlLabelPlacement from '../radio&checkbox/radio-button';

let customStyles = {
  control: (base, state) => ({
    ...base,
    background: 'white',
    fontSize: '12px',
    border: 'none',
    height: 36,
    minHeight: 36,
  }),
  menuList: (base) => ({
    ...base,
    // kill the white space on first and last option
    padding: 0,
    fontSize: '12px',
  }),
};

const input = (props) => {
  let originalElement = null;
  let inputElement = null;
  let errorElement = null;
  let spanElement = null;
  let labelElement = null;
  let iconElement = null;

  const inputClasses = [props?.divClass];

  if (props?.touched && props?.value) {
    inputClasses.push('active');
  }

  if (!props?.isSubmitted && props?.invalid) {
    errorElement = (
      <p className="error-msg margin-zero">
        Required{' '}
        {props?.shouldValidate?.minLength > 1
          ? 'minimum ' + props?.shouldValidate?.minLength
          : null}{' '}
        {props?.shouldValidate?.isNumeric ? 'digits' : null}{' '}
        {props?.shouldValidate?.isEmail ? 'valid email' : null}.
      </p>
    );
  }

  switch (props.elementType) {
    case 'input':
      inputElement = (
        <input
          className={props.class}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
          onKeyPress={props.onKeyPress}
          isrequired={props?.isRequired}
        />
      );
      break;
    case 'textarea':
      inputElement = (
        <textarea
          {...props.elementConfig}
          className={props.class}
          value={props.value}
          onChange={props.changed}
        />
      );
      break;
    case 'select':
      inputElement = (
        <Select
          value={_.find(props.options, {
            value: props.value.value,
          })}
          onChange={props.changed}
          options={props.options}
          {...props.elementConfig}
          styles={customStyles}
          isSearchable={true}
        />
      );
      break;
    case 'radio':
      inputElement = <FormControlLabelPlacement {...props} />;
      break;
    case 'checkbox':
      inputElement = <FormControlLabelPlacement {...props} />;
      break;
    default:
      inputElement = (
        <input
          className={props.class}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      );
  }

  if (props?.span) {
    spanElement = <span className={props?.span?.spanClass}>&#8377;</span>;
  }

  if (props?.label) {
    labelElement = (
      <label className={props?.labelClass}>
        {props?.label}{' '}
        {props?.isRequired ? <span className="required">*</span> : null}
      </label>
    );
  }

  if (props?.isIconInput) {
    iconElement = (
      <img src={props?.iconImg} alt="INF0_ICON" className={props?.iconClass} />
    );
  }

  if (props?.divClass && !props?.isAddDiv && !props?.isAddSalary) {
    originalElement = (
      <Fragment>
        <div className={inputClasses?.join(' ')}>
          {spanElement}
          {labelElement}
          {inputElement}
          {iconElement}
          {props?.inDivError && errorElement}
        </div>
        {!props?.inDivError && errorElement}
      </Fragment>
    );
  }

  if (
    props?.divClass &&
    props?.isAddDiv &&
    !props?.divClasses &&
    !props?.isAddSalary
  ) {
    originalElement = (
      <div>
        <div className={inputClasses?.join(' ')}>
          {spanElement}
          {labelElement}
          {inputElement}
          {iconElement}
        </div>
        {errorElement}
      </div>
    );
  }

  if (
    props?.divClass &&
    props?.isAddDiv &&
    props?.divClasses &&
    !props?.isAddSalary
  ) {
    originalElement = (
      <div className={props?.divClasses}>
        <div className={inputClasses?.join(' ')}>
          {spanElement}
          {labelElement}
          {inputElement}
          {iconElement}
        </div>
        {errorElement}
      </div>
    );
  }

  if (
    props?.divClass &&
    props?.isErrorDiv &&
    props?.divClasses &&
    !props?.isAddSalary
  ) {
    originalElement = (
      <Fragment>
        <div className={props?.divClasses}>
          <div className={inputClasses?.join(' ')}>
            {spanElement}
            {labelElement}
            {inputElement}
            {iconElement}
          </div>
        </div>
        {errorElement}
      </Fragment>
    );
  }

  if (props?.isAddSalary) {
    originalElement = (
      <Fragment>
        <div>
          {labelElement}
          <div className={props?.divClasses}>
            <div className={inputClasses?.join(' ')}>
              {spanElement}
              {inputElement}
              {iconElement}
            </div>
          </div>
          {errorElement}
        </div>
      </Fragment>
    );
  }

  if (!props?.divClass) {
    originalElement = (
      <Fragment>
        {inputElement}
        {errorElement}
      </Fragment>
    );
  }

  return originalElement;
};

export default input;

input.prototype = {
  divClass: PropTypes.string,
  touched: PropTypes.bool,
  value: PropTypes.node.isRequired,
  isSubmitted: PropTypes.bool,
  invalid: PropTypes.bool,
  shouldValidate: PropTypes.object,
  elementType: PropTypes.string.isRequired,
  class: PropTypes.string,
  elementConfig: PropTypes.object.isRequired,
  changed: PropTypes.func.isRequired,
  onKeyPress: PropTypes.func,
  isRequired: PropTypes.bool,
};
