import React, { useEffect } from 'react';
import './nav.scss';
import { Link } from 'react-router-dom';

import { routes } from '../../../routing/routes';
import { useSelector } from 'react-redux';

function Nav() {
  const [menuList] = React.useState([
    { name: 'Dashboard', link: routes.Dashboard, value: 'dashboard' },
    { name: 'Advance Central', link: routes.CreateRequest, value: 'request' },
    { name: 'History', link: routes.Transactions, value: 'transection' },
    { name: 'Profile', link: routes.Profile, value: 'profile' },
    { name: 'Support', link: routes.Help, value: 'help' },
    // { name: 'Settings', link: routes.Setting, value: 'settings' },
  ]);

  const [navLink, setNavLink] = React.useState('');

  const active = useSelector((state) => state.global.activePath);

  useEffect(() => {
    const navLink = menuList.map((data, index) => {
      return (
        <Link
          key={index}
          to={data.link}
          className={`${data.value}${active === data.value ? ' active' : ''}`}
        >
          <span
            className={`icon${active === data.value ? ' active' : ''}`}
          ></span>
          {data.name}
        </Link>
      );
    });

    setNavLink(navLink);
  }, [menuList, active]);

  return (
    <nav className="nav">
      <div className="list">{navLink}</div>
    </nav>
  );
}

export default Nav;
