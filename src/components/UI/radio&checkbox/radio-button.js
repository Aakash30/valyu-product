import React from 'react';
import Radio from '@material-ui/core/Radio';
import Checkbox from '@material-ui/core/Checkbox';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { FormGroup } from '@material-ui/core';


export default function FormControlLabelPlacement(props) {
    return (
        <FormControl component="fieldset" className={`radio-wrapper ${!props.isMargin ? 'margin' : ''}`}>

            {props?.elementConfig?.label && <div component="legend" className="radio-heading">{props?.elementConfig?.label}</div>}

            {props.elementType === 'radio' && <RadioGroup row aria-label="position" name="position" defaultValue={props?.value}>

                {props?.options &&
                    props?.options.map((elm, index) => {
                        return <FormControlLabel key={'radion_' + index} value={elm?.value} control={<Radio color="primary" />} label={elm?.label} className="test" onChange={props?.changed} />
                    })
                }

            </RadioGroup>}

            {props?.elementType === 'checkbox' && <FormGroup>
                {props?.options &&
                    props.options.map((elm, index) => {
                        return <FormControlLabel key={'checkbox_' + index} value={elm?.value} control={<Checkbox color="primary" />} label={elm?.label} className="test" onChange={props?.change} {...props?.elementConfig} name={'option'} />
                    })}
            </FormGroup>}

        </FormControl>
    );
}