import React, { useEffect } from 'react';
import Popper from '@material-ui/core/Popper';
import PopupState, { bindToggle, bindPopper } from 'material-ui-popup-state';
import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';
import './notification.scss';
import IMG from '../../../utils/images.js';
import Tab from '../tab/tab';
import _ from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import * as actions from '../../../store/actions/index';


export default function NotificationPopup({ data }) {
  const dispatch = useDispatch();
  const userInfo = useSelector((state) => state.global.userInfo);
  const [tabs] = React.useState([
    { name: 'All', value: 0 },
    { name: 'Unread', value: 1 },
  ]);
  const [selectedTab, setSelectedTab] = React.useState(0);
  const dataArray = _.keys(data);
  let [notificationElement, setNotificationElement] = React.useState(
    'No notification available',
  );

  useEffect(() => {
    const onHandleNotificationData = () => {
      let divElement = [];
      if (dataArray.length > 0) {
        divElement = dataArray.map((key) => {
          const rowData = data[key];
          if (rowData.isDelete || (rowData.isRead && selectedTab === 1)) {
            return false;
          }

          return (
            <div className="notification-tabs" key={key}>
              <div className="notification-border border-green"></div>
              <div className="notification-icon">
                <img src={IMG.N_GREEN} alt="GREEN" />
                {/* <img src={IMG.N_RED}/> */}
              </div>
              <div className="notification-heading">
                <p className="msg-heading">{rowData.title}</p>
                <p className="msg-content">{rowData.body || rowData.body_data}</p>
              </div>
              <div
                className="notification-cross"
                onClick={() => onHandleStatus(rowData, key)}
              >
                <img src={IMG.CROSS} alt="CROSS" />
              </div>
            </div>
          );
        });
      }

      divElement = divElement.filter((value) => value !== undefined);
      if (divElement.length > 0) {
        setNotificationElement(divElement);
      } else {
        setNotificationElement('No notification available');
      }
    };

    const onHandleStatus = (data, key) => {
      data.id = key;
      if (selectedTab === 0) {
        data.isDelete = true;
      } else {
        data.isRead = true;
      }
      dispatch(actions.postNotificationStatus(data, userInfo.phoneNumber));
    };
    onHandleNotificationData();
  }, [selectedTab, data]);



  const onHandleTab = (tab) => {
    setSelectedTab(tab);
  };

  return (
    <PopupState variant="popper" popupId="demo-popup-popper">
      {(popupState) => (
        <div className="notification-container">
          <div className="notification" {...bindToggle(popupState)}>
            <span className="icon"></span>
          </div>
          <Popper {...bindPopper(popupState)} transition>
            {({ TransitionProps }) => (
              <Fade {...TransitionProps} timeout={150}>
                <Paper>
                  <div className="notification-wrapper">
                    <div className="notification-header">
                      <h5 className="notification-heading">Notifications</h5>
                      <div className="sub-heading mr-0">
                        {/* <CustomTab /> */}
                        <Tab
                          data={tabs}
                          selectedTab={selectedTab}
                          handleTabFun={(selectedTab) =>
                            onHandleTab(selectedTab)
                          }
                        />
                      </div>
                    </div>

                    <div className="notification-tab-wrapper">
                      {notificationElement}
                    </div>
                  </div>
                </Paper>
              </Fade>
            )}
          </Popper>
        </div>
      )}
    </PopupState>
  );
}
