import React, { Fragment, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import './modal.scss';
import Aux from '../../../hoc/auxiliary/auxiliary';
import IMG from '../../../utils/images';


export default function AlertDialog(props) {

    const [open, setOpen] = React.useState(props.open);
    const [invalid, setInvalid] = React.useState('');
    const [isVerify, setIsVerify] = React.useState(false);

    const [isClose, setIsClose] = React.useState(props.isClose);

    useEffect(() => {
        if (props.isConfirmCode) {
            if (isVerify && isClose) {
                setOpen(false);
                props.onLoaderStart();
                props.onHandleCloseAlert();
            }
        } else {
            if (props.isInvalidCode) {
                setInvalid(<p className="error-msg">OTP entered is incorrect.</p>);
            } else {
                setInvalid('');
            }
        }
    }, [props, isVerify, isClose]);

    useEffect(() => {
        if(props.errorMsg){
            setInvalid(<p className="error-msg">{props.errorMsg}</p>);
            setIsClose(false);
        }
    }, [props.errorMsg, isClose]);

    const handleClose = () => {
        props.onLoaderStart();
        if (props.isClose && !props.errorMsg) {
            props.confirmCodeFun();
            setInvalid('');
            setIsVerify(true);
            setIsClose(true);
        } else {
            props.onLoaderStop();
            setInvalid(<p className="error-msg">OTP entered is incorrect.</p>);
            return;
        }
    };

    // render() {
    return (
        <Aux>
            {open && <div>
                <Dialog
                    open={open}
                    onClose={() => handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    className="dialog-width"
                >
                    {!props.isOTP && <Fragment>
                        <DialogTitle id="alert-dialog-title" className="modal-heading">{props?.title}</DialogTitle>
                        <DialogContent className="modal-content">
                            <DialogContentText id="alert-dialog-description">
                                {props?.text}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            {props?.isClose && <Button onClick={handleClose} color="primary" className="modal-btn" autoFocus>
                                {props?.buttonName}
                            </Button>}
                        </DialogActions>
                    </Fragment>}
                    {props.isOTP &&
                        <div className="main-container-modal">
                            <div className="main-content">
                                <DialogTitle id="alert-dialog-title" className="modal-heading">
                                    <p className="sub-heading">You are just <span style={{ color: '#0031ff' }}>30 seconds</span> away!</p>
                                </DialogTitle>
                                <DialogContent className="modal-content">
                                    <DialogContentText id="alert-dialog-description">
                                        {props?.text && <p className="sub-heading">{props?.text}</p>}
                                        {props?.text_1 && <p className="content">{props?.text_1}</p>}
                                        {props.form && <div>
                                            {props.form}
                                        </div>}
                                        {(props.isInvalidCode || !props.isClose) && invalid}
                                        {!props.isInvalidCode && props.isClose && <p className="otp-statement">
                                            <img alt="" src={IMG.CHECK_GREEN} />
                                            <span>OTP enetered is correct</span>
                                        </p>}
                                        {props?.text_2 && <p className="modal-link" onClick={() => props.onSendOTPToFun()}>{props?.text_2}</p>}
                                        {props?.text_3 && <p className="modal-link">{props?.text_3}</p>}
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions className="modal-btn">
                                    <Button onClick={handleClose} color="primary" className="create-request-btn" autoFocus>
                                        {props.buttonName}
                                    </Button>
                                </DialogActions>
                            </div>
                            <div className="create-illustration-wrapper">
                                <img alt="" src={IMG.OTP_VERIFICATION} />
                            </div>
                        </div>}
                </Dialog>
            </div>}
        </Aux>
    );
    // }
}
