export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const updateStorage = async(fieldName, updatedData) => {
    let saveData = JSON.parse(localStorage.getItem(fieldName));
    const updateField = await updateObject(saveData, updatedData);
    await localStorage.setItem(fieldName, JSON.stringify(updateField));
}

export const getStorageData = async(fieldName) => {
  return JSON.parse(localStorage.getItem(fieldName));
}

export const getMonths = () => {
    let i = 1, array=[];
    while(i <= 60){
        array.push({value: i, label: i});
        i++;
    }
    return array;
}

export const checkValidity = (data, rules) => {

    let isValid = true;
    let value = rules.isTrim ? data.replace(/\s/g, '') : data;

    if (!rules) {
        return true;
    }

    if (rules.required) {
        isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
        isValid = value.length >= rules.minLength && isValid
    }

    if (rules.maxLength) {
        isValid = value.length <= rules.maxLength && isValid
    }

    if (rules.isEmail) {
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isNumeric) {
        const pattern = /^\d+$/;
        isValid = pattern.test(value) && isValid
    }

    return isValid;
}