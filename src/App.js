import React, { Component, Fragment } from 'react';
import './App.scss';
import NotificationsProvider from './components/Notification/NotificationsProvider';
import BasicRouting from './routing/routing';

class App extends Component {
  render() {
    return (
      <Fragment>
          <NotificationsProvider
            onNotificationsReceived={payload =>
            {this.onNotificationsReceived(payload)}
            }
          >
          <BasicRouting />
        </NotificationsProvider>
      </Fragment>
    )
  }
}

export default App;
